import React, { Component } from 'react';
import { ScrollView } from "react-native";
import WifiManager from "react-native-wifi-reborn";
import { Container, Header, Left, Right, CheckBox, Body, Title, Accordion, View, Button, Icon, Content, List, ListItem, Text, H1, H2, H3, Form, Item, Input, Label } from 'native-base';
import BasePage from "../../base/BasePage";
import APISourcerPrelogin from '../../../apis/APISourcerPrelogin';
import BaseNavigationBar from '../../base/BaseNavigationBar';

export default class ForgotPasswordFormVerificationPage extends BasePage {

  constructor(props)
  {
    super(props);
    this.state = {
      password: '',
      password_confirm: '',
      code: '',
    }
  }

  componentDidMount(): void {
    this.showLoading()
    new APISourcerPrelogin().resetPassword_request_verify(this.props.navigation.state.params.username)
        .then((res) => {
          this.hideLoading()
        })
        .catch((e) => {
          this.hideLoading()
        });
  }

  renderInner() {

    var isDisabled =
        !this.state.code.trim()
        || this.state.code.trim().length < 6
        || !this.state.password.trim()
        || this.state.password.length < 8
        || !this.state.password_confirm.trim()
        || this.state.password_confirm.length < 8;

    return (
        <Container>

          <BaseNavigationBar {...this.props}
                             isBack
                             title={`Reset password`.i18n()}
          />

          <Content padder>

            <H1 style={{color: '#586C78', marginTop: 60, marginBottom: 30, textAlign: `center`}}>{`Account verification`.i18n()}</H1>

            <Text style={{color: '#586C78', padding: 5, textAlign: `left`}}>{`Please check your email inbox (including your spam inbox) for an email sent by D2Bros. Retrieve and type the verification code and create your new password below:`.i18n()}</Text>


            <Form>
              <Item stackedLabel>
                <Label>{`Verification code`.i18n()}</Label>
                <Input value={this.state.code} autoCapitalize={false} autoCompleteType={'off'} onChangeText={(val) => {this.setState({code: val})}} />
              </Item>
              <Item stackedLabel>
                <Label>{`Password`.i18n()} ({`min length: 8 characters`.i18n()})</Label>
                <Input value={this.state.password} autoCapitalize={false} autoCompleteType={'off'} secureTextEntry onChangeText={(val) => {this.setState({password: val})}} />
              </Item>
              <Item stackedLabel>
                <Label>{`Confirm password`.i18n()}</Label>
                <Input value={this.state.password_confirm} autoCapitalize={false} autoCompleteType={'off'} secureTextEntry onChangeText={(val) => {this.setState({password_confirm: val})}} />
              </Item>
            </Form>


            <View style={{height: 50}}/>

            <Button
                disabled={isDisabled}
                onPress={() => {

                  if (this.state.password !== this.state.password_confirm)
                  {
                    alert(`Password must match confirm password field input.`.i18n());
                    return;
                  }

                  this.showLoading();
                  new APISourcerPrelogin().resetPassword_verify(
                      this.props.navigation.state.params.username,
                      this.state.code,
                      this.state.password,
                  )
                      .then((res) => {
                        this.hideLoading();
                        this.props.navigation.navigate("ForgotPasswordResetCompletionPage", {
                          username: this.props.navigation.state.params.username,
                          password: this.state.password,
                        })
                      })
                      .catch((e) => {
                        this.hideLoading();
                        alert(e.message);
                      })
                }}
                style={{opacity: isDisabled ? 0.5 : 1, backgroundColor: '#4fadf9', justifyContent: `center`}}>
              <H3 style={{color: '#fff'}}>{`Confirm and submit`.i18n()}</H3>
            </Button>

          </Content>

        </Container>
    );
  }
}
