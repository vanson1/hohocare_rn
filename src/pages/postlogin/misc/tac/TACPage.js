import React, { Component } from 'react';
import { ScrollView, Image } from "react-native";
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3,
} from "native-base";
import BasePage from "../../../base/BasePage";
import BaseNavigationBar from '../../../base/BaseNavigationBar';
import BaseWebViewPage from '../../../base/BaseWebViewPage';


export default class TACPage extends BaseWebViewPage {

  getTitle(): string {
    return `Terms of Use`.i18n();
  }
  getUrl(): string {
    return GLOBAL.url_tc || GLOBAL.url_disclaimer;
  }
}
