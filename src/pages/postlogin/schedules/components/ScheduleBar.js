import React, { Component } from 'react';
import { Container, View, Header, Left, Right, Body, Title, Accordion, Button, Icon, Content, List, ListItem, Text } from 'native-base';

export default class SchedulesMainPage extends React.Component {
  constructor(props){
    super(props);
    this.state = {

    }
  }


  getViewPositionX(seconds)
  {
    return 15 + (((this.state.w2 || 0)) * seconds / (24*60*60));
  }



  getFilteredData(data)
  {
    var fdata = [];
    for (var k in data)
    {
      if (!data[k].disabled)
      {
        fdata.push(data[k]);
      }
    }
    return fdata;
  }



  render() {
    var markers = [
      {seconds: 0 * 60 * 60 },
      {seconds: 6 * 60 * 60 },
      {seconds: 12 * 60 * 60 },
      {seconds: 18 * 60 * 60 },
      {seconds: 24 * 60 * 60 },
    ]

    var data = this.getFilteredData(this.props.data || []);

    return (
        <View style={{alignItems: `stretch`, padding: 15}}>
          <View onLayout={(event) => {
            let {x, y, width, height} = event.nativeEvent.layout;
            this.setState({w1: width - 10})
            }}
                style={{height: 20, borderRadius: 10, paddingHorizontal: 7, flexDirection: `row`, justifyContent: `space-between`}}>

            <Text note style={{}}>
              00
            </Text>

            <Text note style={{}}>
              06
            </Text>

            <Text note style={{}}>
              12
            </Text>

            <Text note style={{}}>
              18
            </Text>

            <Text note style={{}}>
              24
            </Text>

          </View>


          <View onLayout={(event) => {
            let {x, y, width, height} = event.nativeEvent.layout;
            this.setState({w2: width - 30, h2: height})
            }}
                style={{overflow: 'hidden', height: 20, borderRadius: 10, paddingHorizontal: 15, backgroundColor: '#eee'}}>



            {
              markers.map((it) => {
                return (
                    <View style={{width: 2, backgroundColor: '#ccc',
                      height: this.state.h2,
                      left: this.getViewPositionX(it.seconds || it.start_time || 0), position: `absolute`}}>
                      <View style={{height: this.state.h2}}></View>
                    </View>
                )
              })
            }


            {
              data.map((it, index) => {

                var bgColor = '#333';

                if (it.action && it.action.power)
                {
                  bgColor = 'rgba(12, 164, 227, 1.0)';
                }

                return (
                    <View style={{width: 2, backgroundColor: bgColor,
                      height: this.state.h2,
                      left: this.getViewPositionX(it.seconds || it.start_time), position: `absolute`}}>
                      <View style={{height: this.state.h2}}></View>
                    </View>
                )
              })
            }

            {
              data.map((it, index) => {

                var next_it_seconds = 25 * 60 * 60;

                if (index < data.length - 1)
                {
                  var next_it = data[index + 1];
                  next_it_seconds = next_it.seconds || next_it.start_time;
                }

                var it_seconds = it.seconds || it.start_time;

                if (it_seconds <= 0) {
                  it_seconds = - 60 * 60
                }

                var bgColor = 'rgba(55,55,55,0.3)';

                if (it.action && it.action.power)
                {
                  bgColor = 'rgba(12, 164, 227, 0.5)';
                }


                return (
                    <View style={{backgroundColor: bgColor,
                      height: this.state.h2,
                      left: this.getViewPositionX(it_seconds),
                      width: this.getViewPositionX(next_it_seconds) - this.getViewPositionX(it_seconds),
                      position: `absolute`}}>
                      <View style={{height: this.state.h2}}></View>
                    </View>
                )
              })
            }


          </View>
        </View>
    );
  }
}
