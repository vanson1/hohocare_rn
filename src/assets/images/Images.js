import { AsyncStorage, DeviceEventEmitter } from 'react-native';
import APISourcerMqtt from "../../apis/APISourcerMqtt";
import APISourcerPreference from '../../apis/APISourcerPreference';
import APISourcerExternal from '../../apis/APISourcerExternal';
import APISourcerPrelogin from '../../apis/APISourcerPrelogin';
import moment from 'moment';

class Images {

  Make(device, os, env) {

    GLOBAL.VERSION = "20.";

    // GLOBAL.SERIALMAPS = [
    //   {serial: "A0000001", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000002", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000003", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000004", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000005", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000006", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000007", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000008", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000009", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000010", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000011", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000012", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000013", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000014", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000015", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000016", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000017", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000018", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000019", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000020", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000021", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000022", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000023", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000024", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000025", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000026", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000027", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000028", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000029", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "A0000030", access_token: "UVRBd01EQXdNREU9"},
    //   {serial: "AC_AIS89F789", access_token: "AC_YUTGJU4564657689ETYUHGVFG="},
    // ]

    GLOBAL.GET_ACCESS_TOKEN = (serial) => {
      // for (var k in GLOBAL.SERIALMAPS)
      // {
      //   var serialmap = GLOBAL.SERIALMAPS[k];
      //   if (serialmap.serial === serial)
      //   {
      //     return serialmap.access_token;
      //   }
      // }

      for (var k in GLOBAL.STORE.devices)
      {
        var device = GLOBAL.STORE.devices[k];
        if (device.serial === serial)
        {
          if (device.access_token !== null && device.access_token !== undefined)
          {
            return device.access_token;
          }
        }
      }

      return "UVRBd01EQXdNREU9";
    }

    GLOBAL.GET_DEGREE = (celsius) =>
    {
      var cTemp = celsius;

      if (cTemp !== null && cTemp !== undefined)
      {
        if (GLOBAL.APP_GETDEGREEDISPLAY()
            && GLOBAL.APP_GETDEGREEDISPLAY() === `f`)
        {
          var cToFahr = cTemp * 9 / 5 + 32;
          cToFahr = Math.round(cToFahr * 100) / 100;
          if (isNaN(cToFahr))
          {
            return "-- F";
          }
          return cToFahr + "F";
        }
        else
        {
          cTemp = Math.round(cTemp * 100) / 100;
          if (isNaN(cTemp))
          {
            return "-- F";
          }
          return cTemp + "˚C";
        }
      }

      return cTemp;
    }

    GLOBAL.APP_SETDEGREEDISPLAY = ((lang) => {
      GLOBAL.___degreedisplay = lang;
      return AsyncStorage.setItem('__DEGREEDISPLAY', lang)
          .then(() => {
            return null;
          })
    })

    GLOBAL.APP_GETDEGREEDISPLAY = () => {
      if (GLOBAL.___degreedisplay == null)
      {
        return AsyncStorage.getItem('__DEGREEDISPLAY')
            .then((value) => {
              if (!value) {
                value = 'c';
              }
              global.___degreedisplay = value;
              return value;
            })
      }
      return global.___degreedisplay;
    }





    GLOBAL.LAST_UPDATED_TIME = ' n/a ';
    GLOBAL.LAST_UPDATED_TIME_UPDATE = (isSilent) => {
      GLOBAL.LAST_UPDATED_TIME = (""+new Date().getHours()).padStart(2, "0") + ":" + (""+new Date().getMinutes()).padStart(2, "0");
      if (isSilent) {
        return;
      }
      GLOBAL.EMIT_LAST_UPDATE_UPDATES();
    }

    GLOBAL.EMIT_DEVICE_UPDATES = (serial) => {
      DeviceEventEmitter.emit(`DEVICE-${serial}`);
    }

    GLOBAL.LISTEN_DEVICE_UPDATES = (serial, fn) => {
      DeviceEventEmitter.addListener(`DEVICE-${serial}`, fn);
    }

    GLOBAL.UNLISTEN_DEVICE_UPDATES = (serial, fn) => {
      DeviceEventEmitter.removeListener(`DEVICE-${serial}`, fn);
    }

    GLOBAL.EMIT_DEVICE_ACK = (serial, json) => {
      // alert(`EMIT DEVICE-ACK-${serial}`);
      GLOBAL.EMIT_DEVICE_ACK_JSON = json;
      DeviceEventEmitter.emit(`DEVICE-ACK-${serial}`);
      GLOBAL.EMIT_DEVICE_ACK_JSON = null;
    }

    GLOBAL.LISTEN_DEVICE_ACK = (serial, fn) => {
      // alert(`LISTEN DEVICE-ACK-${serial}`);
      DeviceEventEmitter.addListener(`DEVICE-ACK-${serial}`, fn);
    }

    GLOBAL.UNLISTEN_DEVICE_ACK = (serial, fn) => {
      DeviceEventEmitter.removeListener(`DEVICE-ACK-${serial}`, fn);
    }

    GLOBAL.EMIT_LAST_UPDATE_UPDATES = () => {
      DeviceEventEmitter.emit(`LAST_UPDATE`);
    }

    GLOBAL.LISTEN_LAST_UPDATE_UPDATES = (fn) => {
      DeviceEventEmitter.addListener(`LAST_UPDATE`, fn);
    }

    GLOBAL.UNLISTEN_LAST_UPDATE_UPDATES = (fn) => {
      DeviceEventEmitter.removeListener(`LAST_UPDATE`, fn);
    }

    GLOBAL.UPDATE_FETCH_WEATHER = (callback) => {
      new APISourcerExternal().getWeatherHk()
          .then((res) => {
            GLOBAL.EMIT_DEVICE_UPDATES()
            callback(res)
          })
          .catch((e) => {
            callback(null)
          })
    }

    GLOBAL.FIRMWARE_GET_LATEST = (callback) => {
      new APISourcerPrelogin().HOHOCARE_Firmware_Get_Latest()
          .then((res) => {
            GLOBAL.EMIT_DEVICE_UPDATES()
            callback(res)
          })
          .catch((e) => {
            callback(null)
          })
    }

    GLOBAL.IDENTIFY_DEVICE_IS_OFFLINE = (it) => {
      return (it.info && (it.info.is_online === false || it.info.is_online === 0))
          || (!it.date || (moment().diff(moment(it.date), 'seconds') > 60*21));
    }

    GLOBAL.STORE = {

      session: null,
      devices: [],
      schedules: [],
      vacation_mode: {},
      schedules_pending_update: false,

      load_session: () => {
        return AsyncStorage.getItem('@session')
            .then((value) => {
              value = value || [];
              var session = JSON.parse(value);
              GLOBAL.STORE.session = session;
              return session;
            })
            .catch((e) => {
              return [];
            })
      },

      save_session: (session) => {
        GLOBAL.STORE.session = session;
        AsyncStorage.setItem('@session', session ? JSON.stringify(session) : null);
      },

      unload_session: () => {
        GLOBAL.STORE.session = null;
        return AsyncStorage.removeItem('@session');
      },



      load_devices: () => {
        return AsyncStorage.getItem('@devices')
            .then((value) => {
              value = value || "[]";
              var items = JSON.parse(value);
              items = items || [];
              for (var k in items)
              {
                items[k].payload = {};
                (items[k].info || {}).is_online = 0;

                for (var m in GLOBAL.STORE.devices)
                {
                  if (GLOBAL.STORE.devices[m].serial === items[k].serial)
                  {
                    items[k].payload = GLOBAL.STORE.devices[m].payload || {};
                    (items[k] && items[k].info || {}).is_online = (GLOBAL.STORE.devices[m] && GLOBAL.STORE.devices[m].info || {}).is_online || 0;
                  }
                }
              }
              GLOBAL.STORE.devices = items;
              APISourcerMqtt.getInstance().subscribeAllDevices();
              return items;
            })
            .catch((e) => {
              return [];
            })
      },

      unload_devices: () => {
        return AsyncStorage.getItem('@devices')
            .then((value) => {
              value = value || [];
              var items = JSON.parse(value);
              APISourcerMqtt.getInstance().unsubscribeAllDevices();
              GLOBAL.STORE.devices = [];
              return items;
            })
            .catch((e) => {
              return [];
            })
      },

      update_devices: (callback) => {
        var items = GLOBAL.STORE.devices;
        AsyncStorage.setItem('@devices', JSON.stringify(items))
            .then(() => {
              if (callback)
              {
                callback();
              }
            });
      },

      update_device_payload: (serial, payload) => {
        var items = GLOBAL.STORE.devices;

        for (var k in items) {
          if (items[k].serial === serial)
          {
            items[k].payload = Object.assign(items[k].payload || {}, payload);
            items[k].date = new Date();
          }
        }

        GLOBAL.LAST_UPDATED_TIME_UPDATE(true);
        GLOBAL.EMIT_DEVICE_UPDATES(serial);
      },

      update_device_info: (serial, info) => {
        var items = GLOBAL.STORE.devices;

        for (var k in items) {
          if (items[k].serial === serial)
          {
            items[k].info = Object.assign(items[k].info || {}, info);
            items[k].date = new Date();
          }
        }

        GLOBAL.LAST_UPDATED_TIME_UPDATE(true);
        GLOBAL.EMIT_DEVICE_UPDATES(serial);
      },

      get_device: (serial) => {
        var items = GLOBAL.STORE.devices;

        for (var k in items) {
          if (items[k].serial === serial)
          {
            return items[k];
          }
        }
        return null
      },

      add_device: (serial, access_token, options) => {
        var items = GLOBAL.STORE.devices;

        var is_unique = true;

        for (var k in items) {
          if (items[k].serial === serial)
          {
            is_unique = false;
            break;
          }
        }

        if (is_unique)
        {
          items.push({
            serial: serial,
            access_token: access_token,
            ssid: serial,
            title: serial,
            alias: serial,
            host_model: 1,
            vacation_mode: false,
            payload: {

              p: 0,
              m: 0,
              f: 1,
              t: 25,
              sv: 0,
              sh: 0,
              ct: 1100,
              ot: 1200,
              otio: 0,
              dht_t: '--',
              dht_h: '--',

            },
          })
        }

        APISourcerMqtt.getInstance().subscribeDevice(serial);
        AsyncStorage.setItem('@devices', JSON.stringify(items));
        GLOBAL.LAST_UPDATED_TIME_UPDATE(true);
        GLOBAL.EMIT_DEVICE_UPDATES(serial);

        if (options && options.is_probing)
        {
          return;
        }

        return new APISourcerPreference().save()
            .then(() => {

              return new APISourcerPreference().HOHOCARE_User_Device_List({
                serial: serial,
              })
            })
            .then(() => {
              return null;
            })
            .catch((e) => {
              return null;
            })
      },

      remove_device: (serial) => {
        var items = GLOBAL.STORE.devices;

        for (var k = items.length - 1; k >= 0; k--) {
          if (items[k].serial === serial)
          {
            items.splice(k, 1);
          }
        }

        APISourcerMqtt.getInstance().unsubscribeDevice(serial);
        AsyncStorage.setItem('@devices', JSON.stringify(items));
        GLOBAL.LAST_UPDATED_TIME_UPDATE(true);
        GLOBAL.EMIT_DEVICE_UPDATES(serial);
        GLOBAL.STORE.remove_schedules(serial);
        new APISourcerPreference().save()
            .then(() => {

            })
            .catch((e) => {

            })
      },







      load_schedules: () => {
        return AsyncStorage.getItem('@schedules')
            .then((value) => {
              value = value || [];
              var items = JSON.parse(value);
              GLOBAL.STORE.schedules = items;
              return items;
            })
            .catch((e) => {
              return [];
            })
      },

      unload_schedules: () => {
        return AsyncStorage.getItem('@schedules')
            .then((value) => {
              value = value || [];
              var items = JSON.parse(value);
              GLOBAL.STORE.schedules = [];
              return items;
            })
            .catch((e) => {
              return [];
            })
      },

      update_schedules: (callback) => {
        var items = GLOBAL.STORE.schedules;
        AsyncStorage.setItem('@schedules', JSON.stringify(items))
            .then(() => {
              if (callback)
              {
                callback();
              }
            });
      },

      update_schedule_payload: (index, start_time, weekday, action, disabled) => {
        var items = GLOBAL.STORE.schedules;
        var k = index;

        items[k].action = action;
        items[k].start_time = start_time;
        items[k].weekday = weekday;
        items[k].disabled = disabled;

        GLOBAL.STORE.schedules_pending_update = true;
        GLOBAL.LAST_UPDATED_TIME_UPDATE(true);
      },

      apply_schedule: () => {
        GLOBAL.STORE.schedules_pending_update = false;

        var devices = GLOBAL.STORE.devices;
        var schedules = GLOBAL.STORE.schedules;
        for (var k in devices)
        {
          var serial = devices[k].serial;
          var data = [];

          for (var s in schedules)
          {
            if (schedules[s].serial === serial)
            {
              var schedule = schedules[s];

              var {
                weekday,
                disabled,
                action,
                start_time,
              } = schedule;

              if (!disabled)
              {
                var seconds = start_time;
                var w = weekday;
                if (weekday === 'Su') {w = 0}
                if (weekday === 'Mo') {w = 1}
                if (weekday === 'Tu') {w = 2}
                if (weekday === 'We') {w = 3}
                if (weekday === 'Th') {w = 4}
                if (weekday === 'Fr') {w = 5}
                if (weekday === 'Sa') {w = 6}
                var hhmm = ((Math.floor(seconds / 60 / 60) + "").padStart(2, "0")) + "" + ((Math.floor(seconds / 60 % 60) + "").padStart(2, "0"));

                var sp = action.power ? '1' : '0';
                var sm = action.mode;
                if (sm === 'auto') {sm = 0}
                if (sm === 'heat') {sm = 4}
                if (sm === 'cool') {sm = 3}
                if (sm === 'fan') {sm = 6}
                if (sm === 'dehum') {sm = 2}
                var st = action.temp;
                var sf = action.fan;
                if (sf === '1') {sf = 1}
                if (sf === '2') {sf = 2}
                if (sf === '3') {sf = 3}
                if (sf === '4') {sf = 4}
                if (sf === '5') {sf = 5}
                if (sf === 'auto') {sf = 8}
                if (sf === 'quiet') {sf = 9}

                var sw = action.swing;
                var sh = '0';
                var sv = '0';
                if (sw == 'Off') {
                  sh = 0;
                  sv = 0;
                }
                if (sw == 'Horizontal') {
                  sh = 1;
                  sv = 0;
                }
                if (sw == 'Vertical') {
                  sh = 0;
                  sv = 1;
                }
                if (sw == '3D mode') {
                  sh = 1;
                  sv = 1;
                }

                var ds = '';
                ds += w;
                ds += hhmm;
                ds += sp;
                ds += sm;
                ds += st;
                ds += sf;
                ds += sh;
                ds += sv;
                ds += 'XXXXXXXXXXXX';

                data.push(ds);
              }
            }
          }

          data = data.sort((a, b) => {
            if(a < b) { return -1; }
            if(a > b) { return 1; }
            return 0;
          })

          APISourcerMqtt.getInstance().publishCommandAC(serial, 'set_schedule', {
            slots: data.length,
            data: data.join(''),
          })
        }

        new APISourcerPreference().save()
            .then(() => {

            })
            .catch((e) => {

            })
      },

      add_schedule: (item) => {
        var items = GLOBAL.STORE.schedules;
        items.push(item)
        GLOBAL.STORE.schedules_pending_update = true;
        AsyncStorage.setItem('@schedules', JSON.stringify(items));
        GLOBAL.LAST_UPDATED_TIME_UPDATE(true);
      },

      get_schedule_template_new: (serial, weekday, start_time) => {
        return {
          serial: serial,
          weekday: weekday,
          start_time: start_time || ((new Date().getMinutes() * 60) + (new Date().getHours() * 60 * 60)),
          disabled: false,
          action: {power: true, temp: 25, mode: 'auto', fan: 'auto', swing: 'Off'},
        }
      },

      remove_schedule: (index) => {
        var items = GLOBAL.STORE.schedules;
        var k = index;
        items.splice(k, 1);
        GLOBAL.STORE.schedules_pending_update = true;
        AsyncStorage.setItem('@schedules', JSON.stringify(items));
        GLOBAL.LAST_UPDATED_TIME_UPDATE(true);
      },

      remove_schedules: (serial) => {
        var items = GLOBAL.STORE.schedules;

        for (var k = items.length - 1; k >= 0; k--) {
          if (items[k].serial === serial)
          {
            items.splice(k, 1);
          }
        }

        GLOBAL.STORE.schedules_pending_update = true;
        AsyncStorage.setItem('@schedules', JSON.stringify(items));
        GLOBAL.LAST_UPDATED_TIME_UPDATE(true);
      },














      load_vacation_mode: () => {
        return AsyncStorage.getItem('@vacation_mode')
            .then((value) => {
              value = value || {};
              var items = JSON.parse(value);
              GLOBAL.STORE.vacation_mode = items;
              return items;
            })
            .catch((e) => {
              return {};
            })
      },

      unload_vacation_mode: () => {
        return AsyncStorage.getItem('@vacation_mode')
            .then((value) => {
              value = value || {};
              var items = JSON.parse(value);
              GLOBAL.STORE.vacation_mode = {};
              return items;
            })
            .catch((e) => {
              return {};
            })
      },

      apply_vacation_mode: () => {
        new APISourcerPreference().save()
            .then(() => {

            })
            .catch((e) => {

            })
      },

      update_vacation_mode: (is_enabled, callback) => {
        GLOBAL.STORE.vacation_mode = GLOBAL.STORE.vacation_mode || {};
        var items = GLOBAL.STORE.vacation_mode || {};
        items.is_enabled = is_enabled;
        AsyncStorage.setItem('@vacation_mode', JSON.stringify(items))
            .then(() => {
              if (callback)
              {
                callback();
              }
            });
      },

      update_vacation_mode_for_device: (serial, is_enabled, callback) => {
        var devices = GLOBAL.STORE.devices;
        for (var k in devices)
        {
          var device = devices[k];
          if (device.serial === serial)
          {
            device.vacation_mode = is_enabled;
            device.date = new Date();
          }
        }
        GLOBAL.LAST_UPDATED_TIME_UPDATE(true);
        GLOBAL.EMIT_DEVICE_UPDATES(serial);
      },

      remove_vacation_mode: (callback) => {
        var items = GLOBAL.STORE.vacation_mode;
        items.is_enabled = false;
        AsyncStorage.setItem('@vacation_mode', JSON.stringify(items))
            .then(() => {
              if (callback)
              {
                callback();
              }
            });
      },








    }


    GLOBAL.IMAGE = {
      device_1_n : require('./device_1_n.png'),
      device_1_o : require('./device_1_o.png'),
      device_2_n : require('./device_2_n.png'),
      device_2_o : require('./device_2_o.png'),
      icon_model_ac : require('./icon_model_ac.png'),
      ic_connect_line : require('./ic_connect_line.png'),

      // device / ac
      d2bros_logo : require('./d2bros_logo.png'),
      logo_hohocare : require('./logo_hohocare.png'),
      wifi_connect_tutorial_d2bros : require('./wifi_connect_tutorial_d2bros.png'),
      d_ac_ic_auto : require('./ac/ac_ic_auto.png'),
      d_ac_ic_circle_btn_left : require('./ac/ac_ic_circle_btn_left.png'),
      d_ac_ic_circle_btn_left_full : require('./ac/ac_ic_circle_btn_left_full.png'),
      d_ac_ic_circle_btn_minus : require('./ac/ac_ic_circle_btn_minus.png'),
      d_ac_ic_circle_btn_plus : require('./ac/ac_ic_circle_btn_plus.png'),
      d_ac_ic_circle_btn_right : require('./ac/ac_ic_circle_btn_right.png'),
      d_ac_ic_circle_btn_right_full : require('./ac/ac_ic_circle_btn_right_full.png'),
      d_ac_ic_circle_oval_control : require('./ac/ac_ic_circle_oval_control.png'),
      d_ac_ic_circle_oval_full_bg : require('./ac/ac_ic_circle_oval_full_bg.png'),
      d_ac_ic_circle_oval_upper : require('./ac/ac_ic_circle_oval_upper.png'),
      d_ac_ic_circle_oval_upper_auto : require('./ac/ac_ic_circle_oval_upper_auto.png'),
      d_ac_ic_circle_oval_upper_cool : require('./ac/ac_ic_circle_oval_upper_cool.png'),
      d_ac_ic_circle_oval_upper_eco : require('./ac/ac_ic_circle_oval_upper_eco.png'),
      d_ac_ic_circle_oval_upper_fan : require('./ac/ac_ic_circle_oval_upper_fan.png'),
      d_ac_ic_circle_oval_upper_dehum : require('./ac/ac_ic_circle_oval_upper_dehum.png'),
      d_ac_ic_circle_oval_upper_heat : require('./ac/ac_ic_circle_oval_upper_heat.png'),
      d_ac_ic_circle_oval_upper_off : require('./ac/ac_ic_circle_oval_upper_off.png'),
      d_ac_ic_circle_oval_full_auto : require('./ac/ac_ic_circle_oval_full_auto.png'),
      d_ac_ic_circle_oval_full_cool : require('./ac/ac_ic_circle_oval_full_cool.png'),
      d_ac_ic_circle_oval_full_heat : require('./ac/ac_ic_circle_oval_full_heat.png'),
      d_ac_ic_fan_power_1 : require('./ac/ac_ic_fan_power_1.png'),
      d_ac_ic_fan_power_2 : require('./ac/ac_ic_fan_power_2.png'),
      d_ac_ic_fan_power_3 : require('./ac/ac_ic_fan_power_3.png'),
      d_ac_ic_fan_power_4 : require('./ac/ac_ic_fan_power_4.png'),
      d_ac_ic_fan_power_5 : require('./ac/ac_ic_fan_power_5.png'),
      d_ac_ic_fan_power_small_ic : require('./ac/ac_ic_fan_power_small_ic.png'),
      d_ac_ic_minus : require('./ac/ac_ic_minus.png'),
      d_ac_ic_mode_cool_n : require('./ac/ac_ic_mode_cool_n.png'),
      d_ac_ic_mode_cool_o : require('./ac/ac_ic_mode_cool_o.png'),
      d_ac_ic_mode_dehum_n : require('./ac/ac_ic_mode_dehum_n.png'),
      d_ac_ic_mode_dehum_o : require('./ac/ac_ic_mode_dehum_o.png'),
      d_ac_ic_mode_fan_n : require('./ac/ac_ic_mode_fan_n.png'),
      d_ac_ic_mode_fan_o : require('./ac/ac_ic_mode_fan_o.png'),
      d_ac_ic_mode_heat_o : require('./ac/ac_ic_mode_heat_o.png'),
      d_ac_ic_mode_eco_o : require('./ac/ac_ic_mode_eco_o.png'),
      d_ac_ic_mode_heat_n : require('./ac/ac_ic_mode_heat_n.png'),
      d_ac_ic_mode_auto_o : require('./ac/ac_ic_mode_auto_o.png'),
      d_ac_ic_mode_eco_n : require('./ac/ac_ic_mode_eco_n.png'),
      d_ac_ic_mode_auto_n : require('./ac/ac_ic_mode_auto_n.png'),
      d_ac_ic_plus : require('./ac/ac_ic_plus.png'),
      d_ac_ic_power_off_n : require('./ac/ac_ic_power_off_n.png'),
      d_ac_ic_power_off_o : require('./ac/ac_ic_power_off_o.png'),
      d_ac_ic_power_on_n : require('./ac/ac_ic_power_on_n.png'),
      d_ac_ic_power_on_o : require('./ac/ac_ic_power_on_o.png'),
      d_ac_ic_room_hum : require('./ac/ac_ic_room_hum.png'),
      d_ac_ic_room_temp : require('./ac/ac_ic_room_temp.png'),
      d_ac_ic_swipe_off : require('./ac/ac_ic_swipe_off.png'),
      d_ac_ic_swipe_on : require('./ac/ac_ic_swipe_on.png'),
      d_ac_ic_time : require('./ac/ac_ic_time.png'),
      d_ac_ic_env_temp : require('./ac/ac_ic_env_temp.png'),

      d_ac_ic_swing_off_n : require('./ac/ac_ic_swing_off_n.png'),
      d_ac_ic_swing_3D_n : require('./ac/ac_ic_swing_3D_n.png'),
      d_ac_ic_swing_3D_o : require('./ac/ac_ic_swing_3D_o.png'),
      d_ac_ic_swing_vertical_n : require('./ac/ac_ic_swing_vertical_n.png'),
      d_ac_ic_swing_vertical_o : require('./ac/ac_ic_swing_vertical_o.png'),
      d_ac_ic_swing_horizontal_n : require('./ac/ac_ic_swing_horizontal_n.png'),
      d_ac_ic_swing_horizontal_o : require('./ac/ac_ic_swing_horizontal_o.png'),
      d_ac_ic_swing_off_o : require('./ac/ac_ic_swing_off_o.png'),

      d_ac_ic_new_device : require('./ac/ac_ic_new_device.png'),

      ic_refresh : require('./ic_refresh.png'),
      ic_device_offline_o : require('./ic_device_offline_o.png'),
      ic_firmware_update_o : require('./ic_firmware_update_o.png'),
      ic_vacation_mode_o : require('./ic_vacation_mode_o.png'),

      icon_FAQ : require('./icon_FAQ.png'),
      icon_TAC : require('./icon_TAC.png'),
      icon_logout : require('./icon_logout.png'),
      icon_setting : require('./icon_setting.png'),
      icon_usermanual : require('./icon_usermanual.png'),
    }
  }
}

module.exports = Images;
