import React, { Component } from 'react';
import { Container, Header, Left, Right, Body, Title, Accordion, Button, Icon, Content, List, ListItem, Text } from 'native-base';
import { Image, TouchableOpacity, View } from 'react-native';
import AccordionHeader from './components/AccordionHeader'
import AccordionItem from './components/AccordionItem'
import BasePage from "../../base/BasePage";
import { withNavigationFocus } from "react-navigation";
import APISourcerMqtt from '../../../apis/APISourcerMqtt';
import BaseNavigationBar from '../../base/BaseNavigationBar';
import APISourcerPrelogin from '../../../apis/APISourcerPrelogin';
import APISourcerPreference from '../../../apis/APISourcerPreference';


class HomeDashboardPage extends BasePage {
  constructor(props)
  {
    super(props);
    this.state = {
      items: [],
    }

    this.refresh = this.refresh.bind(this);
  }

  componentDidMount(): void {
    this.refresh();
    new APISourcerPreference().HOHOCARE_User_Device_List()
        .then((res) => {
          this.forceUpdate()
        })
        .catch((e) => {
        })
    GLOBAL.LISTEN_LAST_UPDATE_UPDATES(this.refresh);
    // GLOBAL.STORE.add_device("A0000002", "UVRBd01EQXdNREU9");
  }

  componentWillAppear() {
    super.componentWillAppear();
    this.refresh();
    GLOBAL.LISTEN_LAST_UPDATE_UPDATES(this.refresh);
  }

  componentWillDisappear() {
    super.componentWillDisappear();
    GLOBAL.UNLISTEN_LAST_UPDATE_UPDATES(this.refresh);
  }

  componentWillUnmount(): void {

  }

  refresh()
  {
    var isUpdate = false;
    if ((this.state.items || []).length != (GLOBAL.STORE.devices || []).length)
    {
      isUpdate = true;
    }
    this.state.items = GLOBAL.STORE.devices;
    this.vbar && this.vbar.forceUpdate();
    this.lbar && this.lbar.forceUpdate();
    if (isUpdate)
    {
      this.forceUpdate();
    }
  }


  _renderHeader(item, expanded)
  {
    return (
        <AccordionHeader item={item} expanded={expanded}/>
    )
  }


  _renderContent(item)
  {
    return (
        <AccordionItem
            onPressFirmwareUpdate={(it, cell) => {
              GLOBAL.CONFIG_ITEM = it;
              if (APISourcerPrelogin.D2Bros_Firmware_Update_Method(item) === 1)
              {
                this.props.navigation.navigate("ConfigureSettingsOTAPage1", {item: it, parent: this})
              }
              if (APISourcerPrelogin.D2Bros_Firmware_Update_Method(item) === 2)
              {
                this.props.navigation.navigate("ConfigureSettingsOTAPage2", {item: it, parent: this})
              }
            }}
            onPressItem={(it, cell) => {
              if (cell)
              {
                cell.sleep();
              }
              GLOBAL.CONFIG_ITEM = it;
              GLOBAL.HOME_ITEM = this;
              this.props.navigation.navigate("Configure", {}, {
                type: "Navigate",
                routeName: "ControlAirConditionerPage",
              });
            }}
            item={item}
            items={item.items}/>
    )
  }

  renderInner() {
    return (
        <Container>

          <BaseNavigationBar {...this.props}
              isMenu
              title={`Home`.i18n()}
              icon_right={
                <Right>
                  <Button transparent onPress={() => {
                    this.showLoading()
                    GLOBAL.STORE.load_devices()
                        .then(() => {
                          return GLOBAL.STORE.load_schedules()
                        })
                        .then(() => {
                          return GLOBAL.STORE.load_vacation_mode()
                        })
                        .then(() => {
                          return new APISourcerPreference().HOHOCARE_User_Device_List()
                        })
                        .then((res) => {
                          this.forceUpdate()
                        })
                        .then(() => {
                          this.setState({
                            is_ready: true,
                            is_postlogin: true,
                          }, () => {
                            setTimeout(() => {
                              this.hideLoading();
                            }, 1000)
                          })
                        })
                        .catch((e) => {
                          setTimeout(() => {
                            this.hideLoading();
                          }, 1000)
                        })
                  }}>
                    <Image source={GLOBAL.IMAGE.ic_refresh} style={{width: 32, height: 32}}/>
                  </Button>
                </Right>
              }
          />

          <Content>

            <HDPVacationBar ref={(v) => {this.vbar = v}}/>
            <HDPLastUpdateBar ref={(v) => {this.lbar = v}}/>

            {
              this.state.items.length > 0 ? (
                  <Accordion
                      dataArray={[{
                        title: `${`All units`.i18n()} (${this.state.items.length})`,
                        items: this.state.items,
                      }]}
                      animation={true}
                      expanded={0}
                      renderHeader={this._renderHeader.bind(this)}
                      renderContent={this._renderContent.bind(this)}
                  />
              ) : (
                  <View style={{height: 400, flex: 1, alignItems: `center`, justifyContent: `center`}}>
                    <TouchableOpacity
                        onPress={() => {
                          this.props.navigation.navigate("ActivationBeforeStartPage");
                        }}
                        style={{flexDirection: `column`, flex: 1, alignItems: `center`, justifyContent: `center`}}>
                      <Image source={GLOBAL.IMAGE.d_ac_ic_new_device} style={{width: 200, height: 200}}/>
                      <Text style={{color: '#999', padding: 15}}>
                        {`Add your first D2Bros device by tapping here`.i18n()}
                      </Text>
                    </TouchableOpacity>
                  </View>
              )
            }

          </Content>

        </Container>
    );
  }
}


class HDPVacationBar extends React.Component
{
  render()
  {
    if (GLOBAL.STORE.vacation_mode.is_enabled)
    {
      return (
          <View style={{alignItems: `center`, justifyContent: `center`,
            backgroundColor: '#57595d', padding: 5, flexDirection: `row`}}>
            <Image source={GLOBAL.IMAGE.ic_vacation_mode_o} style={{width: 22, height: 22}}/>
            <Text style={{color: `#fff`, paddingHorizontal: 5, textAlign: `center`}}>{`Vacation mode is active`.i18n()}</Text>
          </View>
      )
    }
    else
    {
      return null;
    }
  }
}
class HDPLastUpdateBar extends React.Component
{
  render()
  {
    return <Text note style={{padding: 5, textAlign: `center`}}>— {`Last Updated at`.i18n()} {GLOBAL.LAST_UPDATED_TIME} —</Text>
  }
}




export default withNavigationFocus(HomeDashboardPage)
