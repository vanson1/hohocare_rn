import React, { Component } from 'react';
import { ScrollView, Image } from "react-native";
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3, Form, Item, Label, Input,
} from "native-base";
import BasePage from "../../base/BasePage";
import WifiManager from "react-native-wifi-reborn";
import BaseNavigationBar from '../../base/BaseNavigationBar';
import APISourcerPreference from '../../../apis/APISourcerPreference';

export default class ConfigureSettingsChangeAliasPage extends BasePage {

  constructor(props)
  {
    super(props);
    this.state = {
    }
  }

  componentDidMount(): void {
    this.load();
  }



  load()
  {
    this.showLoading()
    new APISourcerPreference().HOHOCARE_User_Device_List({
      serial: this.props.navigation.state.params.item.serial,
    })
        .then((res) => {
          this.hideLoading()
          if (res.data && res.data.items
              && res.data.items.length > 0)
          {
            this.setState({
              alias: res.data.items[0].alias,
            });
          }
        })
        .catch((e) => {
          alert(e.message);
          this.hideLoading()
        })
  }

  save()
  {
    this.showLoading()
    new APISourcerPreference().HOHOCARE_User_Device_Add({
      alias: this.state.alias,
      serial: this.props.navigation.state.params.item.serial,
    })
        .then((res) => {
          if (res && res.success)
          {
            this.props.navigation.state.params.item.alias = this.state.alias;
            new APISourcerPreference().save()
                .then((res2) => {
                  this.hideLoading()
                  GLOBAL.STORE.update_devices();
                  this.props.navigation.state.params.parent.forceUpdate();
                  this.props.navigation.pop();
                })
                .catch((e) => {
                  alert(e.message);
                  this.hideLoading()
                })

          }
        })
        .catch((e) => {
          alert(e.message);
          this.hideLoading()
        })
  }



  renderInner() {

    return (
        <Container>

          <BaseNavigationBar {...this.props}
              isBack
              title={`Configure`.i18n()}
          />


          <Content padder>

            <H1 style={{color: '#586C78', marginTop: 60, marginBottom: 30, textAlign: `center`}}>{`Change alias`.i18n()}</H1>

            <Form>
              <Item>
                <Label>{`Alias`.i18n()}</Label>
                <Input placeholder={`${`Default`.i18n()}: ${this.props.navigation.state.params.item.serial}`} value={this.state.alias} autoCapitalize={false} autoCompleteType={'off'} onChangeText={(val) => {this.setState({alias: val})}} />
              </Item>
            </Form>


            <View style={{height: 50}}/>

            <Button
                onPress={() => {
                  this.save();
                }}
                style={{backgroundColor: '#4fadf9', justifyContent: `center`}}>
              <H3 style={{color: '#fff'}}>{`Confirm`.i18n()}</H3>
            </Button>


          </Content>

        </Container>
    );
  }
}
