import React, { Component } from 'react';
import { Container, Header, Left, Right, Body, Thumbnail, Title, Accordion, Button, Icon, Content, List, ListItem, Text } from 'native-base';
import { Alert, Image, TouchableOpacity, View, FlatList } from 'react-native';
import BasePage from "../../base/BasePage";
import { withNavigationFocus } from "react-navigation";
import BaseNavigationBar from '../../base/BaseNavigationBar';
import APISourcerFamily from '../../../apis/APISourcerFamily';
import moment from 'moment'
import APISourcerMqtt from '../../../apis/APISourcerMqtt';

class FamilyDetailsPage extends BasePage {
  constructor(props)
  {
    super(props);
    this.state = {
      family: null,
      family_users: [],
      family_pending_actions: [],
      is_active: 1,
    }
  }

  componentDidMount(): void {
    var mqtt = APISourcerMqtt.getInstance();
    mqtt.setGateway_ack_callback((json) => {

      var {
        t,
        s,
        gs,
        v,
        sd,
      } = json;

      this.state.family_subdevices = this.state.family_subdevices || [];

      for (var k in this.state.family_subdevices)
      {
        var it = this.state.family_subdevices[k];
        if (it.type === t)
        {
          if (it.serial === s)
          {

            this.state.family_gw_devices = this.state.family_gw_devices || [];

            for (var m in this.state.family_gw_devices)
            {
              var itgw = this.state.family_gw_devices[m];

              if (itgw.serial === gs)
              {
                itgw.status = "在線";
                itgw.online = true;
                break;
              }
            }


            it.online = true;
            it.status = sd;
            it.battery = (v && Number(v)) || null;
            if (it.battery)
            {
              it.battery = Math.floor((1 - (4000 - it.battery) / (4000 - 3600)) * 100);
              if (it.battery > 100)
              {
                it.battery = 100;
              }
              var is_power_low = false;
              if (it.battery < 20)
              {
                is_power_low = true;
              }
              it.battery += "%";
              if (is_power_low)
              {
                it.battery += " (請更換電池)";
              }
            }
          }
        }
      }

      this.forceUpdate();
    })

    this.refresh()
  }


  componentWillUnmount()
  {
    this.state.family_gw_devices = this.state.family_gw_devices || [];
    for (var k in this.state.family_gw_devices)
    {
      var it = this.state.family_gw_devices[k];
      APISourcerMqtt.getInstance().unsubscribeGateway(it.type, it.serial);
    }
  }

  refresh()
  {
    this.showLoading()
    new APISourcerFamily().HOHOCARE_Family_FindByCode({
      family_id: this.props.navigation.state.params._id,
    })
        .then((res) => {
          this.setState({
            family: res.data,
            is_ready: true,
          })


          return new APISourcerFamily().ADMIN_HOHOCARE_Family_ListFamilyUsers({
            _id: res.data._id,
            role: `caretaker`,
          })
        })


        .then((res) => {
          var family_users = [];
          if (res.data.items)
          {
            for (var k in res.data.items)
            {
              var it = res.data.items[k];
              family_users.push(Object.assign({}, it.user, {
                online: true,
                room: `--`,
                lnglat: [
                  114.1930105,
                  22.3187002,
                ],
                healthrecords: {
                  hrs: [],
                  bps: [],
                }
              }))
            }
          }
          // var family_users = Object.assign({}, {
          //   nickname: `鄭潔 女士`,
          //   avatar_url: `http://dev.oicomg.in/hc_public/av4.png`,
          //   online: true,
          //   indoor: false,
          //   room: '--',
          //   phone_number: '51183837',
          //   phone_country_code: '852',
          //   lnglat: [
          //     114.1930105,
          //     22.3187002,
          //   ],
          //   healthrecords: {
          //     hrs: [
          //       {
          //         created_at: moment().subtract(1, 'day').toDate(),
          //         m1: 94,
          //       },
          //       {
          //         created_at: moment().subtract(2, 'day').toDate(),
          //         m1: 86,
          //       },
          //       {
          //         created_at: moment().subtract(3, 'day').toDate(),
          //         m1: 85,
          //       },
          //       {
          //         created_at: moment().subtract(4, 'day').toDate(),
          //         m1: 84,
          //       },
          //       {
          //         created_at: moment().subtract(5, 'day').toDate(),
          //         m1: 92,
          //       },
          //       {
          //         created_at: moment().subtract(6, 'day').toDate(),
          //         m1: 90,
          //       },
          //       {
          //         created_at: moment().subtract(7, 'day').toDate(),
          //         m1: 88,
          //       },
          //     ],
          //     bps: [
          //       {
          //         created_at: moment().subtract(1, 'day').toDate(),
          //         m1: 105,
          //         m2: 82,
          //         h1: 122,
          //         h2: 78,
          //       },
          //       {
          //         created_at: moment().subtract(2, 'day').toDate(),
          //         m1: 115,
          //         m2: 84,
          //         h1: 122,
          //         h2: 78,
          //       },
          //       {
          //         created_at: moment().subtract(3, 'day').toDate(),
          //         m1: 105,
          //         m2: 76,
          //         h1: 122,
          //         h2: 78,
          //       },
          //       {
          //         created_at: moment().subtract(4, 'day').toDate(),
          //         m1: 118,
          //         m2: 73,
          //         h1: 122,
          //         h2: 78,
          //       },
          //       {
          //         created_at: moment().subtract(5, 'day').toDate(),
          //         m1: 117,
          //         m2: 82,
          //         h1: 122,
          //         h2: 78,
          //       },
          //       {
          //         created_at: moment().subtract(6, 'day').toDate(),
          //         m1: 118,
          //         m2: 82,
          //         h1: 122,
          //         h2: 78,
          //       },
          //       {
          //         created_at: moment().subtract(7, 'day').toDate(),
          //         m1: 117,
          //         m2: 73,
          //         h1: 122,
          //         h2: 78,
          //       },
          //     ],
          //   },
          // })
          this.setState({
            activities: [
              // {
              //   created_at: moment().subtract(150, 'minutes').toDate(),
              //   desc: `陈秀慧 女士 已回到院舍。`,
              //   avatar_url: `http://dev.oicomg.in/hc_public/av1.png`,
              // },
              //
              // {
              //   created_at: moment().subtract(350, 'minutes').toDate(),
              //   desc: `陈秀慧 女士 已離開院舍。`,
              //   avatar_url: `http://dev.oicomg.in/hc_public/av1.png`,
              // },
              //
              // {
              //   created_at: moment().subtract(630, 'minutes').toDate(),
              //   desc: `收到新語音指令：鄭潔 女士 - 未知`,
              //   avatar_url: `http://dev.oicomg.in/hc_public/av4.png`,
              // },

              // {
              //   created_at: moment().subtract(630, 'minutes').toDate(),
              //   desc: `鄭潔 女士 已離開院舍超過 1 公里範圍。`,
              //   avatar_url: `http://dev.oicomg.in/hc_public/av4.png`,
              // },
              //
              // {
              //   created_at: moment().subtract(1230, 'minutes').toDate(),
              //   desc: `鄭潔 女士 已離開院舍。`,
              //   avatar_url: `http://dev.oicomg.in/hc_public/av4.png`,
              // },
              //
              // {
              //   created_at: moment().subtract(12250, 'minutes').toDate(),
              //   desc: `鄭潔 女士 已回到院舍。`,
              //   avatar_url: `http://dev.oicomg.in/hc_public/av4.png`,
              // },
            ],
            family_users: family_users,
            gateways: [
              // {
              //   last_updated_at: moment().subtract(150, 'minutes').toDate(),
              //   product_cat: `gw_wifi`,
              //   product_id: `101`,
              //   product_desc: `Wifi Gateway`,
              //   desc: `W00000001`,
              //   alias: `客廳`,
              //   avatar_url: `http://dev.oicomg.in/hc_public/mgs.png`,
              //   online: true,
              //   status: `ONLINE`,
              // },
              //
              // {
              //   last_updated_at: moment().subtract(150, 'minutes').toDate(),
              //   product_cat: `s_pir`,
              //   product_id: `102`,
              //   product_desc: `PIR motion sensor`,
              //   desc: `M00000001`,
              //   alias: `客廳`,
              //   avatar_url: `http://dev.oicomg.in/hc_public/mms.png`,
              //   online: true,
              //   status: `PRESENT`,
              // },
              //
              // {
              //   last_updated_at: moment().subtract(350, 'minutes').toDate(),
              //   product_cat: `s_pir`,
              //   product_id: `102`,
              //   product_desc: `PIR motion sensor`,
              //   desc: `M00000002`,
              //   alias: `廚房`,
              //   avatar_url: `http://dev.oicomg.in/hc_public/mms.png`,
              //   online: true,
              //   status: `ABSENT`,
              // },
              //
              // {
              //   last_updated_at: moment().subtract(630, 'minutes').toDate(),
              //   product_cat: `s_pir`,
              //   product_id: `102`,
              //   product_desc: `PIR motion sensor`,
              //   desc: `M00000003`,
              //   alias: `睡房`,
              //   avatar_url: `http://dev.oicomg.in/hc_public/mms.png`,
              //   online: true,
              //   status: `ABSENT`,
              // },
              //
              // {
              //   last_updated_at: moment().subtract(1230, 'minutes').toDate(),
              //   product_cat: `s_door`,
              //   product_id: `103`,
              //   product_desc: `Door sensor`,
              //   desc: `D00000001`,
              //   alias: `大門`,
              //   avatar_url: `http://dev.oicomg.in/hc_public/mds.png`,
              //   online: true,
              //   status: `CLOSED`,
              // },

            ],
          })

          return new APISourcerFamily().ADMIN_HOHOCARE_Family_ListFamilyPendingActions({
            _id: res.data._id,
          })
        })



        .then((res) => {
          this.hideLoading()
          this.setState({
            family_pending_actions: res.data.items || [],
          })

          return new APISourcerFamily().HOHOCARE_IOT_ListMyFamilyDevices({
            family_id: this.props.navigation.state.params._id,
          })
        })


        .then((res) => {
          this.setState({
            family_gw_devices: res.data.items || [],
            family_devices: [].concat(res.data.items || []),
          }, () => {
            for (var k in this.state.family_gw_devices)
            {
              var it = this.state.family_gw_devices[k];
              APISourcerMqtt.getInstance().subscribeGateway(it.type, it.serial);
            }
          })

          return new APISourcerFamily().HOHOCARE_IOT_ListMyFamilySubdevices({
            family_id: this.props.navigation.state.params._id,
          })
        })


        .then((res) => {
          this.setState({
            family_subdevices: res.data.items || [],
            family_devices: this.state.family_devices.concat(res.data.items || []),
          })
        })



        .catch((e) => {
          this.hideLoading()
        })
  }


  componentWillUnmount(): void {

  }

  renderInner() {
    return (
        <Container>

          <BaseNavigationBar {...this.props}
                             isBack
                             title={`Family`.i18n()}
          />

          <Content>

            {
              this.state.is_ready && this.state.family ? (

                  <ListItem button style={{paddingBottom: 16}} onPress={() => {

                  }}>
                    <View style={{flexDirection: `column`, flex: 1}}>
                      <View style={{flexDirection: `row`}}>

                        <View style={{width: 32, height: 32, alignItems: 'center'}}>
                          <Icon name={`home`} fontSize={15}/>
                        </View>
                        <View style={{width: 32}}>
                        </View>
                        <Text style={{flex: 1, fontSize: 16}}>{this.state.family.desc}</Text>
                        <View style={{width: 32}}>
                        </View>
                        <View style={{width: 16, height: 16, backgroundColor: '#55ff55', borderRadius: 8}}>
                        </View>

                      </View>
                      <View style={{height: 16}}>
                      </View>
                      <View style={{flexDirection: `row`}}>

                        <Text style={{flex: 1, fontSize: 16}}>{`溫度: 26˚C`}</Text>
                        <View style={{width: 32}}>
                        </View>
                        <Text style={{flex: 1, fontSize: 16}}>{`濕度: 72%`}</Text>
                        <View style={{width: 32}}>
                        </View>


                      </View>

                    </View>
                  </ListItem>

              ) : null
            }

            <View style={{flexDirection: `row`}}>
              <Button
                  onPress={() => {
                    this.setState({
                      is_active: 1,
                    })
                  }}
                  info ={this.state.is_active === 1}
                  transparent={this.state.is_active !== 1}
                  style={{flex: 1, alignItems: `center`, flexDirection: `row`, justifyContent: `center`}}>
                <Text>最新通知</Text>
              </Button>

              <Button
                  onPress={() => {
                    this.setState({
                      is_active: 2,
                    })
                  }}

                  info ={this.state.is_active === 2}
                  transparent={this.state.is_active !== 2}
                  style={{flex: 1.2, alignItems: `center`, flexDirection: `row`, justifyContent: `center`}}>
                <Text>老人家列表</Text>
              </Button>

              <Button
                  onPress={() => {
                    this.setState({
                      is_active: 3,
                    })
                  }}

                  info ={this.state.is_active === 3}
                  transparent={this.state.is_active !== 3}
                  style={{flex: 1.5, alignItems: `center`, flexDirection: `row`, justifyContent: `center`}}>
                <Text>智能設備狀態</Text>
              </Button>
            </View>

            {
              this.state.is_ready && this.state.activities && this.state.is_active === 1 ? (

                  <FlatList
                      style={{flex: 1}}
                      data={this.state.activities}
                      renderItem={({item}) => {
                        return (
                            <ListItem button onPress={() => {
                            }}>
                              <View style={{width: 32, height: 32, alignItems: 'center'}}>
                                <Thumbnail small source={{ uri: item.avatar_url }} />
                              </View>
                              <View style={{width: 32}}>
                              </View>
                              <Text style={{flex: 1, fontSize: 16}}>{item.desc}</Text>
                              <View style={{width: 32}}>
                              </View>
                              <View style={{width: 32}}>
                              </View>
                            </ListItem>
                        )
                      }}
                  />

              ) : null
            }
            {
              this.state.is_ready && this.state.family_users && this.state.is_active === 2 ? (

                  <FlatList
                      style={{flex: 1}}
                      data={this.state.family_users}
                      renderItem={({item}) => {
                        return (
                            <ListItem button onPress={() => {
                              this.props.navigation.navigate("FamilyUserDetailsPage", {
                                family_user: item,
                                family: this.state.family,
                                activities: this.state.activities,
                              });
                            }}>
                              <View style={{width: 32, height: 32, alignItems: 'center'}}>
                                <Thumbnail small source={{ uri: item.avatar_url }} />
                              </View>
                              <View style={{width: 32}}>
                              </View>
                              <Text style={{flex: 1, fontSize: 16}}>{item.nickname}</Text>
                              <View style={{width: 32}}>
                              </View>
                              {
                                item.online ? (
                                    <View style={{width: 16, height: 16, backgroundColor: '#55ff55', borderRadius: 8}}>
                                    </View>
                                ) : (
                                    <View style={{width: 16, height: 16, backgroundColor: '#ff5555', borderRadius: 8}}>
                                    </View>
                                )
                              }
                              <View style={{width: 32}}>
                              </View>
                            </ListItem>
                        )
                      }}
                  />

              ) : null
            }
            {
              this.state.is_ready && this.state.family_devices && this.state.is_active === 3 ? (

                  <FlatList
                      style={{flex: 1}}
                      data={this.state.family_devices}
                      renderItem={({item}) => {


                        var avatar_url = "";
                        var alias = item.alias;
                        var type = item.type;
                        var status = item.status || ' -- ';
                        var battery = item.battery || ' -- ';
                        var product_desc = "";
                        var serial = item.serial;
                        var online = item.online;

                        var cat = "";
                        var status_representation = status;

                        if (type === `G0`)
                        {
                          cat = `gw_wifi`;
                        }
                        else if (type === `M0`)
                        {
                          cat = `motion_sr`;
                        }
                        else if (type === `D0`)
                        {
                          cat = `door_sr`;
                        }

                        if (cat === `gw_wifi`)
                        {
                          avatar_url = "http://dev.oicomg.in/hc_public/mgs.png";
                          product_desc = "Wi-Fi 智能網關";
                          battery = null;
                        }
                        else if (cat === `motion_sr`)
                        {
                          avatar_url = "http://dev.oicomg.in/hc_public/mms.png";
                          product_desc = "人體 PIR 紅外感應器";
                          if (status_representation === "H")
                          {
                            status_representation = "探測到人體";
                          }
                          else if (status_representation === "L")
                          {
                            status_representation = " -- ";
                          }
                        }
                        else if (cat === `door_sr`)
                        {
                          avatar_url = "http://dev.oicomg.in/hc_public/mds.png";
                          product_desc = "門關感應器";
                          if (status_representation === "H")
                          {
                            status_representation = "開啟";
                          }
                          else if (status_representation === "L")
                          {
                            status_representation = "關閉";
                          }
                        }


                        return (
                            <ListItem button onPress={() => {
                              if (cat === `gw_wifi`)
                              {
                                Alert.alert(
                                    `Select action`.i18n(),
                                    `${alias} ${serial}\n${product_desc}`,
                                    [
                                      {text: `View details`.i18n(), onPress: () => {}},
                                      {text: `Change alias`.i18n(), onPress: () => {}},
                                      {text: `Wifi activation`.i18n(), onPress: () => {
                                          this.props.navigation.navigate("GatewaySmartWifiPageWizard", {item: item, parent: this})
                                        }},
                                      {text: `Add sub-device`.i18n(), onPress: () => {
                                          this.props.navigation.navigate("SubdeviceScanningPage", {item: item, parent: this})
                                        }},
                                      {text: `Cancel`.i18n(), onPress: () => {}},
                                    ],
                                    {
                                      cancelable: true
                                    }
                                )
                              }
                              else
                              {
                                Alert.alert(
                                    `Select action`.i18n(),
                                    `${alias} ${serial}\n${product_desc}`,
                                    [
                                      {text: `View details`.i18n(), onPress: () => {}},
                                      {text: `Change alias`.i18n(), onPress: () => {}},
                                      {text: `Cancel`.i18n(), onPress: () => {}},
                                    ],
                                    {
                                      cancelable: true
                                    }
                                )
                              }
                              this.props.navigation.navigate("FamilyGatewayDetailsPage", {
                                _id: item._id,
                              });
                            }}>
                              <View style={{width: 48, height: 48, alignItems: 'center'}}>
                                <Image style={{resizeMode: `contain`, width: 48, height: 48}} source={{ uri: avatar_url }} />
                              </View>
                              <View style={{width: 32}}>
                              </View>
                              <View style={{flex: 1, justifyContent: `flex-start`}}>
                                {
                                  alias ? (
                                      <View>
                                        <Text style={{width: 320, paddingLeft: 20}}>{alias}</Text>
                                        <Text note style={{opacity: 0.5, width: 320, paddingLeft: 20}}>{serial}</Text>
                                      </View>
                                  ) : (
                                      <View>
                                        <Text style={{width: 320, paddingLeft: 20}}>{product_desc}</Text>
                                        <Text note style={{opacity: 0.5, width: 320, paddingLeft: 20}}>{serial}</Text>
                                      </View>
                                  )
                                }
                                <Text style={{fontWeight: `bold`, width: 320, paddingLeft: 20}} notes>{`狀態：${status_representation}`}</Text>
                                {
                                  battery ? (
                                      <Text style={{fontWeight: `bold`, width: 320, paddingLeft: 20}} notes>{`電量：${battery}`}</Text>
                                  ) : null
                                }
                              </View>
                              {
                                online ? (
                                    <View style={{width: 16, height: 16, backgroundColor: '#55ff55', borderRadius: 8}}>
                                    </View>
                                ) : (
                                    <View style={{width: 16, height: 16, opacity: 0, backgroundColor: '#ff5555', borderRadius: 8}}>
                                    </View>
                                )
                              }
                              <View style={{width: 32}}>
                              </View>
                            </ListItem>
                        )
                      }}
                  />

              ) : null
            }

          </Content>

        </Container>
    );
  }
}







export default withNavigationFocus(FamilyDetailsPage)
