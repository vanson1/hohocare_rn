import React, { Component } from 'react';
import { Image, DeviceEventEmitter } from 'react-native';
import { Container, Header, View, Left, Right, Body, Title, Switch, Accordion, Button, Icon, Content, List, ListItem, Text, H1, H2, H3 } from 'native-base';
import ScheduleBar from "./ScheduleBar";
import Picker from 'react-native-picker';

export default class AccordionItem extends Component {
  showPicker(it)
  {
    var seconds = it.start_time || 0;
    let data = [
      [],
      [],
      ['am', 'pm'],
      ['On', 'Off'],
      ['cool', 'fan', 'dehum'],
      [],
    ];

    for(var i=1;i<13;i++){
      data[0].push(i);
    }

    for(var i=0;i<60;i++){
      data[1].push(i);
    }
    for(var i=18;i<=30;i++){
      data[5].push(i);
    }

    var hours = (Math.floor(seconds / 60 / 60));
    var minutes = (Math.floor(seconds / 60 % 60));
    var ampm = 'am';
    if (hours >= 12)
    {
      ampm = 'pm';
      hours -= 12;
    }
    if (hours <= 0) {
      hours += 12;
    }

    var {
      mode,
      power,
      temp,
    } = it.action;

    Picker.init({
      pickerData: data,
      selectedValue: [hours, minutes, ampm, power ? 'On' : 'Off', mode, temp],
      onPickerConfirm: data => {
        var hours = data[0];
        var mins = data[1];
        var ampm = data[2];
        var power = data[3];
        var mode = data[4];
        var temp = data[5];

        if (ampm === 'am') {
          if (hours >= 12) {
            hours -= 12;
          }
        } else if (ampm === 'pm') {
          hours += 12;
          if (hours >= 24)
          {
            hours -= 24;
          }
        }

        it.start_time = 0;
        it.start_time += mins * 60;
        it.start_time += hours * 60 * 60;
        it.action.mode = mode;
        it.action.temp = temp;
        it.action.power = power === 'On' ? 1 : 0;

        GLOBAL.STORE.update_schedule_payload(it.index, it.start_time, it.weekday, it.action, it.disabled);

        this.forceUpdate(() => {
          this.props.onUpdate && this.props.onUpdate();
        })
      },
      onPickerCancel: data => {
        console.log(data);
      },
      onPickerSelect: data => {
        console.log(data);
      }
    });
    Picker.show();
  }

  render() {

    var {
      item,
      items,
    } = this.props;


    var data = items || [];



    return (
        <Content>

          <ScheduleBar data={data}/>

          {
            data.map((it) => {

              if (!it.action)
              {
                return null;
              }

              var seconds = it.start_time || 0;
              var hhmm = ((Math.floor(seconds / 60 / 60) + "").padStart(2, "0")) + ":" + ((Math.floor(seconds / 60 % 60) + "").padStart(2, "0"));
              var icon = `d_ac_ic_mode_${it.action.mode}_o`;
              var fan = it.action.fan;

              var fan_fp = GLOBAL.IMAGE.d_ac_ic_fan_power_1;
              if (fan === "1") {
                fan_fp = GLOBAL.IMAGE.d_ac_ic_fan_power_1;
              }
              else if (fan === "2") {
                fan_fp = GLOBAL.IMAGE.d_ac_ic_fan_power_2;
              }
              else if (fan === "3") {
                fan_fp = GLOBAL.IMAGE.d_ac_ic_fan_power_3;
              }
              else if (fan === "4") {
                fan_fp = GLOBAL.IMAGE.d_ac_ic_fan_power_4;
              }
              else if (fan === "5") {
                fan_fp = GLOBAL.IMAGE.d_ac_ic_fan_power_5;
              }
              else if (fan === "auto") {
                fan_fp = GLOBAL.IMAGE.d_ac_ic_mode_auto_o;
              }
              else if (fan === "quiet") {
                fan_fp = GLOBAL.IMAGE.d_ac_ic_mode_eco_o;
              }

              var swing = it.action.swing;
              var swing_fp = GLOBAL.IMAGE.d_ac_ic_swing_off_o;
              if (swing === "Off") {
                swing_fp = GLOBAL.IMAGE.d_ac_ic_swing_off_o;
              }
              else if (swing === "Horizontal") {
                swing_fp = GLOBAL.IMAGE.d_ac_ic_swing_horizontal_o;
              }
              else if (swing === "Vertical") {
                swing_fp = GLOBAL.IMAGE.d_ac_ic_swing_vertical_o;
              }
              else if (swing === "3D mode") {
                swing_fp = GLOBAL.IMAGE.d_ac_ic_swing_3D_o;
              }

              return (
                  <View
                      padder
                      style={{marginLeft: 20, borderTopWidth: 1, borderTopColor: '#eee', flexDirection: `row`,
                        alignItems: `center`,
                        backgroundColor: '#fff'}} button={true} onPress={() => {
                        this.props.onPressItem(it);
                  }}>

                    <Button transparent
                        onPress={() => {

                          this.props.navigation.navigate("SchedulesSettingsPage", {
                            item: it,
                            onConfirmItem: ((it) => {
                              GLOBAL.STORE.update_schedule_payload(it.index, it.start_time, it.weekday, it.action, it.disabled);
                              this.forceUpdate(() => {
                                  GLOBAL.STORE.apply_schedule();
                              })
                            }),
                            onRemoveItem: ((it) => {
                              GLOBAL.STORE.remove_schedule(it.index);
                              this.forceUpdate(() => {
                                  GLOBAL.STORE.apply_schedule();
                              })
                            }),
                          });

                          //
                          //
                          // this.showPicker(it);
                        }}
                        style={{flex: 1, flexDirection: `row`}}>
                      <View style={{paddingLeft: 10, flexDirection: `row`}}>
                        <Text style={{fontSize: 17}}>{hhmm}</Text>
                      </View>

                      {
                        it.action.power ? (
                            <View style={{alignItems: `center`, paddingHorizontal: 20, flexDirection: `row`, flex: 1}}>
                              {
                                GLOBAL.IMAGE[icon] ? (
                                    <Image source={GLOBAL.IMAGE[icon]} style={{width: 26, height: 26, resizeMode: `contain`, marginRight: 20}}/>
                                ) : null
                              }
                              <Text style={{fontSize: 17}}>{it.action.temp}˚C</Text>


                              <Image source={fan_fp} style={{marginLeft: 20, width: 26, height: 26, resizeMode: `contain`, marginRight: 20}}/>

                              <Image source={swing_fp} style={{width: 26, height: 26, resizeMode: `contain`, marginRight: 20}}/>

                            </View>
                        ) : (
                            <View style={{paddingHorizontal: 20, flexDirection: `row`, flex: 1}}>
                              <Text note>{`OFF`}</Text>
                            </View>
                        )
                      }
                    </Button>

                    <Switch
                        onValueChange={(bool) => {
                          it.disabled = !bool;

                          GLOBAL.STORE.update_schedule_payload(it.index, it.start_time, it.weekday, it.action, it.disabled);

                          this.forceUpdate(() => {
                            this.props.doApplySchedule();
                          });
                        }}
                        value={!it.disabled} />


                  </View>
              )
            })
          }
        </Content>
    );
  }
}
