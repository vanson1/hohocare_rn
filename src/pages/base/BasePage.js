import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import {
  getTheme, StyleProvider } from 'native-base';
import customVariables from '../../themes/variable';
import APISourcerMqtt from '../../apis/APISourcerMqtt';

export default class BasePage extends React.PureComponent
{
  componentDidUpdate(prevProps) {
    if (prevProps.isFocused !== this.props.isFocused) {
      if (this.props.isFocused) {
        this.componentWillAppear();
      } else {
        this.componentWillDisappear();
      }
    }
  }


  tryMqttReconnect()
  {
    this.setState({is_loading2: true}, () => {
      APISourcerMqtt.getInstance().connect(true, () => {
        this.setState({is_loading2: false});
      })
    });
  }

  tryMqttDisconnect()
  {
    APISourcerMqtt.getInstance().disconnect();
    this.setState({is_loading2: false});
  }



  componentWillAppear()
  {

  }

  componentWillDisappear()
  {

  }

  showLoading()
  {
    this.setState({
      ___is_loading: true,
    })
  }

  hideLoading()
  {
    this.setState({
      ___is_loading: false,
    })
  }


  showLoading2()
  {
    this.setState({
      ___is_loading2: true,
    })
  }

  hideLoading2()
  {
    this.setState({
      ___is_loading2: false,
    })
  }


  renderInner()
  {
    return null;
  }

  render()
  {
    // return this.renderInner();

    return (
        <View style={{flex: 1}}>
          {
            this.renderInner()
          }
          {
            (this.state && (this.state.___is_loading || this.state.___is_loading2)) ? (
                <View style={{position: `absolute`, left: 0, right: 0, width: null, height: null, top: 0, bottom: 0,
                  backgroundColor: 'rgba(0,0,0,0.3)', alignItems: `center`, justifyContent: `center`}}>
                  <ActivityIndicator size={'large'} color={'#fff'} animating/>
                </View>
            ) : null
          }
        </View>
    )
  }
}
