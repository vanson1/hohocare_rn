import React from 'react';
import { Platform, View, ActivityIndicator, SafeAreaView } from 'react-native';
import {
  Body,
  Button, Container,
  Header, Icon, Left, Right, StyleProvider, Title, getTheme,
} from 'native-base';
import customVariables from '../../themes/variable';

export default class BaseNavigationBar extends React.Component
{
  render()
  {
    var {
      isBack,
      isMenu,
      icon_left,
      icon_right,
      title,
      navigation,
    } = this.props;

    return (
        <Header
            style={{ backgroundColor: '#f0f0f0' }}
            iosBarStyle={"dark-content"}
            androidStatusBarColor="#f0f0f0">
          <Left>
            {
              isBack ? (

                  <Button transparent onPress={() => {
                    navigation.pop();
                  }}>
                    <Icon name='arrow-back' style={{color: '#039BE5'}} color={`#039BE5`} />
                  </Button>

              ) : null
            }
            {
              isMenu ? (

                  <Button transparent onPress={() => {
                    navigation.toggleDrawer();
                  }}>
                    <Icon name='menu' style={{color: '#039BE5'}} color={`#039BE5`} />
                  </Button>

              ) : null
            }
            {
              icon_left ? (
                  icon_left
              ) : null
            }
          </Left>
          {
            Platform.OS === 'ios' ? (
                <Body style={{ position: `absolute`, left: 60, right: 60, bottom: 0, height: 44, alignItems: `center`, justifyContent: `center` }}>
                <Title style={{color: '#333'}}>{title}</Title>
                </Body>
            ) : (
                <Body style={{ position: `absolute`, left: 60, right: 60, top: 0, bottom: 0, alignItems: `center`, justifyContent: `center` }}>
                <Title style={{color: '#333'}}>{title}</Title>
                </Body>
            )
          }
          <Right>
            {
              icon_right ? (
                  icon_right
              ) : null
            }
          </Right>
        </Header>
    )

  }
}
