import React, { Component } from 'react';
import { ScrollView, Image } from "react-native";
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3,
} from "native-base";
import BasePage from "../../base/BasePage";
import moment from 'moment';
import BaseNavigationBar from '../../base/BaseNavigationBar';
import APISourcerPrelogin from '../../../apis/APISourcerPrelogin';

export default class ConfigureMainPage extends BasePage {

  componentDidMount(): void {
    var serial = GLOBAL.CONFIG_ITEM.serial;
    GLOBAL.LISTEN_DEVICE_UPDATES(serial, this.onAck.bind(this))
  }

  componentWillUnmount(): void {
    var serial = GLOBAL.CONFIG_ITEM.serial;
    GLOBAL.UNLISTEN_DEVICE_UPDATES(serial, this.onAck.bind(this))
  }

  onAck()
  {
    this.forceUpdate()
  }

  renderInner() {

    // var item = this.props.navigation.getParam("item");
    var item = GLOBAL.CONFIG_ITEM;

    var {
        title,
        ssid,
        serial,
        host_model,
        alias,
        status
    } = item;

    var {
      is_online,
      fw_version,
      fw_brand,
      fw_category,
      fw_shortcode,
      timezone,
      is_led_disable,
    } = (item.info || {});

    var isOffline = GLOBAL.IDENTIFY_DEVICE_IS_OFFLINE(item);

    return (
        <Container>

          <BaseNavigationBar {...this.props}
              isBack
              title={`Configure`.i18n()}
          />

          <ScrollView>

            <Content padder>

              <H1 style={{color: '#586C78', marginTop: 20, marginBottom: 15, textAlign: `center`}}>{alias}</H1>

              <View style={{flexDirection: `row`, alignItems: `center`, justifyContent: `center`}}>
                <Image
                    source={GLOBAL.IMAGE.device_1_o}
                    style={{flex: 1, resizeMode: `contain`, alignSelf: `center`, height: 100}}/>
                <Image
                    source={GLOBAL.IMAGE.ic_connect_line}
                    style={{resizeMode: `contain`, alignSelf: `center`, height: 100}}/>
                <Image
                    source={GLOBAL.IMAGE.icon_model_ac}
                    style={{flex: 1, resizeMode: `contain`, alignSelf: `center`, height: 100}}/>
              </View>

              <Text style={{color: '#586C78', padding: 5, textAlign: `center`}}>{`${`SSID`.i18n()}: ${ssid}`}</Text>
              <Text style={{color: '#586C78', padding: 5, textAlign: `center`}}>{`${`Serial `.i18n()}#: ${serial}`}</Text>
              {/*<Text style={{color: '#586C78', padding: 5, textAlign: `center`}}>{`${`Host `.i18n()}model: ${host_model}`}</Text>*/}
              <Text style={{color: '#586C78', padding: 5, textAlign: `center`}}>{`${`Alias`.i18n()}: ${alias}`}</Text>
              <Text style={{color: '#586C78', padding: 5, textAlign: `center`}}>{`${`Status`.i18n()}: ${(!isOffline && is_online) ? 'Online'.i18n() : 'Offline'.i18n()}`}</Text>
              <Text style={{color: '#586C78', padding: 5, textAlign: `center`}}>{`${`Firmware `.i18n()}: ${fw_version}`}</Text>

              <View style={{height: 50}}/>


              <List>


                <ListItem button onPress={() => {
                  this.props.navigation.navigate("ConfigureSettingsLEDPage", {item: item, parent: this})
                }}>
                  <Left>
                    <Text>{`LED Settings`.i18n()}</Text>
                  </Left>
                  <Right>
                    <Icon name="arrow-forward" />
                  </Right>
                </ListItem>


                <ListItem button onPress={() => {
                  this.props.navigation.navigate("ConfigureSettingsHostModelPage", {item: item, parent: this})
                }}>
                  <Left>
                    <Text>{`IR remote model`.i18n()}</Text>
                  </Left>
                  <Right>
                    <Icon name="arrow-forward" />
                  </Right>
                </ListItem>


                <ListItem button onPress={() => {
                  this.props.navigation.navigate("ConfigureSettingsConfigureWiFiConnectionPage", {item: item, parent: this})
                }}>
                  <Left>
                    <Text>{`Re-configure router connection`.i18n()}</Text>
                  </Left>
                  <Right>
                    <Icon name="arrow-forward" />
                  </Right>
                </ListItem>


                <ListItem button onPress={() => {
                  this.props.navigation.navigate("ConfigureSettingsChangeAliasPage", {item: item, parent: this})
                }}>
                  <Left>
                    <Text>{`Change alias`.i18n()}</Text>
                  </Left>
                  <Right>
                    <Icon name="arrow-forward" />
                  </Right>
                </ListItem>


                <ListItem button onPress={() => {
                  if (APISourcerPrelogin.D2Bros_Firmware_Update_Method(item) === 1)
                  {
                    this.props.navigation.navigate("ConfigureSettingsOTAPage1", {item: item, parent: this})
                  }
                  if (APISourcerPrelogin.D2Bros_Firmware_Update_Method(item) === 2)
                  {
                    this.props.navigation.navigate("ConfigureSettingsOTAPage2", {item: item, parent: this})
                  }
                }}>
                  <Left>
                    <Text>{`Check for firmware updates`.i18n()}</Text>
                  </Left>
                  <Right>
                    <Icon name="arrow-forward" />
                  </Right>
                </ListItem>
                <ListItem button onPress={() => {
                  this.props.navigation.navigate("ConfigureSettingsDeactivatePage", {item: item, parent: this})
                }}>
                  <Left>
                    <Text>{`Deactivate D2Bros`.i18n()}</Text>
                  </Left>
                  <Right>
                    <Icon name="arrow-forward" />
                  </Right>
                </ListItem>
              </List>



            </Content>

          </ScrollView>

        </Container>
    );
  }
}
