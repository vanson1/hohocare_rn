import Color from "color";
const color = Color;

import { Platform, Dimensions, PixelRatio } from "react-native";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const platform = Platform.OS;
const isIphoneX =
    platform === "ios" && deviceHeight >= 812 && deviceWidth >= 375;



export default {
  isIphoneX: isIphoneX,

  brandPrimary : '#fff',
  brandInfo: '#50adf9',
  brandSuccess: '#5cb85c',
  brandDanger: '#d9534f',
  brandWarning: '#f0ad4e',
  brandSidebar: '#252932',

  fontFamily: (Platform.OS === 'ios' ) ? 'HelveticaNeue' : 'Roboto',
  btnFontFamily: (Platform.OS === 'ios' ) ? 'HelveticaNeue' : 'Roboto_medium',
  iconFamily: 'Ionicons',

  inverseTextColor: '#fff',
  textColor: '#000',

  subtitleColor: '#8e8e93',

  fontSizeBase: 15,
  titleFontColor: '#000',
  titleFontSize: (Platform.OS === 'ios' ) ? 17 : 17,
  subTitleFontSize: (Platform.OS === 'ios' ) ? 12 : 12,

  inputFontSize: 15,
  inputLineHeight: 24,

  get fontSizeH1 () {
    return this.fontSizeBase*1.8;
  },
  get fontSizeH2 () {
    return this.fontSizeBase* 1.6;
  },
  get fontSizeH3 () {
    return this.fontSizeBase* 1.4;
  },
  get btnTextSize () {
    return (Platform.OS==='ios') ? this.fontSizeBase* 1.1 :
        this.fontSizeBase-1;
  },
  get btnTextSizeLarge () {
    return this.fontSizeBase* 1.5;
  },
  get btnTextSizeSmall () {
    return this.fontSizeBase* .8;
  },
  get iconSizeLarge () {
    return this.iconFontSize* 1.5;
  },
  get iconSizeSmall () {
    return this.iconFontSize* .6;
  },

  buttonPadding: 6,

  borderRadiusBase: (Platform.OS === 'ios' ) ? 5 : 2,

  get borderRadiusLarge () {
    return this.fontSizeBase* 3.8;
  },

  footerHeight: 55,
  toolbarHeight: (Platform.OS === 'ios' ) ? 64 : 56,
  toolbarDefaultBg: (Platform.OS === 'ios' ) ? '#F8F8F8' : '#fff',
  toolbarInverseBg: '#222',

  iosToolbarBtnColor: '#039BE5',
  toolbarBtnColor: '#039BE5',

  toolbarTextColor: (Platform.OS==='ios') ? '#000' : '#fff',

  checkboxBgColor: '#039BE5',
  checkboxTickColor: '#fff',

  checkboxSize: 23,

  radioColor: '#7e7e7e',
  get radioSelectedColor() {
    return Color(this.radioColor).darken(0.2).hex();
  },

  radioBtnSize: (Platform.OS === 'ios') ? 25 : 23,

  tabBgColor: '#F8F8F8',
  tabFontSize: 15,
  tabTextColor: '#fff',

  btnDisabledBg: '#b5b5b5',
  btnDisabledClr: '#f1f1f1',

  cardDefaultBg: '#fff',

  get darkenHeader() {
    return Color(this.tabBgColor).darken(0.03).hex();
  },
  get btnPrimaryBg () {
    return this.brandPrimary;
  },
  get btnPrimaryColor () {
    return this.inverseTextColor;
  },
  get btnSuccessBg () {
    return this.brandSuccess;
  },
  get btnSuccessColor () {
    return this.inverseTextColor;
  },
  get btnDangerBg () {
    return this.brandDanger;
  },
  get btnDangerColor () {
    return this.inverseTextColor;
  },
  get btnInfoBg () {
    return this.brandInfo;
  },
  get btnInfoColor () {
    return this.inverseTextColor;
  },
  get btnWarningBg () {
    return this.brandWarning;
  },
  get btnWarningColor () {
    return this.inverseTextColor;
  },

  borderWidth: 1,
  iconMargin: 7,

  get inputColor () {
    return this.textColor;
  },
  get inputColorPlaceholder () {
    return '#575757';
  },
  inputBorderColor: '#D9D5DC',
  inputSuccessBorderColor: '#2b8339',
  inputErrorBorderColor: '#ed2f2f',
  inputHeightBase: 40,
  inputGroupMarginBottom: 10,
  inputPaddingLeft: 5,
  get inputPaddingLeftIcon () {
    return this.inputPaddingLeft* 8;
  },

  btnLineHeight: 19,

  dropdownBg: '#000',
  dropdownLinkColor: '#414142',

  jumbotronPadding: 30,
  jumbotronBg: '#C9C9CE',

  contentPadding: 10,

  listBorderColor: '#ddd',
  listDividerBg: '#ddd',
  listItemPadding: 9,
  listItemHeight: 45,
  listNoteColor: '#808080',
  listNoteSize: 13,

  iconFontSize: (Platform.OS === 'ios' ) ? 30 : 28,

  badgeColor: '#fff',
  badgeBg: '#ED1727',

  lineHeight: (Platform.OS === 'ios' ) ? 20 : 24,
  iconLineHeight: (Platform.OS === 'ios' ) ? 37 : 30,

  toolbarIconSize: (Platform.OS === 'ios' ) ? 20 : 22,

  toolbarInputColor: '#CECDD2',

  defaultSpinnerColor: '#45D56E',
  inverseSpinnerColor: '#1A191B',

  defaultProgressColor: '#E4202D',
  inverseProgressColor: '#1A191B',


  segmentBackgroundColor: '#F8F8F8',
  segmentActiveBackgroundColor:	'#000',
  segmentTextColor:	'#000',
  segmentActiveTextColor:	'#fff',
  segmentBorderColor:	'#000',
  segmentBorderColorMain:	'#a7a6ab',


  tabBarTextColor: '#6b6b6b',
  tabBarTextSize: 14,
  activeTab: '#007aff',
  sTabBarActiveTextColor: '#007aff',
  tabBarActiveTextColor: '#007aff',
  tabActiveBgColor:' #cde1f9',

  tabDefaultBg: '#F8F8F8',
  topTabBarTextColor: '#6b6b6b',
  topTabBarActiveTextColor: '#007aff',
  topTabBarBorderColor: '#a7a6ab',
  topTabBarActiveBorderColor: '#007aff',


  CheckboxRadius: platform === "ios" ? 13 : 0,
  CheckboxBorderWidth: platform === "ios" ? 1 : 2,
  CheckboxPaddingLeft: platform === "ios" ? 4 : 2,
  CheckboxPaddingBottom: platform === "ios" ? 0 : 5,
  CheckboxIconSize: platform === "ios" ? 21 : 16,
  CheckboxIconMarginTop: platform === "ios" ? undefined : 1,
  CheckboxFontSize: platform === "ios" ? 23 / 0.9 : 17,
  checkboxDefaultColor: 'transparent',
  checkboxTextShadowRadius: 0,











  // Accordion
  accordionBorderColor: '#d3d3d3',
  accordionContentPadding: 10,
  accordionIconFontSize: 18,
  contentStyle: '#f5f4f5',
  expandedIconStyle: '#000',
  headerStyle: '#edebed',
  iconStyle: '#000',

  // ActionSheet
  elevation: 4,
  containerTouchableBackgroundColor: 'rgba(0,0,0,0.4)',
  innerTouchableBackgroundColor: '#fff',
  // listItemHeight: 50,
  listItemBorderColor: 'transparent',
  marginHorizontal: -15,
  marginLeft: 14,
  marginTop: 15,
  minHeight: 56,
  padding: 15,
  touchableTextColor: '#757575',

  // Android
  androidRipple: true,
  androidRippleColor: 'rgba(256, 256, 256, 0.3)',
  androidRippleColorDark: 'rgba(0, 0, 0, 0.15)',
  buttonUppercaseAndroidText: true,

  // Badge
  // badgeBg: '#ED1727',
  // badgeColor: '#fff',
  badgePadding: Platform.OS === "ios" ? 3 : 0,

  // Button
  buttonFontFamily: platform === "ios" ? 'System' : 'Roboto_medium',
  buttonDisabledBg: '#b5b5b5',
  // buttonPadding: 6,
  buttonDefaultActiveOpacity: 0.5,
  buttonDefaultFlex: 1,
  buttonDefaultBorderRadius: 2,
  buttonDefaultBorderWidth: 1,
  get buttonPrimaryBg() {
    return '#757575';
  },
  get buttonPrimaryColor() {
    return this.inverseTextColor;
  },
  get buttonInfoBg() {
    return this.brandInfo;
  },
  get buttonInfoColor() {
    return this.inverseTextColor;
  },
  get buttonSuccessBg() {
    return this.brandSuccess;
  },
  get buttonSuccessColor() {
    return this.inverseTextColor;
  },
  get buttonDangerBg() {
    return this.brandDanger;
  },
  get buttonDangerColor() {
    return this.inverseTextColor;
  },
  get buttonWarningBg() {
    return this.brandWarning;
  },
  get buttonWarningColor() {
    return this.inverseTextColor;
  },
  get buttonTextSize() {
    return platform === "ios"
        ? this.fontSizeBase * 1.1
        : this.fontSizeBase - 1;
  },
  get buttonTextSizeLarge() {
    return this.fontSizeBase * 1.5;
  },
  get buttonTextSizeSmall() {
    return this.fontSizeBase * 0.8;
  },
  // get borderRadiusLarge() {
  //   return this.fontSizeBase * 3.8;
  // },
  // get iconSizeLarge() {
  //   return this.iconFontSize * 1.5;
  // },
  // get iconSizeSmall() {
  //   return this.iconFontSize * 0.6;
  // },

  // Card
  // cardDefaultBg: '#fff',
  cardBorderColor: '#ccc',
  cardBorderRadius: 2,
  cardItemPadding: platform === "ios" ? 10 : 12,

  brandDark: '#000',
  brandLight: '#f4f4f4',

  // Container
  containerBgColor: '#fff',

  // Date Picker
  datePickerFlex: 1,
  datePickerPadding: 10,
  datePickerTextColor: '#000',
  datePickerBg: 'transparent',

  // FAB
  fabBackgroundColor: 'blue',
  fabBorderRadius: 28,
  fabBottom: 0,
  fabButtonBorderRadius: 20,
  fabButtonHeight: 40,
  fabButtonLeft: 7,
  fabButtonMarginBottom: 10,
  fabContainerBottom: 20,
  fabDefaultPosition: 20,
  fabElevation: 4,
  fabIconColor: '#fff',
  fabIconSize: 24,
  fabShadowColor: '#000',
  fabShadowOffsetHeight: 2,
  fabShadowOffsetWidth: 0,
  fabShadowOpacity: 0.4,
  fabShadowRadius: 2,
  fabWidth: 56,

  // Font
  DefaultFontSize: 16,

  // Footer
  // footerHeight: 55,
  footerDefaultBg: platform === "ios" ? '#F8F8F8' : '#3F51B5',
  footerPaddingBottom: 0,

  // FooterTab
  // tabBarTextColor: platform === "ios" ? '#6b6b6b' : '#b3c7f9',
  // tabBarTextSize: platform === "ios" ? 14 : 11,
  // activeTab: platform === "ios" ? '#007aff' : '#fff',
  // sTabBarActiveTextColor: '#007aff',
  // tabBarActiveTextColor: platform === "ios" ? '#007aff' : '#fff',
  // tabActiveBgColor: platform === "ios" ? '#cde1f9' : '#3F51B5',

  // Header
  // toolbarBtnColor: platform === "ios" ? '#007aff' : '#fff',
  // toolbarDefaultBg: platform === "ios" ? '#F8F8F8' : '#3F51B5',
  // toolbarHeight: platform === "ios" ? 64 : 56,
  toolbarSearchIconSize: platform === "ios" ? 20 : 23,
  // toolbarInputColor: platform === "ios" ? '#CECDD2' : '#fff',
  searchBarHeight: platform === "ios" ? 30 : 40,
  searchBarInputHeight: platform === "ios" ? 30 : 50,
  toolbarBtnTextColor: platform === "ios" ? '#007aff' : '#fff',
  toolbarDefaultBorder: platform === "ios" ? '#a7a6ab' : '#3F51B5',
  iosStatusbar: platform === "ios" ? 'dark-content' : 'light-content',
  get statusBarColor() {
    return color(this.toolbarDefaultBg)
        .darken(0.2)
        .hex();
  },
  // get darkenHeader() {
  //   return color(this.tabBgColor)
  //       .darken(0.03)
  //       .hex();
  // },

  // Icon
  // iconFamily: 'Ionicons',
  // iconFontSize: platform === "ios" ? 30 : 28,
  iconHeaderSize: platform === "ios" ? 33 : 24,

  // InputGroup
  // inputFontSize: 17,
  // inputBorderColor: '#D9D5DC',
  // inputSuccessBorderColor: '#2b8339',
  // inputErrorBorderColor: '#ed2f2f',
  // inputHeightBase: 50,
  // get inputColor() {
  //   return this.textColor;
  // },
  // get inputColorPlaceholder() {
  //   return '#575757';
  // },

  // Line Height
  buttonLineHeight: 19,
  lineHeightH1: 32,
  lineHeightH2: 27,
  lineHeightH3: 22,
  // lineHeight: platform === "ios" ? 20 : 24,
  listItemSelected: platform === "ios" ? '#007aff' : '#3F51B5',

  // List
  listBg: 'transparent',
  // listBorderColor: '#c9c9c9',
  // listDividerBg: '#f4f4f4',
  listBtnUnderlayColor: '#DDD',
  // listItemPadding: platform === "ios" ? 10 : 12,
  // listNoteColor: '#808080',
  // listNoteSize: 13,

  // Progress Bar
  // defaultProgressColor: '#E4202D',
  // inverseProgressColor: '#1A191B',

  // Radio Button
  // radioBtnSize: platform === "ios" ? 25 : 23,
  radioSelectedColorAndroid: '#3F51B5',
  radioBtnLineHeight: platform === "ios" ? 29 : 24,
  // get radioColor() {
  //   return this.brandPrimary;
  // },

  // Segment
  // segmentBackgroundColor: platform === "ios" ? '#F8F8F8' : '#3F51B5',
  // segmentActiveBackgroundColor: platform === "ios" ? '#007aff' : '#fff',
  // segmentTextColor: platform === "ios" ? '#007aff' : '#fff',
  // segmentActiveTextColor: platform === "ios" ? '#fff' : '#3F51B5',
  // segmentBorderColor: platform === "ios" ? '#007aff' : '#fff',
  // segmentBorderColorMain: platform === "ios" ? '#a7a6ab' : '#3F51B5',

  // Spinner
  // defaultSpinnerColor: '#45D56E',
  // inverseSpinnerColor: '#1A191B',

  // Tab
  tabBarDisabledTextColor: '#BDBDBD',
  // tabDefaultBg: platform === "ios" ? '#F8F8F8' : '#3F51B5',
  // topTabBarTextColor: platform === "ios" ? '#6b6b6b' : '#b3c7f9',
  // topTabBarActiveTextColor: platform === "ios" ? '#007aff' : '#fff',
  // topTabBarBorderColor: platform === "ios" ? '#a7a6ab' : '#fff',
  // topTabBarActiveBorderColor: platform === "ios" ? '#007aff' : '#fff',

  // Tabs
  // tabBgColor: '#F8F8F8',
  // tabFontSize: 15,

  // Text
  // textColor: '#000',
  // inverseTextColor: '#fff',
  noteFontSize: 14,
  get defaultTextColor() {
    return this.textColor;
  },

  // Title
  titleFontfamily: platform === "ios" ? 'System' : 'Roboto_medium',
  // titleFontSize: platform === "ios" ? 17 : 19,
  // subTitleFontSize: platform === "ios" ? 11 : 14,
  // subtitleColor: platform === "ios" ? '#8e8e93' : '#FFF',
  // titleFontColor: platform === "ios" ? '#000' : '#FFF',

  // Other
  // borderRadiusBase: platform === "ios" ? 5 : 2,
  // borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
  // contentPadding: 10,
  // dropdownLinkColor: '#414142',
  // inputLineHeight: 24,
  deviceWidth,
  deviceHeight,
  // isIphoneX,
  inputGroupRoundedBorderRadius: 30,

  // iPhoneX SafeArea
  Inset: {
    portrait: {
      topInset: 24,
      leftInset: 0,
      rightInset: 0,
      bottomInset: 34
    },
    landscape: {
      topInset: 0,
      leftInset: 44,
      rightInset: 44,
      bottomInset: 21
    }
  }
}
