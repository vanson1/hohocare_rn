import React, { Component } from 'react';
import { Image } from 'react-native';
import { Container, Header, View, Left, Right, Body, Title, Accordion, Button, Icon, Content, List, ListItem, Text } from 'native-base';

export default class AccordionHeader extends Component {
  render() {

    var {
      item,
      expanded,
      onPressAddItem,
    } = this.props;

    return (
        <View style={{backgroundColor: '#f1f3f2', marginLeft: 0, padding: 5, flexDirection: `row`, alignItems: `center` }}>
          <View>
            <Image source={GLOBAL.IMAGE.icon_model_ac} style={{width: 64, height: 56, resizeMode: `contain`}}/>
          </View>
          <Text style={{flex: 1}}>{item.alias || item.title}</Text>

          <View style={{width: 56, paddingHorizontal: 4}}>
            <Icon name={expanded ? "arrow-up" : "arrow-down"} />
          </View>

          <Button transparent
                  onPress={() => {

                    onPressAddItem();

                  }} style={{width: 56, paddingHorizontal: 4}}>
            <Icon name={"add"}/>
          </Button>

        </View>
    );
  }
}
