import React, { Component } from 'react';
import { ScrollView } from "react-native";
import WifiManager from "react-native-wifi-reborn";
import { Container, Header, Left, Right, Body, Title, Accordion, View, Button, Icon, Content, List, ListItem, Text, H1, H2, H3, Form, Item, Input, Label } from 'native-base';
import BasePage from "../../base/BasePage";
import APISourcerPrelogin from "../../../apis/APISourcerPrelogin";
import BaseNavigationBar from '../../base/BaseNavigationBar';

export default class ActivationBeforeStartPage extends BasePage {

  constructor(props)
  {
    super(props);
    this.state = {
      code: '',
    }
  }

  componentDidMount(): void {
    this.showLoading()
    new APISourcerPrelogin().register_request_verify(
        this.props.navigation.state.params.username
    )
        .then((res) => {
          this.hideLoading()
        })
        .catch((e) => {
          alert(e.message)
          this.hideLoading()
        });
  }

  renderInner() {

    var isDisabled =
        !this.state.code.trim()
        || this.state.code.trim().length < 6;

    return (
        <Container>

          <BaseNavigationBar {...this.props}
                             isBack
                             title={`Verification`.i18n()}
          />

          <Content padder>

            <H1 style={{color: '#586C78', marginTop: 60, marginBottom: 30, textAlign: `center`}}>{`Verify your email account`.i18n()}</H1>

            <Text style={{color: '#586C78', padding: 5, textAlign: `left`}}>{`Please check your email inbox (including your spam inbox) for an email sent by D2Bros. Retrieve and type the verification code below:`.i18n()}</Text>

            <Form>
              <Item>
                <Label>{`Verification code`.i18n()}</Label>
                <Input value={this.state.code} autoCapitalize={false} autoCompleteType={'off'} onChangeText={(val) => {this.setState({code: val})}} />
              </Item>
            </Form>


            <View style={{height: 50}}/>

            <Button
                disabled={isDisabled}
                onPress={() => {

                  this.showLoading();
                  new APISourcerPrelogin().register_verify(
                      this.props.navigation.state.params.username,
                      this.state.code,
                  )
                      .then((res) => {
                        this.hideLoading();
                        this.props.navigation.navigate("RegisterCompletePage", {
                          username: this.props.navigation.state.params.username,
                          password: this.props.navigation.state.params.password,
                        })
                      })
                      .catch((e) => {
                        this.hideLoading();
                        alert(e.message);
                      })
                }}
                style={{opacity: isDisabled ? 0.5 : 1, backgroundColor: '#4fadf9', justifyContent: `center`}}>
              <H3 style={{color: '#fff'}}>{`Confirm and submit`.i18n()}</H3>
            </Button>

          </Content>

        </Container>
    );
  }
}
