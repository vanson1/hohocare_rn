import { observable, action, extras, useStrict, runInAction } from 'mobx';
import remotedev from 'mobx-remotedev';

@remotedev(/*{ config }*/)
const appStore = observable({
  // ...
});
