import React, {PureComponent} from 'react';
import {Image, TouchableOpacity, ScrollView} from 'react-native';
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3, CheckBox,
} from 'native-base';
import BasePage from "../../../base/BasePage";
import BaseNavigationBar from '../../../base/BaseNavigationBar';


export default class GeneralSettingsLanguagePage extends BasePage {
  refresh()
  {
    this.changeLang = true;
    this.forceUpdate();
  }

  render() {
    return (
        <Container>

          <BaseNavigationBar {...this.props}
              icon_left={
                <Button transparent onPress={() => {
                  if (this.changeLang)
                  {
                    this.props.navigation.pop();

                    GLOBAL.NAV_GOTO_BY_SESSION();
                  }
                  else
                  {
                    this.props.navigation.pop();
                  }
                }}>
                  <Icon name='arrow-back' style={{color: '#039BE5'}} color={`#039BE5`} />
                </Button>
              }
              title={`Language`.i18n()}
          />


          <ScrollView>

            <Content padder>

              <ListItem  onPress={() => {
                global.APP_SETLANGUAGE('en_US');
                this.refresh();
              }} >
                <CheckBox checked={global.APP_GETLANGUAGE() === 'en_US'}  onPress={() => {
                  global.APP_SETLANGUAGE('en_US');
                  this.refresh();
                }} />
                <Body>
                <Text>{`en_US`.i18n()}</Text>
                </Body>
              </ListItem>

              <ListItem  onPress={() => {
                global.APP_SETLANGUAGE('zh_HK');
                this.refresh();
              }} >
                <CheckBox checked={global.APP_GETLANGUAGE() === 'zh_HK'}  onPress={() => {
                  global.APP_SETLANGUAGE('zh_HK');
                  this.refresh();
                }} />
                <Body>
                <Text>{`zh_HK`.i18n()}</Text>
                </Body>
              </ListItem>

              <ListItem  onPress={() => {
                global.APP_SETLANGUAGE('zh_CN');
                this.refresh();
              }} >
                <CheckBox checked={global.APP_GETLANGUAGE() === 'zh_CN'}  onPress={() => {
                  global.APP_SETLANGUAGE('zh_CN');
                  this.refresh();
                }} />
                <Body>
                <Text>{`zh_CN`.i18n()}</Text>
                </Body>
              </ListItem>


            </Content>

          </ScrollView>

        </Container>
    );
  }
}

