import APISourcer from './APISourcer';

export default class APISourcerExternal extends APISourcer
{
  static weather_temp = " -- ";
  static weather_humidity = " -- ";

  getWeatherHk()
  {
    return fetch(`${this.getExternalServerWeatherUrl()}/weather/hk`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {

      if (json && json.current)
      {
        if (json.current.regional)
        {
          if (json.current.regional.degrees_c)
          {
            APISourcerExternal.weather_temp = json.current.regional.degrees_c;
          }
          if (json.current.regional.humidity_pct)
          {
            APISourcerExternal.weather_humidity = json.current.regional.humidity_pct;
          }
        }
      }

      return json;
    }).catch((error) => {
      throw error;
    });
  }
}
