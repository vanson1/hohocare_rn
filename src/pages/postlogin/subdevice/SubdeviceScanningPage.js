import React, { Component } from 'react';
import { Container, Header, Left, Right, Body, Title, Accordion, Button, Icon, Content, List, ListItem, Text } from 'native-base';
import { Image, TouchableOpacity, View, FlatList } from 'react-native';
import BasePage from "../../base/BasePage";
import { withNavigationFocus } from "react-navigation";
import APISourcerMqtt from '../../../apis/APISourcerMqtt';
import BaseNavigationBar from '../../base/BaseNavigationBar';


class SubdeviceScanningPage extends BasePage {
  constructor(props)
  {
    super(props);
    this.state = {
    }
  }

  componentDidMount(): void {

  }

  componentWillUnmount(): void {

    APISourcerMqtt.getInstance().publish(
        `vp/iot/G/app/${this.props.navigation.state.params.item.type}/${this.props.navigation.state.params.item.serial}`,
        ({action: "exit_pair"})
    )
  }

  renderInner() {
    return (
        <Container>

          <BaseNavigationBar {...this.props}
                             isBack
                             title={`Add sensors`.i18n()}
          />

          <Content>

            <Button onPress={() => {

              APISourcerMqtt.getInstance().publish(
                  `vp/iot/G/app/${this.props.navigation.state.params.item.type}/${this.props.navigation.state.params.item.serial}`,
                  ({action: "toggle_pair"})
              )

            }}>
              <Text>{`Toggle scanning (PAIR ONLY)`.i18n()}</Text>
            </Button>


            <Button onPress={() => {

              APISourcerMqtt.getInstance().publish(
                  `vp/iot/G/app/${this.props.navigation.state.params.item.type}/${this.props.navigation.state.params.item.serial}`,
                  ({action: "exit_pair"})
              )

            }}>
              <Text>{`Stop scanning`.i18n()}</Text>
            </Button>

            <View style={{height: 200}}/>

            <Button onPress={() => {

              APISourcerMqtt.getInstance().publish(
                  `vp/iot/G/app/${this.props.navigation.state.params.item.type}/${this.props.navigation.state.params.item.serial}`,
                  ({action: "toggle_pair_assign_KKSN2281sssaererhcCD2a"})
              )

            }}>
              <Text>{`Toggle scanning (FACTORY RESET)`.i18n()}</Text>
            </Button>

          </Content>

        </Container>
    );
  }
}


export default withNavigationFocus(SubdeviceScanningPage)
