import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
} from 'react-native';
import {
  Drawer,
  Text,
} from 'native-base';
import SideBar from './AppDrawerSideBar';
import {
  createStackNavigator,
  createDrawerNavigator,
  createAppContainer,
} from "react-navigation";

import _HomeDashboardPageV2 from './home/HomeDashboardPageV2'
import FamilyDetailsPage from './family/FamilyDetailsPage'
import FamilyUserDetailsPage from './familyuser/FamilyUserDetailsPage'
import GatewaySmartWifiPageWizard from './gatewaysmartwifi/GatewaySmartWifiPageWizard'
import SubdeviceScanningPage from './subdevice/SubdeviceScanningPage'
import ActivationBeforeStartPage from "./activation/ActivationBeforeStartPage";
import ActivationFindDevicePage from "./activation/ActivationFindDevicePage";
import ActivationConfigureWiFiConnectionPage from "./activation/ActivationConfigureWiFiConnectionPage";
import ActivationSetupProgressPage from "./activation/ActivationSetupProgressPage";
import ActivationSetupProgressPage1328 from "./activation/ActivationSetupProgressPage1328";
import ConfigureMainPage from "./configure/ConfigureMainPage";
import ConfigureSettingsChangeAliasPage from "./configure/ConfigureSettingsChangeAliasPage";
import ConfigureSettingsDeactivatePage from "./configure/ConfigureSettingsDeactivatePage";
import ConfigureSettingsHostModelPage from "./configure/ConfigureSettingsHostModelPage";
import ConfigureSettingsConfigureWiFiConnectionPage from './configure/ConfigureSettingsConfigureWiFiConnectionPage';
import ControlAirConditionerPage from "./controls/ControlAirConditionerPage";
import SchedulesMainPage from "./schedules/SchedulesMainPage";
import VacationModeMainPage from "./vacation/VacationModeMainPage";
import ConfigureSettingsLEDPage from './configure/ConfigureSettingsLEDPage';
import SchedulesSettingsPage from './schedules/SchedulesSettingsPage';
import GeneralSettingsPage from './misc/generalsettings/GeneralSettingsPage';
import GeneralSettingsLanguagePage from './misc/generalsettings/GeneralSettingsLanguagePage';
import GeneralSettingsDegreeDisplayPage from './misc/generalsettings/GeneralSettingsDegreeDisplayPage';
import UserManualPage from './misc/usermanual/UserManualPage';
import FAQPage from './misc/faq/FAQPage';
import TACPage from './misc/tac/TACPage';

const Activation = createStackNavigator(
    {
      ActivationBeforeStartPage: { screen: ActivationBeforeStartPage },
      ActivationFindDevicePage: { screen: ActivationFindDevicePage },
      ActivationConfigureWiFiConnectionPage: { screen: ActivationConfigureWiFiConnectionPage },
      ActivationSetupProgressPage: { screen: ActivationSetupProgressPage },
      ActivationSetupProgressPage1328: { screen: ActivationSetupProgressPage1328 },
    },
    {
      headerMode: 'none',
    }
)

const Configure = createStackNavigator(
    {
      ControlAirConditionerPage: { screen: ControlAirConditionerPage },
      ConfigureMainPage: { screen: ConfigureMainPage },
      ConfigureSettingsChangeAliasPage: { screen: ConfigureSettingsChangeAliasPage },
      ConfigureSettingsDeactivatePage: { screen: ConfigureSettingsDeactivatePage },
      ConfigureSettingsHostModelPage: { screen: ConfigureSettingsHostModelPage },
      ConfigureSettingsLEDPage: { screen: ConfigureSettingsLEDPage },
      ConfigureSettingsConfigureWiFiConnectionPage: { screen: ConfigureSettingsConfigureWiFiConnectionPage },
    },
    {
      headerMode: 'none',
    }
)

const Schedule = createStackNavigator(
    {
      SchedulesMainPage: { screen: SchedulesMainPage },
      SchedulesSettingsPage: { screen: SchedulesSettingsPage },
    },
    {
      headerMode: 'none',
    }
)

const GeneralSettings = createStackNavigator(
    {
      GeneralSettingsPage: { screen: GeneralSettingsPage },
      GeneralSettingsLanguagePage: { screen: GeneralSettingsLanguagePage },
      GeneralSettingsDegreeDisplayPage: { screen: GeneralSettingsDegreeDisplayPage },
    },
    {
      headerMode: 'none',
    }
)

const UserManual = createStackNavigator(
    {
      UserManualPage: { screen: UserManualPage },
    },
    {
      headerMode: 'none',
    }
)

const FAQ = createStackNavigator(
    {
      FAQPage: { screen: FAQPage },
    },
    {
      headerMode: 'none',
    }
)

const TAC = createStackNavigator(
    {
      TACPage: { screen: TACPage },
    },
    {
      headerMode: 'none',
    }
)

const HomeDashboardPageV2 = createStackNavigator(
    {
      _HomeDashboardPageV2: { screen: _HomeDashboardPageV2 },
      FamilyDetailsPage: { screen: FamilyDetailsPage },
      FamilyUserDetailsPage: { screen: FamilyUserDetailsPage },
      GatewaySmartWifiPageWizard: { screen: GatewaySmartWifiPageWizard },
      SubdeviceScanningPage: { screen: SubdeviceScanningPage },
    },
    {
      headerMode: 'none',
    }
)

const AppDrawer = createDrawerNavigator(
    {
      HomeDashboardPageV2: { screen: HomeDashboardPageV2 },
      Schedule: { screen: Schedule },
      VacationModeMainPage: { screen: VacationModeMainPage },
      Configure: { screen: Configure },
      Activation: { screen: Activation },
      GeneralSettings: { screen: GeneralSettings },
      FAQ: { screen: FAQ },
      UserManual: { screen: UserManual },
      TACPage: { screen: TAC},
    },
    {
      contentComponent: props => <SideBar {...props} />
    }
);

const App = createAppContainer(AppDrawer);

export default App;

