import React, {Fragment} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ActivityIndicator,
  AppState,
  Dimensions,
  Platform,
  PermissionsAndroid,
} from 'react-native';
const {width, height} = Dimensions.get("window");
import NetInfo from "@react-native-community/netinfo";
import PostloginMain from './PostloginMain'
import PreloginMain from './PreloginMain'
import APISourcerMqtt from "../../apis/APISourcerMqtt";
import WifiManager from "react-native-wifi-reborn"
import APISourcerPreference from '../../apis/APISourcerPreference';
import APISourcerExternal from '../../apis/APISourcerExternal';
import i18n from '../../i18n/i18n'
import {getTheme, StyleProvider} from 'native-base';
import customVariables from '../../themes/variable';
import moment from 'moment';


export default class RootMain extends React.Component
{
  handleFirstConnectivityChange = (connectionInfo) => {
    console.log(
        'First change, type: ' +
        connectionInfo.type +
        ', effectiveType: ' +
        connectionInfo.effectiveType,
    );

    if (connectionInfo.type === 'none'
        || connectionInfo.type === 'unknown')
    {
      APISourcerMqtt.getInstance().unsubscribeAllDevices();
      APISourcerMqtt.getInstance().disconnect();
    }
    else
    {
      this.tryMqttReconnect()
      new APISourcerPreference().applyArrangedSave();
      new APISourcerPreference().HOHOCARE_User_Device_List()
          .then((res) => {

          })
          .catch((e) => {

          })
    }
  }

  constructor(props)
  {
    super(props);
    this.state = {
      is_ready: false,
      is_postlogin: false,
      is_loading: false,
      appState: AppState.currentState,
    }


    GLOBAL.NAV_GOTO_BY_SESSION = () => {
      this.setState({
        is_ready: false,
        is_postlogin: false,
        is_prelogin: false,
      }, () => {
        GLOBAL.STORE.load_session()
            .then(() => {
              if (GLOBAL.STORE.session)
              {
                GLOBAL.NAV_GOTO_POSTLOGIN();
              }
              else
              {
                GLOBAL.NAV_GOTO_PRELOGIN();
              }
            })
      });
    }
    GLOBAL.NAV_GOTO_POSTLOGIN = () => {

      GLOBAL.STORE.load_session()
          .then(() => {
            return new APISourcerPreference().HOHOCARE_User_Get_Access_Info()
          })
          .then(() => {
            GLOBAL.APP_GETDEGREEDISPLAY();
            return GLOBAL.STORE.load_devices()
          })
          .then(() => {
            return GLOBAL.STORE.load_schedules()
          })
          .then(() => {
            return new APISourcerPreference().load()
                .then(() => {
                  this.setState({
                    is_ready: true,
                    is_postlogin: true,
                  }, () => {
                    this.tryGetWeatherFetch();
                    this.tryGetFirmwareLatest();
                  })
                })
                .catch(() => {
                  this.setState({
                    is_ready: true,
                    is_postlogin: true,
                  }, () => {
                    this.tryGetWeatherFetch();
                    this.tryGetFirmwareLatest();
                  })
                })
          })
    }

    GLOBAL.NAV_GOTO_PRELOGIN = () => {

      GLOBAL.STORE.unload_session()
          .then(() => {
            return new APISourcerPreference().HOHOCARE_User_Get_Access_Info()
          })
          .then(() => {
            return GLOBAL.STORE.unload_devices()
          })
          .then(() => {
            return GLOBAL.STORE.unload_schedules()
          })
          .then(() => {
            this.setState({
              is_ready: true,
              is_postlogin: false,
            })
          })
    }

    NetInfo.getConnectionInfo().then((connectionInfo) => {
      console.log(
          'Initial, type: ' +
          connectionInfo.type +
          ', effectiveType: ' +
          connectionInfo.effectiveType,
      );
    });
    NetInfo.removeEventListener('connectionChange', this.handleFirstConnectivityChange);
    NetInfo.addEventListener('connectionChange', this.handleFirstConnectivityChange);
  }

  componentDidMount(): void {
    AppState.addEventListener('change', this._handleAppStateChange);

    i18n.init()
        .then(() => {
          GLOBAL.NAV_GOTO_BY_SESSION();
        })

    this.tryGetWeatherFetch();
    this.tryGetFirmwareLatest();

    this.weatherFetchInterval = setInterval(() => {
      this.tryGetWeatherFetch();
    }, 1000*60*60)
  }

  componentWillUnmount(): void {
    AppState.removeEventListener('change', this._handleAppStateChange);
    if (this.weatherFetchInterval) {
      clearInterval(this.weatherFetchInterval);
      this.weatherFetchInterval = null;
    }
  }



  tryGetWeatherFetch()
  {
    if (this.state.is_ready
        && this.state.is_postlogin)
    {
      GLOBAL.UPDATE_FETCH_WEATHER((res) => {

      })
    }
  }


  tryGetFirmwareLatest()
  {
    if (this.state.is_ready)
    {
      GLOBAL.FIRMWARE_GET_LATEST((res) => {

      })
    }
  }



  _handleAppStateChange = (nextAppState) => {
    if (
        this.state.appState.match(/inactive|background/) &&
        nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
      this.tryMqttReconnect()
    }
    this.setState({appState: nextAppState});
  };




  tryMqttReconnect()
  {
    this.setState({is_loading: true}, () => {
      APISourcerMqtt.getInstance().connect(true, () => {
        this.setState({is_loading: false});
      })
    });
  }

  tryMqttDisconnect()
  {
    APISourcerMqtt.getInstance().disconnect();
    this.setState({is_loading: false});
  }


  componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
    if (prevState.is_postlogin
        && !this.state.is_postlogin)
    {
      this.tryMqttDisconnect();
    }

    else if (!prevState.is_postlogin
        && this.state.is_postlogin)
    {
      this.tryMqttReconnect();
    }
  }


  render()
  {
    if (!this.state.is_ready)
    {
      return (
          <View style={{position: `absolute`, left: 0, right: 0, width: null, height: null, top: 0, bottom: 0,
            backgroundColor: 'rgba(0,0,0,0.3)', alignItems: `center`, justifyContent: `center`}}>
            <ActivityIndicator size={'large'} color={'#fff'} animating/>
          </View>
      );
    }

    return (
        <Fragment>
          {
            this.state.is_postlogin ? (
                  <PostloginMain/>
            ) : (
                  <PreloginMain/>
            )
          }

          {
            false ? (
                <View
                    pointerEvents={`none`}
                    style={{position: `absolute`, bottom: 0, left: 20, right: 20, padding: 10, opacity: 1, backgroundColor: 'rgba(0,0,0,0.2)'}}>
                  <ToastDebugger/>
                </View>
            ) : null
          }

        </Fragment>
    )
  }
}


class ToastDebugger extends React.Component
{
  constructor(props) {
    super(props);
    this.state = {
      serial: "",
      from: "",
      type: "",
      json: "",
      dev: {},
    }
  }

  componentDidMount(): void {
    GLOBAL.OUTPUT_MQTT_RESPONSE = (({serial, from, type, json}) => {
      if (serial)
      {
        var dev = GLOBAL.STORE.get_device(serial);
      }

      this.setState({
        serial, from, type, json, dev,
      })
    })
  }

  render()
  {
    return (
        <View>
          <Text style={{fontSize: 10}}>Mqtt response: </Text>
          <Text style={{fontSize: 10}}>serial - {this.state.serial}</Text>
          <Text style={{fontSize: 10}}>from - {this.state.from}</Text>
          <Text style={{fontSize: 10}}>type - {this.state.type}</Text>
          <Text style={{fontSize: 10}}>json - {this.state.json}</Text>
          <Text style={{fontSize: 10}}>dev - {JSON.stringify(this.state.dev)}</Text>
        </View>
    )
  }
}

