import React, { Component } from 'react';
import { ScrollView } from "react-native";
import WifiManager from "react-native-wifi-reborn";
import { Container, Header, Left, Right, Body, Title, Accordion, View, Button, Icon, Content, List, ListItem, Text, H1, H2, H3, Form, Item, Input, Label } from 'native-base';
import BasePage from "../../base/BasePage";
import {StackActions} from "react-navigation";
import APISourcerPrelogin from '../../../apis/APISourcerPrelogin';
import BaseNavigationBar from '../../base/BaseNavigationBar';

export default class ActivationBeforeStartPage extends BasePage {

  constructor(props)
  {
    super(props);
    this.state = {
      username: '',
      password: '',
    }
  }

  renderInner() {

    return (
        <Container>

          <BaseNavigationBar {...this.props}
                             isBack
                             title={`Reset password`.i18n()}
          />

          <Content padder>

            <H1 style={{color: '#586C78', marginTop: 60, marginBottom: 30, textAlign: `center`}}>{`Reset password complete`.i18n()}</H1>



            <View style={{height: 50}}/>

            <Button
                info
                onPress={() => {

                  this.showLoading()

                  new APISourcerPrelogin().login(
                      this.props.navigation.state.params.username,
                      this.props.navigation.state.params.password
                  )
                      .then((res) => {
                        this.hideLoading()
                        GLOBAL.NAV_GOTO_BY_SESSION();
                      })
                      .catch((e) => {
                        this.hideLoading()
                        alert(e.message);
                      })

                }}
                style={{opacity: 1, justifyContent: `center`}}>
              <H3 style={{color: '#fff'}}>{`Login now!`.i18n()}</H3>
            </Button>

            <View style={{height: 20}}/>
            <Text style={{color: '#586C78', padding: 5, textAlign: `center`}}>{`or`.i18n()}</Text>
            <View style={{height: 20}}/>

            <Button
                light
                onPress={() => {
                  this.props.navigation.dispatch(StackActions.popToTop());
                }}
                style={{opacity: 1, justifyContent: `center`}}>
              <H3>{`Go back to title screen`.i18n()}</H3>
            </Button>


          </Content>

        </Container>
    );
  }
}
