import APISourcer from './APISourcer';

export default class APISourcerActivate extends APISourcer
{
  probe(access_token)
  {
    var didTimeOut = false;
    return new Promise((resolve, reject) => {
      const timeout = setTimeout(() => {
        didTimeOut = true;
        reject(new Error(`Request timed out`.i18n()));
      }, 10000);

      fetch(`${this.getBaseAccessPointUrl()}/api/device_info${access_token && `?access_token=${access_token}`}`, {
        method: 'GET',
        timeout: 10000,
        headers: {
          'Content-Type': 'text/json',
          'Accept': 'text/json',
        },
      }).then((response) => {
        clearTimeout(timeout);
        if (response.ok) {
          return response.json();
        }
      }).then((json) => {
        if (json) {
          if (json.serial != null) {
            if (json.fw_version != null) {
              json.is_valid = true;
            } else {
              json.is_valid = true;
            }
          }
        }

        if (didTimeOut) {
          return;
        }
        resolve(json);

      }).catch((error) => {

        if (didTimeOut) {
          return;
        }
        reject(error);

      });
    })
  }

  activate(access_token, ssid, pass, timezone)
  {
    return fetch(`${this.getBaseAccessPointUrl()}/api/activate`, {
      method: 'POST',
      headers: {
        'Content-Type': 'text/json',
        'Accept': 'text/json',
      },
      body: JSON.stringify({
        access_token: access_token,
        ssid: ssid,
        pass: pass,
        timezone: timezone,
        is_reset: "0",
      }),
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {
      return json;
    }).catch((error) => {
      throw error;
    });
  }


  activate_verify(access_token)
  {
    return fetch(`${this.getBaseAccessPointUrl()}/api/activate_verify`, {
      method: 'POST',
      headers: {
        'Content-Type': 'text/json',
        'Accept': 'text/json',
      },
      body: JSON.stringify({
        access_token: access_token,
        is_reset: "1",
      }),
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {
      return json;
    }).catch((error) => {
      throw error;
    });
  }



  activate_1328(access_token, ssid, pass, timezone)
  {
    return fetch(`${this.getBaseAccessPointUrl()}/api/activate`, {
      method: 'POST',
      headers: {
        'Content-Type': 'text/json',
        'Accept': 'text/json',
      },
      body: JSON.stringify({
        access_token: access_token,
        ssid: ssid,
        pass: pass,
        timezone: timezone,
        is_reset: "0",
        isAsync: 1,
      }),
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {
      return json;
    }).catch((error) => {
      throw error;
    });
  }


  activate_verify_1328(access_token)
  {
    return fetch(`${this.getBaseAccessPointUrl()}/api/activate_verify`, {
      method: 'POST',
      headers: {
        'Content-Type': 'text/json',
        'Accept': 'text/json',
      },
      body: JSON.stringify({
        access_token: access_token,
        is_reset: "0",
        isAsync: 1,
      }),
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {
      return json;
    }).catch((error) => {
      throw error;
    });
  }

  activate_resetme_1328(access_token)
  {
    return fetch(`${this.getBaseAccessPointUrl()}/api/resetme`, {
      method: 'POST',
      headers: {
        'Content-Type': 'text/json',
        'Accept': 'text/json',
      },
      body: JSON.stringify({
        access_token: access_token,
        isAsync: 1,
      }),
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {
      return json;
    }).catch((error) => {
      throw error;
    });
  }
}
