import React, { Component } from 'react';
import { Alert, ScrollView } from "react-native";
import WifiManager from "react-native-wifi-reborn";
import { Container, Header, Left, Right, Body, Title, Accordion, View, Button, Icon, Content, List, ListItem, Text, H1, H2, H3, Form, Item, Input, Label } from 'native-base';
import BasePage from "../../base/BasePage";
import BaseNavigationBar from '../../base/BaseNavigationBar';

export default class ActivationBeforeStartPage extends BasePage {

  constructor(props)
  {
    super(props);
    this.state = {
      ssid: '',
      password: '',
    }
  }

  componentDidMount(): void {
    WifiManager.getCurrentWifiSSID()
        .then((ssid) => {
          if (ssid === "<unknown ssid>")
          {
            this.setState({ssid: "", password: ""})
          }
          else
          {
            // alert("Your current connected wifi SSID is " + ssid)
            this.setState({ssid: ssid, password: ""})
          }
        }, () => {
          // alert('Cannot get current SSID!')
          this.setState({ssid: "", password: ""})
        })
  }

  renderInner() {

    var isDisabled =
        !this.state.ssid.trim()
        || !this.state.password.trim()
        || this.state.password.length < 8;

    return (
        <Container>

          <BaseNavigationBar {...this.props}
              isBack
              title={`Add D2Bros`.i18n()}
          />

          <Content padder>

            <H1 style={{color: '#586C78', marginTop: 60, marginBottom: 30, textAlign: `center`}}>{`Configure Wi-Fi connection`.i18n()}</H1>

            <Text style={{color: '#586C78', padding: 5, textAlign: `left`}}>{`In order to activate your D2Bros device, enter your home router Wi-Fi credentials below:`.i18n()}</Text>


            <Form>
              <Item stackedLabel>
                <Label>{`Your home router Wi-Fi SSID (2.4G only)`.i18n()}</Label>
                <Item style={{ backgroundColor: 'transparent', borderColor: 'transparent' }}>
                  <Input disabled={this.state.isBusy} value={this.state.ssid} autoCapitalize={false} autoCompleteType={'off'} onChangeText={(val) => {this.setState({ssid: val})}} />
                  {
                    this.state.ssid ? (
                        <Button transparent onPress={() => {
                          this.setState({ssid: ''})
                        }}>
                          <Icon color={`#586C78`} name="close" />
                        </Button>
                    ) : null
                  }
                </Item>
              </Item>
              <Item stackedLabel>
                <Label>{`Your home router Wi-Fi Password`.i18n()}</Label>
                <Item style={{ backgroundColor: 'transparent', borderColor: 'transparent' }}>
                  <Input disabled={this.state.isBusy} value={this.state.password} autoCapitalize={false} autoCompleteType={'off'} secureTextEntry onChangeText={(val) => {this.setState({password: val})}} />
                </Item>
              </Item>
            </Form>


            <View style={{height: 50}}/>

            <Button
                disabled={isDisabled}
                onPress={() => {
                  Alert.alert("D2Bros".i18n(), `${`Reminder: please make sure your target Wi-Fi router is a 2.4G channel.`.i18n()}

${`Please make sure below SSID and password combination is correct`.i18n()}: 

${`SSID`.i18n()}: ${this.state.ssid}
${`Password`.i18n()}: ${this.state.password}

${`An incorrect Wi-Fi settings may render your D2Bros unable to establish connection.`.i18n()}`, [
                    {
                      text: 'Not yet'.i18n(), onPress: () => {

                      },
                    },
                    {
                      text: 'Confirm'.i18n(), onPress: () => {
                        this.props.navigation.navigate("ActivationFindDevicePage",{
                          router: {
                            ssid: this.state.ssid,
                            password: this.state.password,
                          },
                        })
                      },
                    },
                  ])
                }}
                style={{opacity: isDisabled ? 0.5 : 1, backgroundColor: '#4fadf9', justifyContent: `center`}}>
              <H3 style={{color: '#fff'}}>{`Done! Proceed to next step`.i18n()}</H3>
            </Button>

          </Content>

        </Container>
    );
  }
}
