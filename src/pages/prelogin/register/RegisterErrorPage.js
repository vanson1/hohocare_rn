import React, { Component } from 'react';
import { ScrollView } from "react-native";
import WifiManager from "react-native-wifi-reborn";
import { Container, Header, Left, Right, Body, Title, Accordion, View, Button, Icon, Content, List, ListItem, Text, H1, H2, H3, Form, Item, Input, Label } from 'native-base';
import BasePage from "../../base/BasePage";
import {StackActions} from "react-navigation";
import BaseNavigationBar from '../../base/BaseNavigationBar';

export default class ActivationBeforeStartPage extends BasePage {

  constructor(props)
  {
    super(props);
    this.state = {
      username: '',
      password: '',
    }
  }

  renderInner() {

    var isDisabled =
        !this.state.username.trim()
        || !this.state.password.trim()
        || this.state.password.length < 8;

    return (
        <Container>

          <BaseNavigationBar {...this.props}
                             isBack
                             title={`Register`.i18n()}
          />

          <Content padder>

            <H1 style={{color: '#586C78', marginTop: 60, marginBottom: 30, textAlign: `center`}}>{`Registration error`.i18n()}</H1>

            <View style={{height: 30}}/>

            <Text style={{color: '#586C78', padding: 5, textAlign: `center`}}>{`Reason: Email account already registered`.i18n()}</Text>


            <View style={{height: 50}}/>

            <Button
                info
                onPress={() => {
                  this.props.navigation.pop();
                }}
                style={{opacity: 1, backgroundColor: '#4fadf9', justifyContent: `center`}}>
              <H3 style={{color: '#fff'}}>{`Go back to registration form`.i18n()}</H3>
            </Button>


          </Content>

        </Container>
    );
  }
}
