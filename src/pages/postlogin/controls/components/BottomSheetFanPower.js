import React, { Component } from 'react';
import { Image, ImageBackground, Dimensions, SafeAreaView } from 'react-native';
import { View, Container, Header, Left, Right, Body, Title, Accordion, Button, Icon, Content, List, ListItem, Text, H1, H2, H3 } from 'native-base';
import BasePage from "../../../base/BasePage";
import APISourcerMqtt from "../../../../apis/APISourcerMqtt";
import { withNavigationFocus } from "react-navigation";
import moment from 'moment';

const {width, height} = Dimensions.get("window");


class BottomSheetFanPower extends React.Component {
  constructor(props)
  {
    super(props);
    this.state = {

    }
  }

  componentDidMount(): void {

  }

  render()
  {
    return (
        <Container>
          <View style={{flexDirection: `row`, alignItems: `center`, justifyContent: `space-around`}}>
            <Text style={{fontWeight: `bold`, padding: 10, fontSize: 16, color: '#666'}}>
              {`Fan power`.i18n()}
            </Text>
          </View>
          <View style={{flexDirection: `row`, width: width}}>
            <View style={{marginTop: 15, flex: 1, flexDirection: `row`, alignItems: `center`, justifyContent: `space-around`}}>
              <Button transparent
                      onPress={() => {
                        this.props.onValueChangeItem({f: 1})
                      }}
                      style={{flex: 1, alignItems: `center`, justifyContent: `center`}}>
                <Text style={{color: this.props.item.f == 1 ? '#0ca4e3' : '#666', fontSize: 28, fontWeight: 'bold'}}>{`1`}</Text>
              </Button>
              <Button transparent
                      onPress={() => {
                        this.props.onValueChangeItem({f: 2})
                      }}
                      style={{flex: 1, alignItems: `center`, justifyContent: `center`}}>
                <Text style={{color: this.props.item.f == 2 ? '#0ca4e3' : '#666', fontSize: 28, fontWeight: 'bold'}}>{`2`}</Text>
              </Button>
              <Button transparent
                      onPress={() => {
                        this.props.onValueChangeItem({f: 3})
                      }}
                      style={{flex: 1, alignItems: `center`, justifyContent: `center`}}>
                <Text style={{color: this.props.item.f == 3 ? '#0ca4e3' : '#666', fontSize: 28, fontWeight: 'bold'}}>{`3`}</Text>
              </Button>
              <Button transparent
                      onPress={() => {
                        this.props.onValueChangeItem({f: 4})
                      }}
                      style={{flex: 1, alignItems: `center`, justifyContent: `center`}}>
                <Text style={{color: this.props.item.f == 4 ? '#0ca4e3' : '#666', fontSize: 28, fontWeight: 'bold'}}>{`4`}</Text>
              </Button>
              <Button transparent
                      onPress={() => {
                        this.props.onValueChangeItem({f: 5})
                      }}
                      style={{flex: 1, alignItems: `center`, justifyContent: `center`}}>
                <Text style={{color: this.props.item.f == 5 ? '#0ca4e3' : '#666', fontSize: 28, fontWeight: 'bold'}}>{`5`}</Text>
              </Button>
              <Button transparent
                      onPress={() => {
                        this.props.onValueChangeItem({f: 10})
                      }}
                      style={{flex: 1, alignItems: `center`, justifyContent: `center`}}>
                <Image style={{height: 28, resizeMode: `contain`}}
                       source={this.props.item.f == 10 ? GLOBAL.IMAGE.d_ac_ic_mode_auto_o : GLOBAL.IMAGE.d_ac_ic_mode_auto_n}/>
              </Button>
              <Button transparent
                      onPress={() => {
                        this.props.onValueChangeItem({f: 11})
                      }}
                      style={{flex: 1, alignItems: `center`, justifyContent: `center`}}>
                <Image style={{height: 28, resizeMode: `contain`}}
                       source={this.props.item.f == 11 ? GLOBAL.IMAGE.d_ac_ic_mode_eco_o : GLOBAL.IMAGE.d_ac_ic_mode_eco_n}/>
              </Button>
            </View>
          </View>
        </Container>
    )
  }
}

export default BottomSheetFanPower;

