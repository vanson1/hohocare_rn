import APISourcer from './APISourcer';
import {
  AsyncStorage
} from 'react-native'

export default class APISourcerPreference extends APISourcer
{
  static isArrangedSave = false;

  HOHOCARE_User_Device_Add(params)
  {
    var {
      alias,
      serial,
    } = (params || {});

    return fetch(`${this.getBaseAPIUrl()}/HOHOCARE_User_Device_Add` , {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'x-access-token': GLOBAL.STORE.session.accessToken,
      },
      body: JSON.stringify(params),
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {
      if (json.success)
      {
        return json;
      }
      else
      {
        throw Error(json.message);
      }
    }).catch((error) => {
      throw error;
    });
  }

  HOHOCARE_User_Device_List(params)
  {
    if (!GLOBAL.STORE
        || !GLOBAL.STORE.session
        || !GLOBAL.STORE.session.accessToken)
    {
      return new Promise((resolve, reject) => {
        resolve(null);
      })
    }

    var {
        serial,
    } = (params || {});

    var body = {};
    if (serial) {body.serial = serial;}

    return fetch(`${this.getBaseAPIUrl()}/HOHOCARE_User_Device_List` , {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'x-access-token': GLOBAL.STORE.session.accessToken,
      },
      body: JSON.stringify(body),
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {
      if (json.success)
      {
        if (json.data && json.data.items)
        {
          for (var k = 0; k < json.data.items.length; k++)
          {
            for (var m = 0; m < GLOBAL.STORE.devices.length; m++)
            {
              var device = GLOBAL.STORE.devices[m];
              if (device.serial === json.data.items[k].serial)
              {
                device.alias = json.data.items[k].alias;
              }
            }
          }
          GLOBAL.STORE.update_devices();
          return json;
        }
      }
      else
      {
        throw Error(json.message);
      }
    }).catch((error) => {
      throw error;
    });
  }

  HOHOCARE_User_Get_Access_Info()
  {
    return fetch(`${this.getBaseAPIUrl()}/HOHOCARE_User_Get_Access_Info` , {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        lang: global.APP_GETLANGUAGE(),
      })
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {
      if (json.success)
      {
        AsyncStorage.setItem('__ACCESS_INFO', JSON.stringify(json));
        GLOBAL.default_mqtt_url = json.data.default_mqtt_url;
        GLOBAL.default_api_url = json.data.default_api_url;
        GLOBAL.url_tc = json.data.url_tc;
        GLOBAL.url_disclaimer = json.data.url_disclaimer;
        GLOBAL.url_usermanual = json.data.url_usermanual;
        GLOBAL.url_faq = json.data.url_faq;
        GLOBAL.SHOULD_SHOW_DEBUG_BUTTON = json.data.SHOULD_SHOW_DEBUG_BUTTON;
        GLOBAL.MQTT_WS_URL = json.data.MQTT_WS_URL;
        GLOBAL.EXT_SERVER_WEATHER_URL = json.data.EXT_SERVER_WEATHER_URL;
        GLOBAL.BASE_ACCESS_POINT_URL = json.data.BASE_ACCESS_POINT_URL;
        GLOBAL.FW_SIGNAL_ACK_TIMEOUT = json.data.FW_SIGNAL_ACK_TIMEOUT;
        return json;
      }
      else
      {
        throw Error(json.message);
      }
    }).catch((error) => {
      return AsyncStorage.getItem('__ACCESS_INFO')
          .then((js) => {
            if (js)
            {
              console.log(js)
              var json = JSON.parse(js);
              GLOBAL.default_mqtt_url = json.data.default_mqtt_url;
              GLOBAL.default_api_url = json.data.default_api_url;
              GLOBAL.url_tc = json.data.url_tc;
              GLOBAL.url_disclaimer = json.data.url_disclaimer;
              GLOBAL.url_usermanual = json.data.url_usermanual;
              GLOBAL.url_faq = json.data.url_faq;
              GLOBAL.SHOULD_SHOW_DEBUG_BUTTON = json.data.SHOULD_SHOW_DEBUG_BUTTON;
              GLOBAL.MQTT_WS_URL = json.data.MQTT_WS_URL;
              GLOBAL.EXT_SERVER_WEATHER_URL = json.data.EXT_SERVER_WEATHER_URL;
              GLOBAL.BASE_ACCESS_POINT_URL = json.data.BASE_ACCESS_POINT_URL;
              GLOBAL.FW_SIGNAL_ACK_TIMEOUT = json.data.FW_SIGNAL_ACK_TIMEOUT;
              return json;
            }
            return null;
          })
    })
  }



  arrangeSave(i)
  {
    // alert("ARRANGE SAVE (" + i + ")");
    APISourcerPreference.isArrangedSave = true;
    if (i == 3)
    {
      if (this.cooldown) {clearTimeout(this.cooldown); this.cooldown = null;}
      this.cooldown = setTimeout(() => {
        this.applyArrangedSave();
      }, 10000);
    }
  }

  applyArrangedSave()
  {
    if (APISourcerPreference.isArrangedSave && !APISourcerPreference.isArrangedSaveBusy)
    {
      this.save();
    }
  }

  save()
  {
    APISourcerPreference.isArrangedSaveBusy = true;
    var json = {
      devices: (GLOBAL.STORE.devices || []).map((it) => {
        var _item = Object.assign({}, it);
        return _item;
      }),
      schedules: GLOBAL.STORE.schedules,
      vacation_mode: GLOBAL.STORE.vacation_mode,
    }

    return fetch(`${this.getBaseAPIUrl()}/HOHOCARE_User_Preference_Save` , {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'x-access-token': GLOBAL.STORE.session.accessToken,
      },
      body: JSON.stringify({
        json: json,
      }),
    }).then((response) => {
      APISourcerPreference.isArrangedSaveBusy = false;
      if (response.ok) {
        return response.json();
      } else {
        this.arrangeSave(1);
      }
    }).then((json) => {
      APISourcerPreference.isArrangedSaveBusy = false;
      if (json.success)
      {
        // alert("SAVE DONE");
        APISourcerPreference.isArrangedSave = false;
        return json;
      }
      else
      {
        this.arrangeSave(2);
        throw Error(json.message);
      }
    }).catch((error) => {
      APISourcerPreference.isArrangedSaveBusy = false;
      this.arrangeSave(3);
      throw error;
    });
  }

  load()
  {
    return fetch(`${this.getBaseAPIUrl()}/HOHOCARE_User_Preference_Load` , {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'x-access-token': GLOBAL.STORE.session.accessToken,
      },
      body: JSON.stringify({
      }),
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {
      if (json.success)
      {
        json.data = json.data || {
          json: {},
        }

        if (json.data)
        {
          if (json.data.json)
          {
            var j = json.data.json;
            var {
              devices,
              schedules,
              vacation_mode,
            } = j;

            var items = devices || [];

            try
            {

              for (var k in items)
              {
                items[k].payload = {};
                (items[k] && items[k].info || {}).is_online = 0;

                for (var m in GLOBAL.STORE.devices)
                {
                  if (GLOBAL.STORE.devices[m].serial === items[k].serial)
                  {
                    items[k].payload = GLOBAL.STORE.devices[m].payload || {};
                    (items[k].info && items[k].info || {}).is_online = (GLOBAL.STORE.devices[m] && GLOBAL.STORE.devices[m].info || {}).is_online || 0;
                  }
                }
              }
            }
            catch (e)
            {
              alert(e.message)
            }

            GLOBAL.STORE.devices = items || [];
            GLOBAL.STORE.update_devices();

            GLOBAL.STORE.schedules = schedules || [];
            GLOBAL.STORE.update_schedules();

            if (vacation_mode)
            {
              GLOBAL.STORE.vacation_mode = vacation_mode;
              GLOBAL.STORE.update_vacation_mode(vacation_mode.is_enabled);
            }
            else
            {
              GLOBAL.STORE.update_vacation_mode(false);
            }

            GLOBAL.LAST_UPDATED_TIME_UPDATE(true);
          }
        }
        return json;
      }
      else
      {
        throw Error(json.message);
      }
    }).catch((error) => {
      throw error;
    });
  }
}
