import React, { Component } from 'react';
import { ScrollView, Image, Alert, Keyboard } from "react-native";
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3, Form, Item, Label, Input,
} from "native-base";
import BasePage from "../../base/BasePage";
import APISourcerPrelogin from "../../../apis/APISourcerPrelogin";

import Smartconfig from 'react-native-smartconfig-esp';

import { PermissionsAndroid } from 'react-native';
import WifiManager from "react-native-wifi-reborn";
import APISourcerMqtt from '../../../apis/APISourcerMqtt';


export default class WelcomePage extends BasePage {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
    }
  }



  wifi_brute_connect()
  {
    var connect = () => {

      WifiManager.getCurrentWifiSSID().then(
          ssid => {
            console.log("Your current connected wifi SSID is " + ssid);
            Alert.alert(
                "Your current connected wifi SSID is " + ssid,
                "Your current connected wifi SSID is " + ssid,
                [
                  {text: "Next", onPress: () => {

                      // WifiManager.connectToProtectedSSID("ESPsoftAP_01", "pass-to-soft-AP", false).then(
                      //     () => {
                      //       console.log("Connected successfully!");
                      //       alert("Connected successfully!")
                      //     },
                      //     () => {
                      //       console.log("Connection failed!");
                      //       alert("Connection failed!")
                      //     }
                      // );
                    }}
                ]
            )

          },
          () => {
            console.log("Cannot get current SSID!");
            alert("Cannot get current SSID!")
          }
      );
    }

    if (Platform.OS === "ios")
    {
      connect();
    }
    else
    {
      PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Location permission is required for WiFi connections',
            message:
                'This app needs location permission as this is required  ' +
                'to scan for wifi networks.',
            buttonNegative: 'DENY',
            buttonPositive: 'ALLOW',
          },
      )
          .then((granted) => {
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {

              connect();

            } else {

            }

          })
          .catch((e) => {

          })
    }
  }



  sc_aggregate_results(is_force)
  {
    this.hideLoading()

    if (!is_force)
    {
      if (this.sc_result_from_mqtt && this.sc_result_from_udp)
      {
        alert(`Case A: \n\nmqtt[${this.sc_result_from_mqtt && JSON.stringify(this.sc_result_from_mqtt)}] \n\nudp[${this.sc_result_from_udp && JSON.stringify(this.sc_result_from_udp)}]`)
        APISourcerMqtt.getInstance().unsubscribeSmartConfig("G0");
      }
    }
    else if (is_force)
    {
      if (this.sc_result_from_mqtt)
      {
        alert(`Case B: \n\nmqtt[${this.sc_result_from_mqtt && JSON.stringify(this.sc_result_from_mqtt)}] \n\nudp[${this.sc_result_from_udp && JSON.stringify(this.sc_result_from_udp)}]`)
        APISourcerMqtt.getInstance().unsubscribeSmartConfig("G0");
      }
    }
  }



  sc_begin()
  {
    var ___begin = (ssid, bssid, password) => {
      Alert.alert(
          "Smart config",
          `Are you sure to connect the WiFi gateway to the router below? :\n\n SSID: ${ssid} \nBSSID: ${bssid} \nPassword: ${password}`,
          [
            {text: `No`},
            {text: `Confirm`, onPress: () => {

                Keyboard.dismiss();

                this.showLoading();
                this.sc_result_from_mqtt = null;
                this.sc_result_from_udp = null;
                APISourcerMqtt.getInstance().setSmartconfigAckCallback((json) => {
                  this.sc_result_from_mqtt = json;
                  this.sc_aggregate_results();
                })

                APISourcerMqtt.getInstance().subscribeSmartConfig("G0");

                Smartconfig.start({
                  type: 'esptouch', //or airkiss, now doesn't not effect
                  ssid: ssid,
                  bssid: bssid, //"" if not need to filter (don't use null)
                  password: password,
                  taskCount: 1,
                  timeout: 50000, //now support (default 30000)
                }).then((results) => {
                  //Array of device success do smartconfig
                  console.log(results);

                  this.sc_result_from_udp = results;
                  this.sc_aggregate_results();

                  /*[
                    {
                      'bssid': 'device-bssi1', //device bssid
                      'ipv4': '192.168.1.11' //local ip address
                    },
                    {
                      'bssid': 'device-bssi2', //device bssid
                      'ipv4': '192.168.1.12' //local ip address
                    },
                    ...
                  ]*/
                }).catch((error) => {
                  console.log(error);
                  alert("error: " + error.message)
                  this.hideLoading()
                  APISourcerMqtt.getInstance().unsubscribeSmartConfig("G0");
                });

              }},
          ]
      )
    }





    if (Platform.OS === "ios")
    {
      ___begin(this.state.username, "", this.state.password);
    }
    else
    {
      PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Location permission is required for WiFi connections',
            message:
                'This app needs location permission as this is required  ' +
                'to scan for wifi networks.',
            buttonNegative: 'DENY',
            buttonPositive: 'ALLOW',
          },
      )
          .then((granted) => {
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {

              WifiManager.getBSSID((bssid) => {
                WifiManager.getCurrentWifiSSID()
                    .then((ssid) => {
                      ___begin(this.state.username || ssid, bssid, this.state.password);
                    })
              })

            } else {

            }

          })
          .catch((e) => {

          })
    }

  }

  sc_stop()
  {
    Smartconfig.stop();
  }



  renderInner() {
    return (
        <Container>

          <Content scrollEnabled={true} padder>


            <View style={{flexDirection: `row`, justifyContent: `center`}}>
              <Image source={GLOBAL.IMAGE.logo_hohocare} style={{width: 180, height: 200, alignItems: `center`, resizeMode: `contain`}}/>
            </View>


            <Form>
              <Item>
                <Label>{`Phone`.i18n()}</Label>
                <Input value={this.state.username} autoCapitalize={false} autoCompleteType={'off'} onChangeText={(val) => {this.setState({username: val})}} />
              </Item>
              <Item>
                <Label>{`Password`.i18n()}</Label>
                <Input value={this.state.password} autoCapitalize={false} autoCompleteType={'off'} secureTextEntry onChangeText={(val) => {this.setState({password: val})}} />
              </Item>
            </Form>

            <View style={{height: 40}}/>


            <Button info
                onPress={() => {

                  this.showLoading();
                  new APISourcerPrelogin().login(
                      this.state.username,
                      this.state.password
                      )
                      .then((res) => {
                        this.hideLoading();
                        GLOBAL.NAV_GOTO_BY_SESSION();
                      })
                      .catch((e) => {
                        this.hideLoading();
                        alert("Error: " + e.message);
                      })
                }}
                style={{justifyContent: `center`}}>
              <H3 style={{color: '#fff'}}>{`Login`.i18n()}</H3>
            </Button>

            <View style={{height: 10}}/>

            <Button light
                onPress={() => {
                  this.props.navigation.navigate("RegisterFormPage");
                }}
                style={{justifyContent: `center`}}>
              <H3 style={{}}>{`Register`.i18n()}</H3>
            </Button>


            <View style={{height: 15}}/>

            <Button transparent
                    onPress={() => {
                      this.props.navigation.navigate("ForgotPasswordFormRequestPage");
                    }}
                    style={{justifyContent: `center`}}>
              <Text style={{}}>{`Forgot Password?`.i18n()}</Text>
            </Button>


            <View style={{height: 15}}/>

            <Button transparent
                    onPress={() => {
                      this.props.navigation.navigate("GeneralSettingsLanguagePage", {
                        isBack: true,
                        callback: (() => {
                          this.forceUpdate()
                        })
                      });
                    }}
                    style={{justifyContent: `center`}}>
              <Text style={{}}>{`Change language`.i18n()}</Text>
            </Button>


            <Button transparent
                    onPress={() => {
                      this.sc_begin()
                    }}
                    style={{justifyContent: `center`}}>
              <Text style={{}}>{`Smart Config Test begin`.i18n()}</Text>
            </Button>

            <Button transparent
                    onPress={() => {
                      this.sc_stop()
                    }}
                    style={{justifyContent: `center`}}>
              <Text style={{}}>{`Smart Config Test stop`.i18n()}</Text>
            </Button>

            <Button transparent
                    onPress={() => {
                      this.wifi_brute_connect()
                    }}
                    style={{justifyContent: `center`}}>
              <Text style={{}}>{`Wifi Connect brute force`.i18n()}</Text>
            </Button>

            <Button transparent
                    onPress={() => {
                      if (Platform.OS === 'ios')
                      {
                        WifiManager.disconnectFromSSID("ESPsoftAP_01")
                            .then(() => {
                              alert('disconnected')
                            })
                            .catch((e) => {
                              alert(e.message)
                            })
                      }
                      else
                      {
                        WifiManager.disconnect()
                            .then(() => {
                              alert('disconnected')
                            })
                            .catch((e) => {
                              alert(e.message)
                            })
                      }
                    }}
                    style={{justifyContent: `center`}}>
              <Text style={{}}>{`Wifi Disconnect`.i18n()}</Text>
            </Button>

            <View style={{height: 15}}/>
            <Text style={{textAlign: `center`, fontSize: 11}} note>{`${`Version`.i18n()}: ${GLOBAL.VERSION}`}</Text>




          </Content>


        </Container>
    );
  }
}
