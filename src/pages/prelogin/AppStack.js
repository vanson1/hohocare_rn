import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
} from 'react-native';
import {
  Drawer,
  Text,
} from 'native-base';
import {
  createStackNavigator,
  createAppContainer,
} from "react-navigation";

import WelcomePage from "./welcome/WelcomePage";
import RegisterFormPage from "./register/RegisterFormPage";
import RegisterCompletePage from "./register/RegisterCompletePage";
import RegisterErrorPage from "./register/RegisterErrorPage";
import ForgotPasswordFormRequestPage from "./forgotpassword/ForgotPasswordFormRequestPage";
import ForgotPasswordFormResetPage from "./forgotpassword/ForgotPasswordFormResetPage";
import ForgotPasswordFormVerificationPage from "./forgotpassword/ForgotPasswordFormVerificationPage";
import ForgotPasswordResetCompletionPage from "./forgotpassword/ForgotPasswordResetCompletionPage";
import OfflineTestBeforeStartPage from "./offlinetest/OfflineTestBeforeStartPage";
import OfflineTestControlAirConditionerPage from "./offlinetest/OfflineTestControlAirConditionerPage";
import OfflineTestFindDevicePage from "./offlinetest/OfflineTestFindDevicePage";
import OfflineTestRawDataPage from './offlinetest/OfflineTestRawDataPage';
import RegisterFormVerificationPage from './register/RegisterFormVerificationPage';
import TACPage from './misc/tac/TACPage';
import GeneralSettingsLanguagePage from './misc/generalsettings/GeneralSettingsLanguagePage';

const Prelogin = createStackNavigator(
    {
      GeneralSettingsLanguagePage: { screen: GeneralSettingsLanguagePage },
      WelcomePage: { screen: WelcomePage },
      RegisterFormPage: { screen: RegisterFormPage },
      RegisterCompletePage: { screen: RegisterCompletePage },
      RegisterErrorPage: { screen: RegisterErrorPage },
      RegisterFormVerificationPage: {screen: RegisterFormVerificationPage },
      ForgotPasswordFormRequestPage: { screen: ForgotPasswordFormRequestPage },
      ForgotPasswordFormResetPage: { screen: ForgotPasswordFormResetPage },
      ForgotPasswordFormVerificationPage: { screen: ForgotPasswordFormVerificationPage },
      ForgotPasswordResetCompletionPage: { screen: ForgotPasswordResetCompletionPage },
      OfflineTestBeforeStartPage: { screen: OfflineTestBeforeStartPage },
      OfflineTestControlAirConditionerPage: { screen: OfflineTestControlAirConditionerPage },
      OfflineTestFindDevicePage: { screen: OfflineTestFindDevicePage },
      TACPage: { screen: TACPage },
    },
    {
      headerMode: 'none',
    }
)

const App = createAppContainer(Prelogin);

export default App;

