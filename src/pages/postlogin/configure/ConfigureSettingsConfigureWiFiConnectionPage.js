import React, { Component } from 'react';
import { Keyboard, ActivityIndicator, Alert, ScrollView} from 'react-native';
import WifiManager from "react-native-wifi-reborn";
import { Container, Header, Left, Right, Body, Title, Accordion, View, Button, Icon, Content, List, ListItem, Text, H1, H2, H3, Form, Item, Input, Label } from 'native-base';
import BasePage from "../../base/BasePage";
import BaseNavigationBar from '../../base/BaseNavigationBar';
import APISourcerMqtt from '../../../apis/APISourcerMqtt';

export default class ConfigureSettingsConfigureWiFiConnectionPage extends BasePage {

  constructor(props)
  {
    super(props);
    this.state = {
      ssid: '',
      password: '',
      isBusy: false,
      isActive: false,
    }
  }

  componentDidMount(): void {
    WifiManager.getCurrentWifiSSID()
        .then((ssid) => {
          if (ssid === "<unknown ssid>")
          {
            this.setState({ssid: "", password: ""})
          }
          else
          {
            // alert("Your current connected wifi SSID is " + ssid)
            this.setState({ssid: ssid, password: ""})
          }
        }, () => {
          // alert('Cannot get current SSID!')
          this.setState({ssid: "", password: ""})
        })

    var serial = this.props.navigation.state.params.item.serial;
    GLOBAL.LISTEN_DEVICE_ACK(serial, this.onAck.bind(this))

    this.state.isActive = true;
  }

  componentWillUnmount(): void {
    var serial = this.props.navigation.state.params.item.serial;
    GLOBAL.UNLISTEN_DEVICE_ACK(serial, this.onAck.bind(this))
    if (this.timeout1) clearTimeout(this.timeout1);
    if (this.timeout2) clearTimeout(this.timeout2);

    this.state.isActive = false;
  }

  doReactivate()
  {
    Keyboard.dismiss();
    this.timeout1 = setTimeout(() => {
      this.setState({
        isBusy: false,
      }, () => {
        if (this.state.isActive)
        {
          alert("Connection invalid. Please check your Wi-Fi settings. Connect to an Wi-Fi device prefixed with \"D2Bros-\". Then try again.".i18n());
        }
      });
    }, 10000);

    this.setState({
      isBusy: true,
    }, () => {
      Keyboard.dismiss();
      var serial = this.props.navigation.state.params.item.serial;
      APISourcerMqtt.getInstance().publishCommandAC(serial, 'reactivate_wifi', {
        ssid: this.state.ssid,
        pass: this.state.password,
        is_reset: "0",
        timezone: 8,
      });
    })
  }

  onAck()
  {
    var json = Object.assign({}, GLOBAL.EMIT_DEVICE_ACK_JSON);
    if (json.hash === 'reactivate_wifi')
    {
      if (this.timeout1) clearTimeout(this.timeout1);
      this.createTimeout2();
    }
    else if (json.hash === 'ping')
    {
      if (this.timeout2) clearTimeout(this.timeout2);
      this.props.navigation.pop();
    }
  }



  createTimeout2()
  {
    Keyboard.dismiss();
    this.timeout2 = setTimeout(() => {
      this.setState({
        isBusy: false,
      }, () => {
        if (this.state.isActive)
        {
          Alert.alert(
              "error".i18n(),
              "Connection invalid. Please check your Wi-Fi settings. If your D2Bros device has entered AP mode, you may go through \"Add new D2Bros Device\" to re-activate the device.".i18n(),
              [
                {
                  text: `Exit`.i18n(), onPress: () => {
                    this.props.navigation.pop();
                  }
                },
                {
                  text: `wait`.i18n(), onPress: () => {

                    this.setState({
                      isBusy: true,
                    }, () => {
                      this.createTimeout2();
                    })

                  }
                },
              ],
              {
                cancelable: false,
              })
        }
      });
    }, 60000);
  }




  renderInner() {

    var isDisabled =
        !this.state.ssid.trim()
        || !this.state.password.trim()
        || this.state.password.length < 8;

    return (
        <Container>

          <BaseNavigationBar {...this.props}
                             isBack
                             title={`Re-configure router connection`.i18n()}
          />

          <Content padder>

            <H1 style={{color: '#586C78', marginTop: 60, marginBottom: 30, textAlign: `center`}}>{`Configure Wi-Fi connection`.i18n()}</H1>

            <Text style={{color: '#586C78', padding: 5, textAlign: `left`}}>{`In order to activate your D2Bros device, enter your home router Wi-Fi credentials below:`.i18n()}</Text>


            <Form>
              <Item stackedLabel>
                <Label>{`Your home router Wi-Fi SSID (2.4G only)`.i18n()}</Label>
                <Item style={{ backgroundColor: 'transparent', borderColor: 'transparent' }}>
                  <Input disabled={this.state.isBusy} value={this.state.ssid} autoCapitalize={false} autoCompleteType={'off'} onChangeText={(val) => {this.setState({ssid: val})}} />
                  {
                    this.state.ssid ? (
                        <Button transparent onPress={() => {
                          this.setState({ssid: ''})
                        }}>
                          <Icon color={`#586C78`} name="close" />
                        </Button>
                    ) : null
                  }
                </Item>
              </Item>
              <Item stackedLabel>
                <Label>{`Your home router Wi-Fi Password`.i18n()}</Label>
                <Input disabled={this.state.isBusy} value={this.state.password} autoCapitalize={false} autoCompleteType={'off'} secureTextEntry onChangeText={(val) => {this.setState({password: val})}} />
              </Item>
            </Form>


            <View style={{height: 50}}/>

            <Button
                disabled={isDisabled}
                onPress={() => {
                  Alert.alert("D2Bros".i18n(), `${`Reminder: please make sure your target Wi-Fi router is a 2.4G channel.`.i18n()}

${`Please make sure below SSID and password combination is correct`.i18n()}: 

${`SSID`.i18n()}: ${this.state.ssid}
${`Password`.i18n()}: ${this.state.password}

${`An incorrect Wi-Fi settings may render your D2Bros unable to establish connection.`.i18n()}`, [
                    {
                      text: 'Not yet'.i18n(), onPress: () => {

                      },
                    },
                    {
                      text: 'Confirm'.i18n(), onPress: () => {

                        this.doReactivate();
                      },
                    },
                  ])
                }}
                style={{opacity: isDisabled ? 0.5 : 1, backgroundColor: '#4fadf9', justifyContent: `center`}}>
              <H3 style={{color: '#fff'}}>{`Proceed`.i18n()}</H3>
            </Button>

          </Content>

          {
            this.state.isBusy ? (
                <View style={{position: `absolute`, left: 0, right: 0, width: null, height: null, top: 0, bottom: 0,
                  backgroundColor: 'rgba(0,0,0,0.3)', alignItems: `center`, justifyContent: `center`}}>
                  <ActivityIndicator size={'large'} color={'#fff'} animating/>
                </View>
            ) : null
          }

        </Container>
    );
  }
}
