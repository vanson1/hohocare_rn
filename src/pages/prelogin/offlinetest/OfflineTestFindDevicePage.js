import React, { Component } from 'react';
import {ScrollView, Image, ActivityIndicator, SafeAreaView, Platform, PermissionsAndroid} from 'react-native';
import WifiManager from "react-native-wifi-reborn"
import { Container, Header, Left, Right, Body, Title, Accordion, View, Button, Icon, Content, List, ListItem, Text, H1, H2, H3 } from 'native-base';
import BasePage from "../../base/BasePage";
import APISourcerActivate from '../../../apis/APISourcerActivate';
import Prompt from 'rn-prompt';

export default class OfflineTestFindDevicePage extends BasePage {

  constructor(props)
  {
    super(props);
    this.state = {
      is_loading: false,
      wifi_list: [],
      ap_ssid: "",
      serial: "",
      access_token: "",
      passcode: "",
    }
  }

  tryProbe(is_raw)
  {
    this.setState({
      is_loading: true,
    }, () => {

      var passcode = GLOBAL.GET_ACCESS_TOKEN();

      if (!passcode)
      {
        passcode = "UVRBd01EQXdNREU9";
      }

      if (this.state.passcode)
      {
        passcode = this.state.passcode;
      }

      if (this.state.passcode && (this.state.passcode.toLowerCase().trim() === "skip"))
      {
        passcode = "LJSUVASD123DDN2E9U";
      }

      new APISourcerActivate().probe(passcode)
          .then((res) => {
            if (res.is_valid)
            {

              if (res.atr)
              {
                if (!res.atc)
                {
                  this.hideLoading();
                  this.setState({
                    is_loading: false,
                  }, () => {
                    this.setState({
                      promptVisible: true,
                    })
                  });
                  return;
                }
              }

              var atp = "";
              if (res.access_token_priv)
              {
                // try to assign "access_token_priv" into the device
                atp = res.access_token_priv;
              }
              else
              {
                atp = passcode;
              }

              var serial = res.serial;



              this.state.passcode = "";

              if (is_raw)
              {
                this.props.navigation.navigate("OfflineTestRawDataPage", Object.assign(
                    {},
                    this.props.navigation.state.params,
                    {
                      serial: serial,
                    }
                ))
              }
              else
              {
                this.props.navigation.navigate("OfflineTestControlAirConditionerPage", Object.assign(
                    {},
                    this.props.navigation.state.params,
                    {
                      serial: serial,
                    }
                ))
              }



            }
            else
            {
              alert("Connection invalid. Please check your Wi-Fi settings. Connect to an Wi-Fi device prefixed with \"D2Bros-\". Then try again.");
            }

            this.setState({
              is_loading: false,
            })
          })
          .catch((e) => {
            alert(e.message);

            this.setState({
              is_loading: false,
            })
          })

    })
  }




  renderInner() {

    return (
        <Container>

          <Header>
            <Left>
              <Button transparent onPress={() => {
                this.props.navigation.pop();
              }}>
                <Icon name='arrow-back' style={{color: '#039BE5'}} color={`#039BE5`} />
              </Button>
            </Left>
            <Body>
            <Title>{`Offline Test`}</Title>
            </Body>
            <Right>
            </Right>
          </Header>

          <SafeAreaView style={{flex: 1}}>
            <View padder style={{flex: 1}}>

              <H1 style={{color: '#586C78', marginTop: 30, marginBottom: 15, textAlign: `center`}}>{`Find your D2Bros`.i18n()}</H1>

              <Text style={{color: '#586C78', padding: 5, textAlign: `left`}}>{`Please check your target D2Bros device you wish to activate by going to "Settings > Wifi" page

After configuration complete, wait for up to 3 seconds and press the button below.`.i18n()}</Text>

              <View style={{height: 30}}/>

              <Image
                  source={GLOBAL.IMAGE.wifi_connect_tutorial_d2bros}
                  style={{flex: 1, resizeMode: `contain`, alignSelf: `center`}}
              />

              <View style={{height: 30}}/>

              <Button
                  disabled={this.state.is_loading ? true : false}
                  onPress={() => {
                    this.tryProbe();
                  }}
                  style={{opacity: 1, backgroundColor: '#4fadf9', justifyContent: `center`}}>
                {
                  this.state.is_loading ? (
                      <ActivityIndicator animating color={"white"} size={'large'}/>
                  ) : (
                      <H3 style={{color: '#fff'}}>Confirm</H3>
                  )
                }
              </Button>

              <View style={{height: 20}}/>
              {
                false ? (
                    <Button
                        disabled={this.state.is_loading ? true : false}
                        onPress={() => {
                          this.tryProbe(true);
                        }}
                        style={{opacity: 1, backgroundColor: '#4fadf9', justifyContent: `center`}}>
                      {
                        this.state.is_loading ? (
                            <ActivityIndicator animating color={"white"} size={'large'}/>
                        ) : (
                            <H3 style={{color: '#fff'}}>Confirm (Test Raw Data)</H3>
                        )
                      }
                    </Button>
                ) : null
              }

            </View>
          </SafeAreaView>

          <Prompt
              title={`Enter D2Bros device passcode`.i18n()}
              placeholder={`D2Bros device passcode`.i18n()}
              defaultValue={this.state.passcode}
              visible={ this.state.promptVisible }
              cancelText={`Cancel`.i18n()}
              submitText={`Confirm and submit`.i18n()}
              onCancel={ () => {
                this.setState({
                  promptVisible: false,
                });
              }}
              onSubmit={ (value) => this.setState({
                promptVisible: false,
                passcode: value,
              }, () => {
                this.tryProbe();
              }) }/>

        </Container>
    );
  }
}
