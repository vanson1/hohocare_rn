import React, { Component } from 'react';
import {Image, SafeAreaView} from "react-native";
import {
  Container,
  Header,
  Content,
  Icon,
  List,
  View,
  ListItem,
  Text,
  Title,
  StyleProvider,
  getTheme,
} from 'native-base';
import APISourcerPrelogin from "../../apis/APISourcerPrelogin";
import BasePage from '../base/BasePage';
import customVariables from '../../themes/variable';

export default class AppDrawerSideBar extends BasePage {
  renderInner() {

    var menuitems = [
      {icon: 'home', name: 'Home', onPress: () => { setTimeout(() => { this.props.navigation.navigate("HomeDashboardPageV2"); }, 0);  } },
      {icon: 'add', name: 'Add device', onPress: () => { setTimeout(() => { this.props.navigation.navigate("ActivationBeforeStartPage"); }, 0);  } },
      {image: GLOBAL.IMAGE.icon_setting, icon: '', name: 'General settings', onPress: () => { setTimeout(() => { this.props.navigation.navigate("GeneralSettingsPage"); }, 0);  } },
      {image: GLOBAL.IMAGE.icon_usermanual, icon: '', name: 'User Manual', onPress: () => { setTimeout(() => { this.props.navigation.navigate("UserManualPage"); }, 0);  } },
      {image: GLOBAL.IMAGE.icon_FAQ, icon: '', name: 'FAQ', onPress: () => { setTimeout(() => { this.props.navigation.navigate("FAQPage"); }, 0);  } },
      {image: GLOBAL.IMAGE.icon_TAC, icon: '', name: 'Terms of Use', onPress: () => { setTimeout(() => { this.props.navigation.navigate("TACPage"); }, 0);  } },
      {image: GLOBAL.IMAGE.icon_logout, icon: '', name: 'Logout', onPress: () => {
        this.showLoading();
        new APISourcerPrelogin().logout()
            .then(() => {
              this.hideLoading();
              GLOBAL.NAV_GOTO_BY_SESSION();
            })
            .catch((e) => {
              alert(e.message);
              this.hideLoading();
            })
        } },
    ]

    return (
          <Container>
            <SafeAreaView style={{flex: 1}}>
              <Content>

                <View style={{height: 180, justifyContent: `center`}}>
                  <View style={{flexDirection: `row`, justifyContent: `center`}}>
                    <Image source={GLOBAL.IMAGE.logo_hohocare} style={{height: 150, width: 100, alignItems: `center`, resizeMode: `contain`}}/>
                  </View>
                  <Text style={{textAlign: `center`, fontSize: 11}} note>{`${`Version`.i18n()}: ${GLOBAL.VERSION}`}</Text>
                </View>

                <List>
                  {
                    menuitems.map((it) => {
                      return (
                          <ListItem button onPress={() => {
                            it.onPress()
                          }}>
                            <View style={{width: 32, height: 32, alignItems: 'center'}}>
                              {
                                it.icon ? (
                                    <Icon name={it.icon} fontSize={15}/>
                                ) : null
                              }
                              {
                                it.image ? (
                                    <Image source={it.image} style={{resizeMode: `contain`, width: 30, height: 30}}/>
                                ) : null
                              }
                            </View>
                            <View style={{width: 32}}>
                            </View>
                            <Text style={{fontSize: 16}}>{it.name && it.name.i18n()}</Text>
                          </ListItem>
                      )
                    })
                  }
                </List>
              </Content>
            </SafeAreaView>
          </Container>
    );
  }
}
