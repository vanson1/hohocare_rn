import React, { Component } from 'react';
import { withNavigationFocus } from "react-navigation";

import { ControlAirConditionerPage } from '../../postlogin/controls/ControlAirConditionerPage'
import APISourcerData from "../../../apis/APISourcerData";
import {Body, Button, Header, Icon, Left, Right, Title} from "native-base";
import moment from '../../postlogin/controls/ControlAirConditionerPage';

class OfflineTestControlAirConditionerPage extends ControlAirConditionerPage {
  constructor(props)
  {
    super(props);
    this.device = {
      serial: this.props.navigation.state.params.serial,
      alias: this.props.navigation.state.params.serial,
      payload: {
        p: 0,
        m: 0,
        f: 1,
        t: 25,
        sv: 0,
        sh: 0,
        ct: 1212,
        ot: 1313,
        otio: 0,
      }
    }
  }

  componentWillAppear() {
    this.refresh();
  }

  componentWillDisappear() {
  }


  isDeviceOffline()
  {
    return false;
  }


  componentDidMount(): void {
    this.refresh();
    this.interval = setInterval(() => {
      new APISourcerData().status()
          .then((res) => {
            this.device.payload = res;
            this.refresh();
          })
          .catch((e) => {

          })
    }, 3000);

    setTimeout(() => {
      this.setState({
        is_ready: true,
      })
    }, 100);
  }

  getDeviceItem()
  {
    return this.device;
  }



  isDeviceOffline()
  {
    return false;
  }


  isDeviceVacationMode()
  {
    return false;
  }




  publishCommand()
  {
    new APISourcerData().sendjson(this.device.payload)
        .then((res) => {
        })
        .catch((e) => {
        })
    // new APISourcerData().sendraw(this.device.serial, null);
    //     .then((res) => {
    //     })
    //     .catch((e) => {
    //     })
  }



  __renderHeader()
  {
    var device = this.getDeviceItem();

    return (

        <Header>
          <Left>
            <Button transparent onPress={() => {
              this.props.navigation.pop();
            }}>
              <Icon name='arrow-back' style={{color: '#039BE5'}} color={`#039BE5`} />
            </Button>
          </Left>
          <Body>
          <Title>{device.alias}</Title>
          </Body>
          <Right>
          </Right>
        </Header>
    )
  }

}

export default withNavigationFocus(OfflineTestControlAirConditionerPage);

