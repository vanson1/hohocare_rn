import React, { Component } from 'react';
import { StackActions, NavigationActions } from 'react-navigation';
import { Alert, ScrollView, Image } from "react-native";
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3, Form, Item, Label, Input,
} from "native-base";
import BasePage from "../../base/BasePage";
import WifiManager from "react-native-wifi-reborn";
import APISourcerMqtt from "../../../apis/APISourcerMqtt";
import BaseNavigationBar from '../../base/BaseNavigationBar';

export default class ConfigureSettingsDeactivatePage extends BasePage {

  constructor(props)
  {
    super(props);
    this.state = {
    }
  }

  componentDidMount(): void {
    this.didDeactivate = false;
    var serial = this.props.navigation.state.params.item.serial;
    GLOBAL.LISTEN_DEVICE_ACK(serial, this.onAck.bind(this))
  }

  componentWillUnmount(): void {
    this.didDeactivate = false;
    var serial = this.props.navigation.state.params.item.serial;
    GLOBAL.UNLISTEN_DEVICE_ACK(serial, this.onAck.bind(this))
  }

  onAck()
  {
    if (this.didDeactivate)
    {
      var json = Object.assign({}, GLOBAL.EMIT_DEVICE_ACK_JSON);
      if (json.hash === 'deactivate')
      {
        this.hideLoading();
        clearTimeout(this.mqttTimer);
        this.removeRecord();
      }
    }
  }

  removeRecord()
  {
    var serial = this.props.navigation.state.params.item.serial;
    GLOBAL.STORE.remove_device(serial);
    GLOBAL.STORE.remove_schedules(serial);
    this.props.navigation.dispatch(StackActions.popToTop());
    this.props.navigation.navigate("HomeDashboardPage")
  }

  renderInner() {

    return (
        <Container>

          <BaseNavigationBar {...this.props}
              isBack
              title={`Configure`.i18n()}
          />


          <Content padder>

            <H1 style={{color: '#586C78', marginTop: 60, marginBottom: 30, textAlign: `center`}}>{`Deactivate D2Bros`.i18n()}</H1>

            <Text style={{color: '#586C78', padding: 5, textAlign: `left`}}>{`This will remove any linkage, settings and all schedules from your account. This action is irreversible. Are you sure to proceed?`.i18n()}</Text>

            <View style={{height: 50}}/>

            <Button danger
                onPress={() => {
                  Alert.alert("D2Bros".i18n(), "Are you sure you wish to remove this device from your app?".i18n(), [
                    {text: 'No'.i18n(), onPress: () => {}},
                    {text: 'Yes'.i18n(), onPress: () => {

                        this.showLoading();
                        this.didDeactivate = true;
                        this.mqttTimer = setTimeout(() => {

                          this.hideLoading();
                          this.didDeactivate = false;
                          Alert.alert("D2Bros".i18n(), "Could not connect to device or device/app is offline. Device may be unable to reset properly. Do you still wish to remove this device from your app?".i18n(), [
                            {text: 'No'.i18n(), onPress: () => {}},
                            {text: 'Yes'.i18n(), onPress: () => {
                                this.removeRecord()
                              }},
                          ])

                        }, 4500);

                        var serial = this.props.navigation.state.params.item.serial;
                        APISourcerMqtt.getInstance().publishCommandAC(serial, 'deactivate');

                      }},
                  ])
                }}
                style={{justifyContent: `center`}}>
              <H3 style={{color: '#fff'}}>{`Yes, please proceed`.i18n()}</H3>
            </Button>


          </Content>

        </Container>
    );
  }
}
