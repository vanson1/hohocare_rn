import React, { Component } from 'react';
import { ScrollView, Image } from "react-native";
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3,
} from "native-base";
import BaseWebViewPage from "../../../base/BaseWebViewPage";

export default class UserManualPage extends BaseWebViewPage {

  getTitle(): string {
    return `User Manual`.i18n();
  }
  getUrl(): string {
    return GLOBAL.url_usermanual;
  }
}
