import React, { Component } from 'react';
import { ScrollView, Image } from "react-native";
import { Container, Header, Left, Right, Body, Title, Accordion, View, Button, Icon, Content, List, ListItem, Text, H1, H2, H3, Form, Item, Input, Label } from 'native-base';
import BasePage from "../../base/BasePage";
import BaseNavigationBar from '../../base/BaseNavigationBar';

export default class ActivationBeforeStartPage extends BasePage {

  renderInner() {
    return (
        <Container>

          <BaseNavigationBar {...this.props}
              isMenu
              title={`Add D2Bros`.i18n()}
          />

          <ScrollView>

            <Content padder>

              <H1 style={{color: '#586C78', marginTop: 60, marginBottom: 30, textAlign: `center`}}>{`Before you start`.i18n()}</H1>

              <Image
                  source={GLOBAL.IMAGE.device_2_o}
                  style={{resizeMode: `contain`, alignSelf: `center`, height: 250}}/>

              <Text style={{color: '#586C78', padding: 5, textAlign: `left`}}>{`1. Make sure your D2Bros device is powered on and its LED is flashing blue light (i.e. blinking on and off at 1 second interval)

2. If it does not perform the LED light flashing, wait for up to 1 minute.

3. If it still does not perform the LED light flashing, press and hold the “Reset” button for 6 seconds.`.i18n()}</Text>

              <View style={{height: 50}}/>

              <Button
                  onPress={() => {
                    this.props.navigation.navigate("ActivationConfigureWiFiConnectionPage");
                  }}
                  style={{backgroundColor: '#4fadf9', justifyContent: `center`}}>
                <H3 style={{color: '#fff'}}>{`Ready, proceed to next step`.i18n()}</H3>
              </Button>

            </Content>

          </ScrollView>

        </Container>
    );
  }
}
