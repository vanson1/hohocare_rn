import React, { Component } from 'react';
import { ScrollView, TouchableOpacity } from "react-native";
import WifiManager from "react-native-wifi-reborn";
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3,
  Form,
  Item,
  Input,
  Label,
  CheckBox,
} from 'native-base';
import BasePage from "../../base/BasePage";
import APISourcerPrelogin from "../../../apis/APISourcerPrelogin";
import BaseNavigationBar from '../../base/BaseNavigationBar';

export default class ActivationBeforeStartPage extends BasePage {

  constructor(props)
  {
    super(props);
    this.state = {
      username: '',
      password: '',
      password_confirm: '',
    }
  }

  renderInner() {

    var isDisabled =
        !this.state.isTc
        || !this.state.username.trim()
        || !this.state.password.trim()
        || this.state.password.length < 8
        || !this.state.password_confirm.trim()
        || this.state.password_confirm.length < 8;

    return (
        <Container>

          <BaseNavigationBar {...this.props}
                             isBack
                             title={`Register`.i18n()}
          />

          <Content padder>

            <H1 style={{color: '#586C78', marginTop: 60, marginBottom: 30, textAlign: `center`}}>{`Register new account`.i18n()}</H1>

            <Text style={{color: '#586C78', padding: 5, textAlign: `left`}}>{`In order to use our D2Bros device service, please register your account below. Enter your email address and password below:`.i18n()}</Text>


            <Form>
              <Item stackedLabel>
                <Label>{`Email`.i18n()}</Label>
                <Input value={this.state.username} autoCapitalize={false} autoCompleteType={'off'} onChangeText={(val) => {this.setState({username: val})}} />
              </Item>
              <Item stackedLabel>
                <Label>{`Password`.i18n()} ({`min length: 8 characters`.i18n()})</Label>
                <Input value={this.state.password} autoCapitalize={false} autoCompleteType={'off'} secureTextEntry onChangeText={(val) => {this.setState({password: val})}} />
              </Item>
              <Item stackedLabel>
                <Label>{`Confirm password`.i18n()}</Label>
                <Input value={this.state.password_confirm} autoCapitalize={false} autoCompleteType={'off'} secureTextEntry onChangeText={(val) => {this.setState({password_confirm: val})}} />
              </Item>
            </Form>


            <View style={{height: 10}}/>

            <ListItem noBorder>
              <CheckBox checked={this.state.isTc} onPress={() => {
                this.setState({isTc: !this.state.isTc})
              }} />
              <Body>
                <Text
                    onPress={() => {
                      this.setState({isTc: !this.state.isTc})
                    }}
                >{`I hereby confirm to accept the `.i18n()}<Text
                    style={{color: '#61b1f6'}}
                    onPress={() => {
                      this.props.navigation.navigate("TACPage");
                    }}
                >{`Terms and Conditions`.i18n()}</Text></Text>
              </Body>
            </ListItem>


            <View style={{height: 50}}/>

            <Button
                disabled={isDisabled}
                onPress={() => {

                  if (this.state.password !== this.state.password_confirm)
                  {
                    alert(`Password must match confirm password field input.`.i18n());
                    return;
                  }

                  this.showLoading();
                  new APISourcerPrelogin().register(
                      this.state.username,
                      this.state.password
                  )
                      .then((res) => {
                        this.hideLoading();
                        this.props.navigation.navigate("RegisterFormVerificationPage", {
                          username: this.state.username,
                          password: this.state.password,
                        })
                      })
                      .catch((e) => {
                        this.hideLoading();
                        alert(e.message);
                      })
                }}
                style={{opacity: isDisabled ? 0.5 : 1, backgroundColor: '#4fadf9', justifyContent: `center`}}>
              <H3 style={{color: '#fff'}}>{`Confirm and submit`.i18n()}</H3>
            </Button>

          </Content>

        </Container>
    );
  }
}
