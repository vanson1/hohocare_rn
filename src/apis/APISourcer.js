export default class APISourcer
{
  getBaseAPIUrl()
  {
    return "http://ap2.www.vanportdev.com:10016/api/pg";
  }
  getBaseMQTTUrl()
  {
    return GLOBAL.MQTT_WS_URL || 'ws://ap2.www.vanportdev.com:1800/';
  }
  getExternalServerWeatherUrl()
  {
    return GLOBAL.EXT_SERVER_WEATHER_URL || 'http://157.245.198.40:8659';
  }
  getBaseAccessPointUrl()
  {
    return GLOBAL.BASE_ACCESS_POINT_URL || "http://192.168.4.1";
  }
}
