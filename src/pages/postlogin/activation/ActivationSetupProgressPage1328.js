import React, { Component } from 'react';
import { ScrollView, Image } from "react-native";
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3,
} from "native-base";
import BasePage from "../../base/BasePage";
import {StackActions} from "react-navigation";
import APISourcerActivate from '../../../apis/APISourcerActivate'
import BaseNavigationBar from '../../base/BaseNavigationBar';
import APISourcerMqtt from '../../../apis/APISourcerMqtt';
import APISourcerPreference from '../../../apis/APISourcerPreference';

export default class ActivationSetupProgressPage1328 extends BasePage {

  constructor(props)
  {
    super(props);
    this.state = {
      isConnected: null,
      isInternet: null,
      isComplete: null,
      loop_count: 0,
    }
  }

  componentDidMount(): void {
    setTimeout(() => {
      this.doActivate();
    }, 1000);
  }

  componentWillDisappear() {
    super.componentWillDisappear();
    if (this.getPingLoopTimer) { clearTimeout(this.getPingLoopTimer); this.getPingLoopTimer = null; }
    var {
      serial,
    } = this.props.navigation.state.params;
    GLOBAL.UNLISTEN_DEVICE_ACK(serial, this.onAck.bind(this))
  }

  componentWillUnmount(): void {
    if (this.getPingLoopTimer) { clearTimeout(this.getPingLoopTimer); this.getPingLoopTimer = null; }
    var {
      serial,
    } = this.props.navigation.state.params;
    GLOBAL.UNLISTEN_DEVICE_ACK(serial, this.onAck.bind(this))
  }


  doActivate()
  {
    var {
      serial,
      router,
      access_token_priv,
    } = this.props.navigation.state.params;

    var {
      ssid,
      password,
    } = router || {};

    var access_token = GLOBAL.GET_ACCESS_TOKEN(serial);
    if (access_token_priv)
    {
      access_token = access_token_priv;
    }

    new APISourcerActivate().activate_1328(
        access_token,
        ssid,
        password,
        8
    )
        .then((res) => {

          if (res.success)
          {
            this.setState({
              isConnected: true,
            })

            GLOBAL.STORE.add_device(serial, access_token)
                .then(() => {

                })
                .catch(() => {

                })

            setTimeout(() => {
              this.loopVerify();
            }, 1000);
          }
          else
          {
            this.deviceRequestFailedFallbackToGetPing(`E0011`);
          }

        })
        .catch((e) => {
          this.deviceRequestFailedFallbackToGetPing(`E0012`);
        })
  }


  loopVerify()
  {
    if (this.state.loop_count > 10)
    {
      alert("D2Bros applying home router Wi-Fi settings - timeout".i18n());

      this.setState({
        isInternet: false,
        isComplete: false,
      })

      return;
    }

    var {
      serial,
      router,
      access_token_priv,
    } = this.props.navigation.state.params;

    var {
      ssid,
      password,
    } = router || {};

    var access_token = GLOBAL.GET_ACCESS_TOKEN(serial);
    if (access_token_priv)
    {
      access_token = access_token_priv;
    }

    new APISourcerActivate().activate_verify_1328(
        access_token,
        ssid,
        password
    )
        .then((res) => {

          if (res.success && res.connected)
          {
            this.setState({
              isInternet: true,
            }, () => {

              setTimeout(() => {
                this.doActivateVerifyReset();
              }, 3000);

            })
          }
          else
          {
            setTimeout(() => {
              this.state.loop_count += 1;
              this.loopVerify();
            }, 3000)
          }

        })
        .catch((e) => {
          // alert(e.message);

          this.setState({
            isInternet: true,
          })

          setTimeout(() => {

            this.setState({
              isComplete: true,
            })

          }, 3000);
        })

  }



  doActivateVerifyReset()
  {
    var {
      serial,
      router,
      access_token_priv,
    } = this.props.navigation.state.params;

    var {
      ssid,
      password,
    } = router || {};

    var access_token = GLOBAL.GET_ACCESS_TOKEN(serial);
    if (access_token_priv)
    {
      access_token = access_token_priv;
    }


    new APISourcerActivate().activate_resetme_1328(
        access_token
    )
        .then((res) => {

          if (res.success)
          {
            this.setState({
              isComplete: true,
            })
          }
          else
          {
            this.deviceRequestFailedFallbackToGetPing(`E0011`);
          }

        })
        .catch((e) => {
          this.deviceRequestFailedFallbackToGetPing(`E0012`);
        })
  }





  deviceGetPing(count)
  {
    console.log("Count... " + count);

    var {
      serial,
    } = this.props.navigation.state.params;

    APISourcerMqtt.getInstance().publishCommandAC(serial, 'ping');
  }


  onAck()
  {
    //alert('onAck...' + JSON.stringify(GLOBAL.EMIT_DEVICE_ACK_JSON));
    var json = Object.assign({}, GLOBAL.EMIT_DEVICE_ACK_JSON);
    if (json.hash === 'ping')
    {
      if (this.getPingLoopTimer) { clearTimeout(this.getPingLoopTimer); this.getPingLoopTimer = null; }

      var {
        serial,
        access_token_priv,
      } = this.props.navigation.state.params;

      var access_token = GLOBAL.GET_ACCESS_TOKEN(serial);
      if (access_token_priv)
      {
        access_token = access_token_priv;
      }


      GLOBAL.STORE.add_device(serial, access_token)
          .then((res) => {
            this.setState({
              isInternet: true,
            })

            setTimeout(() => {

              this.setState({
                isComplete: true,
              })

            }, 3000);
          })
          .catch((e) => {
            this.setState({
              isInternet: true,
            })

            setTimeout(() => {

              this.setState({
                isComplete: true,
              })

            }, 3000);
          })
    }
  }




  deviceRequestFailedFallbackToGetPing(error_code)
  {
    if (error_code === `E0011` || error_code === `E0013`)
    {
      alert(`${error_code}: Network connection failed. Here are the possible cause(s): 

1. You are using the same mobile phone for setting up a Wi-Fi hotspot & connecting to the D2Bros device at the same time.

2. You have entered a wrong Wi-Fi SSID or password for your D2Bros device.

3. Your designated Wi-Fi network is not using 2.4G.

A 2.4G network supported by a home or portable Wi-Fi router is recommended for establishing a stable connection for your D2Bros device. Please try again. `.i18n());

      if (error_code === `E0011`)
      {
        this.setState({
          isConnected: false,
          isInternet: false,
          isComplete: false,
        })
      }

      else if (error_code === `E0013`)
      {
        this.setState({
          isConnected: true,
          isInternet: false,
          isComplete: false,
        })

        var {
          serial,
        } = this.props.navigation.state.params;
        var access_token = GLOBAL.GET_ACCESS_TOKEN(serial);

        GLOBAL.STORE.add_device(serial, access_token)
            .then((res) => {
              this.setState({
                isInternet: true,
              })

              setTimeout(() => {

                this.setState({
                  isComplete: true,
                })

              }, 3000);
            })
            .catch((e) => {
              this.setState({
                isInternet: true,
              })

              setTimeout(() => {

                this.setState({
                  isComplete: true,
                })

              }, 3000);
            })

      }
    }


    else if (error_code === `E0012`)
    {
      this.setState({
        isConnected: true,
      }, () => {

        var {
          serial,
          access_token_priv,
        } = this.props.navigation.state.params;

        var access_token = GLOBAL.GET_ACCESS_TOKEN(serial);
        if (access_token_priv)
        {
          access_token = access_token_priv;
        }

        GLOBAL.STORE.add_device(serial, access_token, {
          is_probing: true,
        })

        GLOBAL.LISTEN_DEVICE_ACK(serial, this.onAck.bind(this))

        if (this.getPingLoopTimer) { clearTimeout(this.getPingLoopTimer); this.getPingLoopTimer = null; }
        var count = 0;
        this.getPingLoopTimer = setInterval(() => {
          count += 1;
          if (count > 10)
          {
            this.deviceRequestFailedFallbackToGetPing(`E0013`);
            if (this.getPingLoopTimer) { clearTimeout(this.getPingLoopTimer); this.getPingLoopTimer = null; }
          }
          else
          {
            this.deviceGetPing(count);
          }
        }, 7500);

      })
    }
  }




  renderInner() {

    var isComplete =
        this.state.isConnected !== null
        && this.state.isInternet !== null
        && this.state.isComplete !== null;

    return (
        <Container>

          <BaseNavigationBar {...this.props}
              title={`Add D2Bros`.i18n()}
          />

          <ScrollView>

            <Content padder>

              <H1 style={{color: '#586C78', marginTop: 60, marginBottom: 30, textAlign: `center`}}>{`Activating your D2Bros`.i18n()}</H1>

              <Image
                  source={GLOBAL.IMAGE.device_1_o}
                  style={{resizeMode: `contain`, alignSelf: `center`, height: 200}}/>


              <List>
                <ListItem selected={this.state.isConnected !== null}>
                  <Left>
                    <Text>{`Phone connected to D2Bros device`.i18n()}</Text>
                  </Left>
                  <Right>
                    <Icon name={this.state.isConnected === false ? "close" : "checkmark"} />
                  </Right>
                </ListItem>
                <ListItem selected={this.state.isInternet !== null}>
                  <Left>
                    <Text>{`D2Bros applying home router Wi-Fi settings`.i18n()} {this.state.loop_count > 0 ?  `... (${this.state.loop_count})`   :  ``}</Text>
                  </Left>
                  <Right>
                    <Icon name={this.state.isInternet === false ? "close" : "checkmark"} />
                  </Right>
                </ListItem>
                <ListItem selected={this.state.isComplete !== null}>
                  <Left>
                    <Text>{`D2Bros setup complete`.i18n()}</Text>
                  </Left>
                  <Right>
                    <Icon name={this.state.isComplete === false ? "close" : "checkmark"} />
                  </Right>
                </ListItem>
              </List>

              <View style={{height: 50}}/>

              <Button
                  disabled={!isComplete}
                  info
                  onPress={() => {
                    this.props.navigation.dispatch(StackActions.popToTop());
                    this.props.navigation.navigate("HomeDashboardPage");
                  }}
                  style={{opacity: !isComplete ? 0.5 : 1, backgroundColor: '#4fadf9', justifyContent: `center`}}>
                <H3 style={{color: '#fff'}}>{`Completed, go back to home`.i18n()}</H3>
              </Button>

            </Content>

          </ScrollView>

        </Container>
    );
  }
}
