import React, { PureComponent } from 'react';
import { PanResponder, View, Text } from 'react-native';
import range from 'lodash.range';


function calculateArcCircle(index0, segments, radius, startAngle0 = 0, angleLength0 = 2 * Math.PI, strokeWidth = 0) {
  // Add 0.0001 to the possible angle so when start = stop angle, whole circle is drawn
  const startAngle = startAngle0 % (2 * Math.PI);
  const angleLength = angleLength0 % (2 * Math.PI);
  const index = index0 + 1;
  const fromAngle = angleLength / segments * (index - 1) + startAngle;
  const toAngle = angleLength / segments * index + startAngle;
  const fromX = radius * Math.sin(fromAngle);
  const fromY = -radius * Math.cos(fromAngle);
  const realToX = radius * Math.sin(toAngle);
  const realToY = -radius * Math.cos(toAngle);

  // add 0.005 to start drawing a little bit earlier so segments stick together
  const toX = radius * Math.sin(toAngle + 0.005);
  const toY = -radius * Math.cos(toAngle + 0.005);

  return {
    fromAngle,
    toAngle,
    fromX,
    fromY,
    toX,
    toY,
    realToX,
    realToY,
  };
}


export default class CircularSlider extends PureComponent {

  // static propTypes = {
  //   onUpdate: PropTypes.func.isRequired,
  //   startAngle: PropTypes.number.isRequired,
  //   angleLength: PropTypes.number.isRequired,
  //   segments: PropTypes.number,
  //   strokeWidth: PropTypes.number,
  //   radius: PropTypes.number,
  //   gradientColorFrom: PropTypes.string,
  //   gradientColorTo: PropTypes.string,
  //   showClockFace: PropTypes.bool,
  //   clockFaceColor: PropTypes.string,
  //   bgCircleColor: PropTypes.string,
  //   stopIcon: PropTypes.element,
  //   startIcon: PropTypes.element,
  // }

  static defaultProps = {
    segments: 5,
    strokeWidth: 40,
    radius: 145,
    gradientColorFrom: '#ff9800',
    gradientColorTo: '#ffcf00',
    clockFaceColor: '#9d9d9d',
    bgCircleColor: '#171717',
  }

  state = {
    circleCenterX: false,
    circleCenterY: false,
  }

  componentWillMount() {
    this._sleepPanResponder = PanResponder.create({
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => this.setCircleCenter(),
      onPanResponderMove: (evt, { moveX, moveY }) => {
        const { circleCenterX, circleCenterY } = this.state;
        const { angleLength, startAngle, onUpdate } = this.props;

        const currentAngleStop = (startAngle + angleLength) % (2 * Math.PI);
        let newAngle = Math.atan2(moveY - circleCenterY, moveX - circleCenterX) + Math.PI/2;

        if (newAngle < 0) {
          newAngle += 2 * Math.PI;
        }

        let newAngleLength = currentAngleStop - newAngle;

        if (newAngleLength < 0) {
          newAngleLength += 2 * Math.PI;
        }



        var al = (newAngleLength) / Math.PI / 2;
        // 0.3399432119664225
        // 0.6505125225358988

        var leftmargin = 0.33;
        var rightmargin = 0.66;
        if (al > leftmargin && al < rightmargin)
        {
          if (al > ((leftmargin + rightmargin) / 2)) {
            al = 0.66;
            newAngleLength = al * Math.PI * 2;
          }
          else if (al <= ((leftmargin + rightmargin) / 2)) {
            al = 0.33;
            newAngleLength = al * Math.PI * 2;
          }
        }

        if (al >= 0.66)
        {
          al -= 0.66;
        }
        else
        {
          al += (1 - 0.66);
        }

        al /= (1 - 0.33);
        console.log("SADSADASAD", al);


        onUpdate({ startAngle: newAngle, al: al, angleLength: newAngleLength % (2 * Math.PI) });
      },
    });

    this._wakePanResponder = PanResponder.create({
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => this.setCircleCenter(),
      onPanResponderMove: (evt, { moveX, moveY }) => {
        const { circleCenterX, circleCenterY } = this.state;
        const { angleLength, startAngle, onUpdate } = this.props;

        let newAngle = Math.atan2(moveY - circleCenterY, moveX - circleCenterX) + Math.PI/2;
        let newAngleLength = (newAngle - startAngle) % (2 * Math.PI);

        if (newAngleLength < 0) {
          newAngleLength += 2 * Math.PI;
        }



        var al = (newAngleLength) / Math.PI / 2;
        // 0.3399432119664225
        // 0.6505125225358988

        var leftmargin = 0.33;
        var rightmargin = 0.66;
        if (al > leftmargin && al < rightmargin)
        {
          if (al > ((leftmargin + rightmargin) / 2)) {
            al = 0.66;
            newAngleLength = al * Math.PI * 2;
          }
          else if (al <= ((leftmargin + rightmargin) / 2)) {
            al = 0.33;
            newAngleLength = al * Math.PI * 2;
          }
        }

        if (al >= 0.66)
        {
          al -= 0.66;
        }
        else
        {
          al += (1 - 0.66);
        }

        al /= (1 - 0.33);
        console.log("SADSADASAD", al);

        // var b = newAngleLength;
        // if (isNaN(b))
        // {
          // alert(al + " " + newAngleLength + " " + startAngle + " " + newAngle + " " + circleCenterX + " " + moveX);
        // }

        onUpdate({ startAngle, al: al, angleLength: newAngleLength });
      },
    });
  }

  onLayout = () => {
    this.setCircleCenter();
  }

  setCircleCenter = () => {
    this._circle.measure((x, y, w, h, px, py) => {
      const halfOfContainer = this.getContainerWidth() / 2;
      // alert(halfOfContainer + " " + px + " " + py + " "+ x + " " + y + " " + w + " " + h)
      if (px && py)
      {
        this.setState({ circleCenterX: px + halfOfContainer, circleCenterY: py + halfOfContainer });
      }
    });
  }


  getContainerWidth() {
    const { strokeWidth, radius } = this.props;
    // return strokeWidth + radius * 2 + 2;
    return radius * 2;
  }




  getParsedValue(al, min, max)
  {
    console.log(al, min, max);
    var val = min;
    var pc = (al / 1);
    val += ( (max - min) * (pc));
    return Math.round(val);
  }


  getAngleLengthByParsedValue(val, min, max)
  {
    var al = (val - min) / (max - min);
    var angleLength = 0;

    al *= (1 - 0.33);

    if (al <  (1 - 0.33) / 2 )
    {
      al += 0.66;
    }
    else
    {
      al -= (1 - 0.66);
    }

    angleLength = al * Math.PI * 2;
    return angleLength;
  }





  render() {
    var { angleLength } = this.props;
    const { startAngle, segments, strokeWidth, radius, gradientColorFrom, gradientColorTo, bgCircleColor,
      showClockFace, clockFaceColor, startIcon, stopIcon } = this.props;

    var al = (angleLength) / Math.PI / 2;
    // 0.3399432119664225
    // 0.6505125225358988

    var leftmargin = 0.33;
    var rightmargin = 0.66;
    if (al > leftmargin && al < rightmargin)
    {
      if (al > ((leftmargin + rightmargin) / 2)) {
        al = 0.66;
        angleLength = al * Math.PI * 2;
      }
      else if (al <= ((leftmargin + rightmargin) / 2)) {
        al = 0.33;
        angleLength = al * Math.PI * 2;
      }
    }

    if (al >= 0.66)
    {
      al -= 0.66;
    }
    else
    {
      al += (1 - 0.66);
    }

    al /= (1 - 0.33);

    // console.log("DDDDDDDD", startAngle, al, this.getAngleLengthByParsedValue(28, 14, 30));

    const containerWidth = this.getContainerWidth();


    const start = calculateArcCircle(0, segments, radius, startAngle, angleLength);
    const stop = calculateArcCircle(segments - 1, segments, radius, startAngle, angleLength, strokeWidth);

    let offsetX = 4 - ((stop.realToX + (radius)) / (radius * 2) ) * (strokeWidth + (18 * (this.props.iconRadius / 45) ) );
    let offsetY = 2 + strokeWidth - ((stop.realToY + (radius)) / (radius * 2) ) * (strokeWidth + (18 * (this.props.iconRadius / 45) ) );



    return (
        <View style={{ width: containerWidth, height: containerWidth }} onLayout={this.onLayout}>
          <View
              collapsable={false}
              renderToHardwareTextureAndroid={true}
              style={{
                position: `absolute`,
                width: containerWidth, height: containerWidth,
              }}
              ref={circle => this._circle = circle}
          >

            {/*
              ##### Stop Icon
            */}

            {
              showClockFace ? (
                  <View
                      onPressIn={() => this.setState({ angleLength: angleLength + Math.PI / 2 })}
                      {...this._wakePanResponder.panHandlers}
                      style={{backgroundColor: '#fff', borderRadius: this.props.iconRadius / 2, width: this.props.iconRadius, height: this.props.iconRadius, transform: [
                          {translateX: (stop.realToX + radius + offsetX) },
                          {translateY: (stop.realToY - strokeWidth) + radius + offsetY },
                        ]}}>
                  </View>
              ) : null
            }

            {
              /*
            <Text>{startAngle} {angleLength} {containerWidth} {strokeWidth} {radius}</Text>
               */
            }


          </View>
        </View>
    );
  }
}
