import React, { Component } from 'react';
import { Container, Header, Left, Right, Body, Title, Accordion, Button, Icon, Content, List, ListItem, Text } from 'native-base';
import { Image, TouchableOpacity, View, FlatList } from 'react-native';
import AccordionHeader from './components/AccordionHeader'
import AccordionItem from './components/AccordionItem'
import BasePage from "../../base/BasePage";
import { withNavigationFocus } from "react-navigation";
import APISourcerMqtt from '../../../apis/APISourcerMqtt';
import BaseNavigationBar from '../../base/BaseNavigationBar';
import APISourcerPrelogin from '../../../apis/APISourcerPrelogin';
import APISourcerFamily from '../../../apis/APISourcerFamily';


class HomeDashboardPageV2 extends BasePage {
  constructor(props)
  {
    super(props);
    this.state = {
      items: [],
      me: {},
    }

    this.refresh = this.refresh.bind(this);
  }

  componentDidMount(): void {
    this.refresh()
  }

  refresh()
  {
    this.showLoading()
    new APISourcerPrelogin().me()
        .then((res) => {
          alert(JSON.stringify(res))
          this.setState({
            me: res.data,
          })
          return new APISourcerFamily().HOHOCARE_Family_ListMyJoinedFamilys()
        })
        .then((res) => {
          this.hideLoading()
          this.setState({
            items: res.data.items || [],
            is_ready: true,
          })
        })
        .catch((e) => {
          this.hideLoading()
        })
  }


  componentWillUnmount(): void {

  }

  renderInner() {
    return (
        <Container>

          <BaseNavigationBar {...this.props}
                             isMenu
                             title={`Home`.i18n()}
                             icon_right={
                               <Right>
                                 <Button transparent onPress={() => {
                                   this.refresh()
                                 }}>
                                   <Image source={GLOBAL.IMAGE.ic_refresh} style={{width: 32, height: 32}}/>
                                 </Button>
                               </Right>
                             }
          />

          <Content>

            <View>
              <Text note style={{paddingHorizontal: 20, paddingVertical: 10}}>Welcome back, {this.state.me.nickname}</Text>
            </View>
            <View style={{flex: 1}}>
            {
              this.state.is_ready ? (

                  this.state.items.length > 0 ? (
                      <FlatList
                          data={this.state.items}
                          renderItem={({item}) => {
                            return (
                                <ListItem button onPress={() => {
                                  this.props.navigation.navigate("FamilyDetailsPage", {
                                    _id: item.family._id,
                                  });
                                }}>
                                  <View style={{width: 32, height: 32, alignItems: 'center'}}>
                                    <Icon name={`home`} fontSize={15}/>
                                  </View>
                                  <View style={{width: 32}}>
                                  </View>
                                  <Text style={{flex: 1, fontSize: 16}}>{item.family.desc}</Text>
                                  <View style={{width: 32}}>
                                  </View>
                                  <View style={{width: 16, height: 16, backgroundColor: '#55ff55', borderRadius: 8}}>
                                  </View>
                                  <View style={{width: 32}}>
                                  </View>
                                </ListItem>
                            )
                          }}
                      />
                  ) : (
                      <View style={{height: 400, flex: 1, alignItems: `center`, justifyContent: `center`}}>
                        <TouchableOpacity
                            onPress={() => {
                              this.props.navigation.navigate("ActivationBeforeStartPage");
                            }}
                            style={{flexDirection: `column`, flex: 1, alignItems: `center`, justifyContent: `center`}}>
                          <Image source={GLOBAL.IMAGE.d_ac_ic_new_device} style={{width: 200, height: 200}}/>
                          <Text style={{color: '#999', padding: 15}}>
                            {`Add your first HOHOCARE family by tapping here`.i18n()}
                          </Text>
                        </TouchableOpacity>
                      </View>
                  )


              ) : null
            }
            </View>

          </Content>

        </Container>
    );
  }
}


export default withNavigationFocus(HomeDashboardPageV2)
