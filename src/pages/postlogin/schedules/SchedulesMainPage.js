import React, { Component } from 'react';
import { Container, Header, Left, Right, Body, Title, Tabs, Tab, Accordion, Button, Icon, Content, List, ListItem, Text } from 'native-base';

import AccordionHeader from './components/AccordionHeader'
import AccordionItem from './components/AccordionItem'
import BasePage from "../../base/BasePage";
import { withNavigationFocus } from "react-navigation";
import Picker from 'react-native-picker';
import APISourcerMqtt from '../../../apis/APISourcerMqtt';
import {Image, TouchableOpacity, View} from 'react-native';
import BaseNavigationBar from '../../base/BaseNavigationBar';

const dataArray = [

      {title: "BedRoomTH(WWW)", temp_room: "28˚c", temp_ac: "25˚c", hum_room: "65%",

        items: [
          {disabled: false, seconds: 0 * 60 * 60, action: {power: true, temp: 25, mode: 'cool'} },
          {disabled: true, seconds: 9 * 60 * 60, action: {power: false} },
          {disabled: false, seconds: 19 * 60 * 60 + 30 * 60, action: {power: true, temp: 25, mode: 'cool'} },
        ],

        ssid: "D2Bros-XXXXXXXX", serial: "D2Bros-XXXXXXXX", host_model: "Daikin VAC Air conditioner series", alias: "BedRoomTH(WWW)", status: "Online"},
      {title: "KidsRoomTH(WWW)", temp_room: "28˚c", temp_ac: "25˚c", hum_room: "65%",

        items: [
          {disabled: false, seconds: 0 * 60 * 60, action: {power: true, temp: 25, mode: 'cool'} },
          {disabled: true, seconds: 9 * 60 * 60, action: {power: false} },
          {disabled: false, seconds: 19 * 60 * 60 + 30 * 60, action: {power: true, temp: 25, mode: 'cool'} },
        ],

        ssid: "D2Bros-XXXXXXXX", serial: "D2Bros-XXXXXXXX", host_model: "Daikin VAC Air conditioner series", alias: "BedRoomTH(WWW)", status: "Online"},
      {title: "LivingRoomTH(WWW)", temp_room: "28˚c", temp_ac: "25˚c", hum_room: "65%",

        items: [
          {disabled: false, seconds: 0 * 60 * 60, action: {power: true, temp: 25, mode: 'cool'} },
          {disabled: true, seconds: 9 * 60 * 60, action: {power: false} },
          {disabled: false, seconds: 19 * 60 * 60 + 30 * 60, action: {power: true, temp: 25, mode: 'cool'} },
        ],

        ssid: "D2Bros-XXXXXXXX", serial: "D2Bros-XXXXXXXX", host_model: "Daikin VAC Air conditioner series", alias: "BedRoomTH(WWW)", status: "Online"},
];

class SchedulesMainPage extends BasePage {

  constructor(props)
  {
    super(props);
    this.refresh = this.refresh.bind(this);
    this.state = {
      items: GLOBAL.STORE.devices || [],
    }
  }

  componentDidMount(): void {
    this.refresh();
    GLOBAL.LISTEN_LAST_UPDATE_UPDATES(this.refresh);
  }

  componentWillAppear() {
    super.componentWillAppear();
    this.refresh();
    GLOBAL.LISTEN_LAST_UPDATE_UPDATES(this.refresh);
  }


  componentWillDisappear() {
    super.componentWillDisappear();
    GLOBAL.UNLISTEN_LAST_UPDATE_UPDATES(this.refresh);
  }

  componentWillUnmount(): void {

  }

  refresh()
  {
    this.state.items = GLOBAL.STORE.devices;
    this.forceUpdate();
  }


  doApplySchedule()
  {
    GLOBAL.STORE.apply_schedule();
  }

  renderInner() {
    return (
        <Container>

          <BaseNavigationBar {...this.props}
              isMenu
              title={`Schedules`.i18n()}
          />


          {
            this.state.items.length > 0 ? (
                <Tabs initialPage={new Date().getDay()}>
                  <Tab
                      textStyle={{color: '#6b6b6b'}}
                      activeTextStyle={{color: '#009bec'}}
                      heading={"Su".i18n()}>
                    <WeekdayPage doApplySchedule={() => {
                      this.doApplySchedule();
                    }} navigation={this.props.navigation} dataArray={this.state.items} weekday={'Su'} />
                  </Tab>
                  <Tab
                      textStyle={{color: '#6b6b6b'}}
                      activeTextStyle={{color: '#009bec'}}
                      heading={"Mo".i18n()}>
                    <WeekdayPage doApplySchedule={() => {
                      this.doApplySchedule();
                    }} navigation={this.props.navigation} dataArray={this.state.items} weekday={'Mo'} />
                  </Tab>
                  <Tab
                      textStyle={{color: '#6b6b6b'}}
                      activeTextStyle={{color: '#009bec'}}
                      heading={"Tu".i18n()}>
                    <WeekdayPage doApplySchedule={() => {
                      this.doApplySchedule();
                    }} navigation={this.props.navigation} dataArray={this.state.items} weekday={'Tu'} />
                  </Tab>
                  <Tab
                      textStyle={{color: '#6b6b6b'}}
                      activeTextStyle={{color: '#009bec'}}
                      heading={"We".i18n()}>
                    <WeekdayPage doApplySchedule={() => {
                      this.doApplySchedule();
                    }} navigation={this.props.navigation} dataArray={this.state.items} weekday={'We'} />
                  </Tab>
                  <Tab
                      textStyle={{color: '#6b6b6b'}}
                      activeTextStyle={{color: '#009bec'}}
                      heading={"Th".i18n()}>
                    <WeekdayPage doApplySchedule={() => {
                      this.doApplySchedule();
                    }} navigation={this.props.navigation} dataArray={this.state.items} weekday={'Th'} />
                  </Tab>
                  <Tab
                      textStyle={{color: '#6b6b6b'}}
                      activeTextStyle={{color: '#009bec'}}
                      heading={"Fr".i18n()}>
                    <WeekdayPage doApplySchedule={() => {
                      this.doApplySchedule();
                    }} navigation={this.props.navigation} dataArray={this.state.items} weekday={'Fr'} />
                  </Tab>
                  <Tab
                      textStyle={{color: '#6b6b6b'}}
                      activeTextStyle={{color: '#009bec'}}
                      heading={"Sa".i18n()}>
                    <WeekdayPage doApplySchedule={() => {
                      this.doApplySchedule();
                    }} navigation={this.props.navigation} dataArray={this.state.items} weekday={'Sa'} />
                  </Tab>
                </Tabs>

            ) : (

                <View style={{height: 400, flex: 1, alignItems: `center`, justifyContent: `center`}}>
                  <TouchableOpacity
                      onPress={() => {
                        this.props.navigation.navigate("ActivationBeforeStartPage");
                      }}
                      style={{flexDirection: `column`, flex: 1, alignItems: `center`, justifyContent: `center`}}>
                    <Image source={GLOBAL.IMAGE.d_ac_ic_new_device} style={{width: 200, height: 200}}/>
                    <Text style={{color: '#999', padding: 15}}>
                      {`Add your first D2Bros device by tapping here`.i18n()}
                    </Text>
                  </TouchableOpacity>
                </View>
            )
          }

        </Container>
    );
  }
}


class WeekdayPage extends React.Component
{
  constructor(props)
  {
    super(props);
    this.state = {
      expanded_index: 0,
    }
  }

  _renderHeader(item, expanded)
  {
    return (
        <AccordionHeader item={item} expanded={expanded}
                         onPressAddItem={() => {
                           // GLOBAL.STORE.add_schedule(item.serial, this.props.weekday, 19 * 60 * 60);
                           // this.forceUpdate();

                           this.props.navigation.navigate("SchedulesSettingsPage", {
                             serial: item.serial,
                             weekday: this.props.weekday,
                             onConfirmItem: ((it) => {
                               GLOBAL.STORE.add_schedule(it);
                               this.props.doApplySchedule();
                             })
                           });
                         }}
        />
    )
  }

  _renderContent(item)
  {
    var items = GLOBAL.STORE.schedules;
    var __items = [];
    for (var k in items)
    {
      if (items[k].serial === item.serial
      && items[k].weekday === this.props.weekday)
      {
        items[k].index = k;
        __items.push(items[k]);
      }
    }
    return (
        <AccordionItem doApplySchedule={() => {
          this.props.doApplySchedule();
        }} navigation={this.props.navigation} item={item} items={__items}/>
    )
  }

  render() {
    var dataArray = this.props.dataArray || [];
    return (
        <Content>

          <Text note style={{padding: 5, textAlign: `center`}}>— {`Last Updated at`.i18n()} {GLOBAL.LAST_UPDATED_TIME} —</Text>

          {
            dataArray.length > 0 ? (
                <Accordion
                    ref={(v) => {this._accordion = v;}}
                    dataArray={dataArray}
                    animation={true}
                    expanded={this.state.expanded_index}
                    renderHeader={this._renderHeader.bind(this)}
                    renderContent={this._renderContent.bind(this)}
                />
            ) : null
          }

        </Content>
    );
  }
}


export default withNavigationFocus(SchedulesMainPage);

