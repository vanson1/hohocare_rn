import APISourcer from './APISourcer';
import {
  AsyncStorage
} from 'react-native'

export default class APISourcerFamily extends APISourcer
{
  GENFETCH(url, params)
  {
    return fetch(url , {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'x-access-token': GLOBAL.STORE.session.accessToken,
      },
      body: JSON.stringify(params),
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {
      if (json.success)
      {
        return json;
      }
      else
      {
        throw Error(json.message);
      }
    }).catch((error) => {
      throw error;
    });
  }

  HOHOCARE_Family_FindByCode(params)
  {
    var {
      family_id,
      code,
    } = (params || {});

    return this.GENFETCH(`${this.getBaseAPIUrl()}/HOHOCARE_Family_FindByCode`, params);
  }

  HOHOCARE_Family_Create(params)
  {
    var {
      desc,
    } = (params || {});

    return this.GENFETCH(`${this.getBaseAPIUrl()}/HOHOCARE_Family_Upsert`, params);
  }

  HOHOCARE_Family_Join(params)
  {
    var {
      family_id,
      code,
    } = (params || {});

    return this.GENFETCH(`${this.getBaseAPIUrl()}/HOHOCARE_Family_Upsert`, params);
  }

  HOHOCARE_Family_ListMyJoinedFamilys(params)
  {
    var {
    } = (params || {});

    return this.GENFETCH(`${this.getBaseAPIUrl()}/HOHOCARE_Family_ListMyJoinedFamilys`, params);
  }

  HOHOCARE_Family_ListMyAllFamilys(params)
  {
    var {
    } = (params || {});

    return this.GENFETCH(`${this.getBaseAPIUrl()}/HOHOCARE_Family_ListMyAllFamilys`, params);
  }

  HOHOCARE_Family_ListMyPendingFamilys(params)
  {
    var {
    } = (params || {});

    return this.GENFETCH(`${this.getBaseAPIUrl()}/HOHOCARE_Family_ListMyPendingFamilys`, params);
  }




  ADMIN_HOHOCARE_Family_ListFamilyUsers(params)
  {
    var {
      _id,
      role,
    } = (params || {});

    return this.GENFETCH(`${this.getBaseAPIUrl()}/HOHOCARE_Family_ListFamilyUsers`, params);
  }

  ADMIN_HOHOCARE_Family_ListFamilyPendingActions(params)
  {
    var {
      _id,
    } = (params || {});

    return this.GENFETCH(`${this.getBaseAPIUrl()}/HOHOCARE_Family_ListFamilyPendingActions`, params);
  }

  ADMIN_HOHOCARE_FamilyUser_React(params)
  {
    var {
      _id,
      action,
    } = (params || {});

    return this.GENFETCH(`${this.getBaseAPIUrl()}/HOHOCARE_FamilyUser_React`, params);
  }






  HOHOCARE_IOT_ListMyFamilyDevices(params)
  {
    var {
      family_id,
    } = (params || {});

    return this.GENFETCH(`${this.getBaseAPIUrl()}/HOHOCARE_IOT_ListMyFamilyDevices`, params);
  }

  HOHOCARE_IOT_ListMyGWSubdevices(params)
  {
    var {
      gw_device_id,
    } = (params || {});

    return this.GENFETCH(`${this.getBaseAPIUrl()}/HOHOCARE_IOT_ListMyGWSubdevices`, params);
  }


  HOHOCARE_IOT_ListMyFamilySubdevices(params)
  {
    var {
      family_id,
    } = (params || {});

    return this.GENFETCH(`${this.getBaseAPIUrl()}/HOHOCARE_IOT_ListMyFamilySubdevices`, params);
  }
}
