import React, { Component } from 'react';
import { Container, Header, Left, Right, Body, Thumbnail, H1, Title, Accordion, Button, Icon, Content, List, ListItem, Text } from 'native-base';
import { Image, Dimensions, TouchableOpacity, View, FlatList } from 'react-native';
import BasePage from "../../base/BasePage";
import { withNavigationFocus } from "react-navigation";
import BaseNavigationBar from '../../base/BaseNavigationBar';
import APISourcerFamily from '../../../apis/APISourcerFamily';
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';
import moment from 'moment'
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

class FamilyUserDetailsPage extends BasePage {
  constructor(props)
  {
    super(props);
    this.state = {
      family: null,
      family_users: [],
      family_pending_actions: [],
      is_active: 1,
    }
  }

  componentDidMount(): void {
    this.refresh()
  }

  refresh()
  {
    this.setState({
      is_ready: true,
      family: this.props.navigation.state.params.family,
      family_user: this.props.navigation.state.params.family_user,
      activities: this.props.navigation.state.params.activities,
    })
  }


  componentWillUnmount(): void {

  }

  renderInner() {
    return (
        <Container>

          <BaseNavigationBar {...this.props}
                             isBack
                             title={`Family User`.i18n()}
          />

          <Content>

            {
              this.state.is_ready && this.state.family_user ? (

                  <ListItem button style={{paddingBottom: 16}} onPress={() => {

                  }}>
                    <View style={{flexDirection: `column`, flex: 1}}>
                      <View style={{flexDirection: `row`}}>

                        <View style={{width: 32, height: 32, alignItems: 'center'}}>
                          <Thumbnail small source={{ uri: this.state.family_user.avatar_url }} />
                        </View>
                        <View style={{width: 32}}>
                        </View>
                        <Text style={{flex: 1, fontSize: 16}}>{this.state.family_user.nickname}</Text>
                        <View style={{width: 32}}>
                        </View>
                        <View style={{width: 16, height: 16, backgroundColor: '#55ff55', borderRadius: 8}}>
                        </View>

                      </View>
                      <View style={{height: 16}}>
                      </View>
                      <View style={{flexDirection: `row`}}>

                        <Text style={{flex: 1, fontSize: 16}}>{`位置`.i18n()}: { this.state.family_user.indoor ? `--`.i18n() : `--`.i18n() }</Text>
                        <View style={{width: 32}}>
                        </View>
                        <Text style={{flex: 1, fontSize: 16}}>{`房間`.i18n()}: { this.state.family_user.room }</Text>
                        <View style={{width: 32}}>
                        </View>


                      </View>

                    </View>
                  </ListItem>

              ) : null
            }

            <View style={{flexDirection: `row`}}>
              <Button
                  onPress={() => {
                    this.setState({
                      is_active: 1,
                    })
                  }}
                  info ={this.state.is_active === 1}
                  transparent={this.state.is_active !== 1}
                  style={{flex: 1, alignItems: `center`, flexDirection: `row`, justifyContent: `center`}}>
                <Text>最新通知</Text>
              </Button>

              <Button
                  onPress={() => {
                    this.setState({
                      is_active: 2,
                      is_sub_active: 1,
                    })
                  }}

                  info ={this.state.is_active === 2}
                  transparent={this.state.is_active !== 2}
                  style={{flex: 1.2, alignItems: `center`, flexDirection: `row`, justifyContent: `center`}}>
                <Text>保健記錄</Text>
              </Button>

              <Button
                  onPress={() => {
                    this.setState({
                      is_active: 3,
                    })
                  }}

                  info ={this.state.is_active === 3}
                  transparent={this.state.is_active !== 3}
                  style={{flex: 1.5, alignItems: `center`, flexDirection: `row`, justifyContent: `center`}}>
                <Text>外出地圖</Text>
              </Button>
            </View>




            {
              this.state.is_ready && this.state.activities && this.state.is_active === 1 ? (

                  <FlatList
                      style={{flex: 1}}
                      data={this.state.activities}
                      renderItem={({item}) => {
                        return (
                            <ListItem button onPress={() => {
                            }}>
                              <View style={{width: 32, height: 32, alignItems: 'center'}}>
                                <Thumbnail small source={{ uri: item.avatar_url }} />
                              </View>
                              <View style={{width: 32}}>
                              </View>
                              <Text style={{flex: 1, fontSize: 16}}>{item.desc}</Text>
                              <View style={{width: 32}}>
                              </View>
                              <View style={{width: 32}}>
                              </View>
                            </ListItem>
                        )
                      }}
                  />

              ) : null
            }



            {
              this.state.is_ready && this.state.family_user.healthrecords && this.state.is_active === 2 ? (

                  <View
                      style={{padding: 20, flex: 1}}>

                    <View style={{flexDirection: `row`}}>
                      <Button
                          onPress={() => {
                            this.setState({
                              is_sub_active: 1,
                            })
                          }}
                          info ={this.state.is_sub_active === 1}
                          transparent={this.state.is_sub_active !== 1}
                          style={{flex: 1, alignItems: `center`, flexDirection: `row`, justifyContent: `center`}}>
                        <Text>心率</Text>
                      </Button>

                      <Button
                          onPress={() => {
                            this.setState({
                              is_sub_active: 2,
                            })
                          }}

                          info ={this.state.is_sub_active === 2}
                          transparent={this.state.is_sub_active !== 2}
                          style={{flex: 1.2, alignItems: `center`, flexDirection: `row`, justifyContent: `center`}}>
                        <Text>血壓</Text>
                      </Button>

                    </View>


                    <View
                        style={{paddingTop: 20, flex: 1}}>
                    {
                      (this.state.is_sub_active === 1
                       && this.state.family_user.healthrecords.hrs.length > 0) ? (
                          <View style={{flex: 1}}>
                            <View style={{flexDirection: `row`}}>
                              <H1>{`心率`.i18n()}</H1>
                              <Text style={{flex: 1, textAlign: `right`}}>
                                {this.state.family_user.healthrecords.hrs[0].m1}
                              </Text>
                              <Text note>
                                {`次/分`}
                              </Text>
                            </View>



                            <FlatList
                                style={{flex: 1}}
                                data={this.state.family_user.healthrecords.hrs}
                                renderItem={({item}) => {
                                  return (
                                      <ListItem button onPress={() => {
                                      }}>
                                        <Text note>{moment(item.created_at).format('YYYY-MM-DD HH:mm')}</Text>
                                        <View style={{width: 32}}>
                                        </View>
                                        <Text style={{flex: 1, fontSize: 16, textAlign: `right`}}>{item.m1}</Text>
                                        <View style={{width: 32}}>
                                        </View>
                                      </ListItem>
                                  )
                                }}
                                />

                          </View>
                      ) : null
                    }
                    {
                      (this.state.is_sub_active === 2
                       && this.state.family_user.healthrecords.bps.length > 0) ? (
                          <View style={{flex: 1}}>
                            <View style={{flexDirection: `row`}}>
                              <H1>{`血壓`.i18n()}</H1>
                              <Text style={{flex: 1, textAlign: `right`}}>
                                {this.state.family_user.healthrecords.bps[0].m1}  {this.state.family_user.healthrecords.bps[0].m2}
                              </Text>
                              <Text note>
                                {`最高/最低`}
                              </Text>
                            </View>



                            <FlatList
                                style={{flex: 1}}
                                data={this.state.family_user.healthrecords.bps}
                                renderItem={({item}) => {
                                  return (
                                      <ListItem button onPress={() => {
                                      }}>
                                        <Text note>{moment(item.created_at).format('YYYY-MM-DD HH:mm')}</Text>
                                        <View style={{width: 32}}>
                                        </View>
                                        <Text style={{flex: 1, fontSize: 16, textAlign: `right`}}>{item.m1}  {item.m2}</Text>
                                        <View style={{width: 32}}>
                                        </View>
                                      </ListItem>
                                  )
                                }}
                            />

                          </View>
                      ) : null
                    }
                    </View>

                  </View>

              ) : null
            }



            {
              this.state.is_ready && this.state.family_user.lnglat && this.state.is_active === 3 ? (
                  <MapView
                      style={{flex: 1, width: width, height: 250}}
                      initialRegion={{
                        latitude: this.state.family_user.lnglat[1],
                        longitude: this.state.family_user.lnglat[0],
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                      }}
                      onRegionChange={() => {

                      }}
                  >
                    <Marker
                        coordinate={{
                          latitude: this.state.family_user.lnglat[1],
                          longitude: this.state.family_user.lnglat[0],
                        }}
                        title={this.state.family_user.nickname}
                        description={moment().format('YYYY-MM-DD HH:mm:ss')}
                    />
                  </MapView>
              ) : null
            }


          </Content>

        </Container>
    );
  }
}







export default withNavigationFocus(FamilyUserDetailsPage)
