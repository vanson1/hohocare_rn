import React, { Component } from 'react';
import { ScrollView } from "react-native";
import WifiManager from "react-native-wifi-reborn";
import { Container, Header, Left, Right, Body, Title, Accordion, View, Button, Icon, Content, List, ListItem, Text, H1, H2, H3, Form, Item, Input, Label } from 'native-base';
import BasePage from "../../base/BasePage";
import BaseNavigationBar from '../../base/BaseNavigationBar';

export default class ActivationBeforeStartPage extends BasePage {

  constructor(props)
  {
    super(props);
    this.state = {
      username: '',
    }
  }

  renderInner() {

    var isDisabled =
        !this.state.username.trim();

    return (
        <Container>

          <BaseNavigationBar {...this.props}
                             isBack
                             title={`Reset password`.i18n()}
          />

          <Content padder>

            <H1 style={{color: '#586C78', marginTop: 60, marginBottom: 30, textAlign: `center`}}>{`Submit forgot password request`.i18n()}</H1>

            <Text style={{color: '#586C78', padding: 5, textAlign: `left`}}>{`This wizard will guide you through a few steps in order to reset your account password.

If you somehow forgot your account login password, we can help you reset your login password by sending an activation email to your email address. Please enter your account email address below:`.i18n()}</Text>


            <Form>
              <Item>
                <Label>{`Email`.i18n()}</Label>
                <Input value={this.state.username} autoCapitalize={false} autoCompleteType={'off'} onChangeText={(val) => {this.setState({username: val})}} />
              </Item>
            </Form>


            <View style={{height: 50}}/>

            <Button
                disabled={isDisabled}
                onPress={() => {
                  this.props.navigation.navigate("ForgotPasswordFormVerificationPage", {
                    username: this.state.username,
                  })
                }}
                style={{opacity: isDisabled ? 0.5 : 1, backgroundColor: '#4fadf9', justifyContent: `center`}}>
              <H3 style={{color: '#fff'}}>{`Confirm and submit`.i18n()}</H3>
            </Button>

          </Content>

        </Container>
    );
  }
}
