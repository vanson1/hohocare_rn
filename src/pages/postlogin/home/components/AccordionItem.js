import React, { Component } from 'react';
import { TouchableOpacity, Image } from 'react-native';
import {
  Container,
  Header,
  View,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H3
} from "native-base";
import APISourcerMqtt from "../../../../apis/APISourcerMqtt";
import moment from 'moment'
import APISourcerPrelogin from '../../../../apis/APISourcerPrelogin';

class AccordionItemSingle extends Component
{
  constructor(props)
  {
    super(props);
    this.state = {

    }

    this.refresh = this.refresh.bind(this);
  }


  componentDidMount(): void {
    GLOBAL.LISTEN_DEVICE_UPDATES(this.props.it.serial, this.refresh);
    if (this.loopForceUpdate) clearInterval(this.loopForceUpdate);
    this.loopForceUpdate = setInterval(() => {
      this.refresh();
    }, 60000);
  }

  componentWillUnmount(): void {
    GLOBAL.UNLISTEN_DEVICE_UPDATES(this.props.it.serial, this.refresh);
    if (this.loopForceUpdate) clearInterval(this.loopForceUpdate);
    this.sleep()
  }


  sleep()
  {
    if (this.cooldown) { clearTimeout(this.cooldown); this.cooldown = null; }
    if (this.cooldown2) { clearTimeout(this.cooldown2); this.cooldown2 = null; }
    if (this.cooldown3) { clearTimeout(this.cooldown3); this.cooldown3 = null; }
  }

  refresh()
  {
    this.doRefresh(false, null);
  }


  doRefresh(isForce, callback)
  {
    if (!isForce && this.cooldown)
    {
      if (this.cooldown2)
      {
        clearTimeout(this.cooldown2);
      }

      this.cooldown2 = setTimeout(() => {
        this.cooldown2 = null;
        this.doRefresh(isForce, callback)
      }, 3000);
    }
    else
    {
      this.forceUpdate(callback);
    }
  }


  onSendCommand()
  {
    this.doRefresh(true, () => {
      if (this.cooldown) {

        if (this.cooldown)
        {
          clearTimeout(this.cooldown);
        }

        this.pending = true;
        this.pending_it = {
          serial: this.props.it.serial,
          payload: Object.assign({}, this.props.it.payload || {}),
        }
      }
      else
      {
        this.publishCommand();
      }

      this.cooldown = setTimeout(() => {
        this.cooldown = null;
        if (this.pending) {
          this.pending = false;
          if (this.pending_it)
          {
            this.onSendCommandPending(this.pending_it);
            this.pending_it = null;
            this.cooldown = setTimeout(() => {
              this.cooldown = null;
            }, 1000);
          }
        }
      }, 1000);
    })
  }


  onSendCommandPending(_it)
  {
    this.publishCommand(_it);
  }



  publishCommand(_it)
  {
    var it = Object.assign({}, this.props.it);

    if (_it)
    {
      it = _it;
    }

    APISourcerMqtt.getInstance().publishDataAC(it.serial, it.payload);

    if (this.cooldown3) clearTimeout(this.cooldown3);
    this.cooldown3 = setTimeout(() => {
      // if (GLOBAL.IDENTIFY_DEVICE_IS_OFFLINE(it))
      // {
      //   this.forceUpdate()
      // }
    }, 2000 * 10);
  }





  onPowerOn()
  {
    var {
      it,
    } = this.props;

    it.payload.p = 1;
    this.onSendCommand();
  }

  onPowerOff()
  {
    var {
      it,
    } = this.props;

    it.payload.p = 0;
    this.onSendCommand();
  }



  render() {

    var {
      it,
    } = this.props;

    it.payload = it.payload || {};
    var m = it.payload && it.payload.m;
    var icon = `d_ac_ic_mode_cool_o`;
    var show_temp = true;

    if (m == 3) {
      icon = `d_ac_ic_mode_cool_o`;
    }
    else if (m == 0) {
      icon = `d_ac_ic_mode_auto_o`;
    }
    else if (m == 4) {
      icon = `d_ac_ic_mode_heat_o`;
    }
    else if (m == 6) {
      icon = `d_ac_ic_mode_fan_o`;
      show_temp = false;
    }
    else if (m == 2) {
      icon = `d_ac_ic_mode_dehum_o`;
      show_temp = false;
    }
    else {
      icon = `d_ac_ic_mode_auto_o`;
      show_temp = false;
    }

    var isOffline = GLOBAL.IDENTIFY_DEVICE_IS_OFFLINE(it);
    var isFirmwareUpdate = APISourcerPrelogin.D2Bros_Firmware_Compare_Device(it);
    var isVacationMode = it.vacation_mode && GLOBAL.STORE.vacation_mode.is_enabled;

    return (
        <ListItem
            style={{opacity: isOffline ? 0.5: 1, backgroundColor: '#fff'}} button={true} onPress={() => {
          this.props.onPressItem(it);
        }}>

          <TouchableOpacity
              disabled={isOffline || isVacationMode}
              onPress={() => {
                if (isFirmwareUpdate)
                {
                  this.props.onPressFirmwareUpdate(it, this);
                }
                else
                {
                  this.props.onPressItem(it, this);
                }
              }}
              style={{width: 64, height: 56}}>
            <Image source={GLOBAL.IMAGE.icon_model_ac} style={{width: 64, height: 56, resizeMode: `contain`}}/>
            {
              isFirmwareUpdate ? (
                  <Image source={GLOBAL.IMAGE.ic_firmware_update_o} style={{
                    position: `absolute`,
                    right: 0, bottom: 0,
                    margin: 4, width: 22, height: 22, resizeMode: `contain`}}/>
              ) : null
            }
            {
              isVacationMode ? (
                  <Image source={GLOBAL.IMAGE.ic_vacation_mode_o} style={{
                    position: `absolute`,
                    left: 0, bottom: 0,
                    margin: 4, width: 22, height: 22, resizeMode: `contain`}}/>
              ) : null
            }
          </TouchableOpacity>

          <View style={{
            opacity: isVacationMode ? 0.3 : 1.0,
            flexDirection: `row`, flex: 1, alignItems: `center`, justifyContent: `space-between`}}>

            <View style={{width: 175}}>
              <View style={{flexDirection: `row`}}>
                <Title style={{color: '#000'}}>{it.alias}</Title>
              </View>
              <View style={{flexDirection: `row`}}>
                <View style={{flexDirection: `row`, flex: 1, alignItems: `center`}}>
                  <Image source={GLOBAL.IMAGE.d_ac_ic_room_temp} style={{margin: 4, width: 30, height: 30, resizeMode: `contain`}} />
                  <Text>{(isOffline || (it.payload.dht_t !== 0 && !it.payload.dht_t) || it.payload.dht_t == -1) ? '--' : GLOBAL.GET_DEGREE(it.payload.dht_t) }</Text>
                </View>
                <View style={{flexDirection: `row`, flex: 1, alignItems: `center`}}>
                  <Image source={GLOBAL.IMAGE[icon]} style={{margin: 4, width: 26, height: 26, resizeMode: `contain`}}/>
                  {
                    show_temp ? (
                        <Text>{(isOffline || (it.payload.t <= 0) || (!it.payload.t)) ? '--' : it.payload.t}˚C</Text>
                    ) : null
                  }
                  {/*
                  <Text> [ID {it.payload.id}]</Text>
                  */}
                </View>
              </View>

              {/*<Text style={{fontSize: 10}}>*/}
                {/*{JSON.stringify(it)}*/}
              {/*</Text>*/}
            </View>

            {
              (isOffline) ? (
                  <View style={{flexDirection: `row`}}>
                    <Image source={GLOBAL.IMAGE.ic_device_offline_o} style={{margin: 4, width: 40, height: 40, resizeMode: `contain`}}/>
                  </View>
              ) : (

                  <View style={{flexDirection: `row`}}>
                    <Button transparent
                            disabled={isOffline || isVacationMode}
                            onPress={() => {
                      this.onPowerOff()
                    }} style={{paddingHorizontal: 4, width: 44, height: 44}}>
                      <Image source={it.payload.p == 0 ? GLOBAL.IMAGE.d_ac_ic_power_off_o :  GLOBAL.IMAGE.d_ac_ic_power_off_n} style={{resizeMode: `contain`, padding: 5, width: 34, height: 34}}/>
                    </Button>

                    <Button transparent
                            disabled={isOffline || isVacationMode}
                            onPress={() => {
                      this.onPowerOn()
                    }} style={{paddingHorizontal: 4, width: 44, height: 44}}>
                      <Image source={it.payload.p == 1 ? GLOBAL.IMAGE.d_ac_ic_power_on_o :  GLOBAL.IMAGE.d_ac_ic_power_on_n} style={{resizeMode: `contain`, padding: 5, width: 34, height: 34}}/>
                    </Button>
                  </View>

              )
            }

          </View>


        </ListItem>
    );
  }
}


export default class AccordionItem extends Component {
  render() {

    var {
      item,
      items,
    } = this.props;

    return (
        <Content>
          {
            items.map((it) => {
              return (
                  <AccordionItemSingle it={it}
                                       onPressItem={this.props.onPressItem}
                                       onPressFirmwareUpdate={this.props.onPressFirmwareUpdate}
                  />
              )
            })
          }
        </Content>
    );
  }
}
