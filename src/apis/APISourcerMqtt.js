import { Client, Message } from 'react-native-paho-mqtt';
import {
  DeviceEventEmitter
} from 'react-native';
import APISourcer from './APISourcer';
import moment from 'moment';

//Set up an in-memory alternative to global localStorage
const myStorage = {
  setItem: (key, item) => {
    myStorage[key] = item;
  },
  getItem: (key) => myStorage[key],
  removeItem: (key) => {
    delete myStorage[key];
  },
};

var instance = null;
var id = 0;


var Gateway_ack_callback = null;
var Smartconfig_ack_callback = null;



export default class APISourcerMqtt extends APISourcer
{

  setSmartconfigAckCallback(callback)
  {
    Smartconfig_ack_callback = callback;
  }

  setGateway_ack_callback(callback)
  {
    Gateway_ack_callback = callback;
  }



  /* SETTINGS: TOPICS */
  getMQTTTopicPrefix()
  {
    return `vp/iot/G`;
  }
  getMQTTTopicSubdeviceUpdate()
  {
    return `vp/iot/D`;
  }
  getMQTTTopicSmartConfigPrefix()
  {
    return `vp/iot/S`;
  }

  getMQTTTopicAsPub(type, serial)
  {
    var suf = [type, serial].join("/");
    suf = "/" + suf;
    return `${this.getMQTTTopicPrefix()}/app${suf}`;
  }

  getMQTTTopicAsSub(type, serial)
  {
    var suf = [type, serial].join("/");
    suf = "/" + suf;
    return `${this.getMQTTTopicPrefix()}/device${suf}`;
  }

  getMQTTTopicSubdeviceUpdateAsSub(type, serial)
  {
    var suf = [type, serial].join("/");
    suf = "/" + suf;
    return `${this.getMQTTTopicSubdeviceUpdate()}/device${suf}`;
  }

  getMQTTTopicSmartConfigAsSub(type)
  {
    var suf = [type].join("/");
    suf = "/" + suf;
    return `${this.getMQTTTopicSmartConfigPrefix()}/device${suf}`;
  }



  isMQTTTopicGateway(topic)
  {
    var cs = topic.split("/");
    if (cs.length >= 4)
    {
      if (cs[0] === "vp"
          && cs[1] === "iot"
          && cs[2] === "G"
          && cs[3] === "device")
      {
        return true;
      }
    }
    return false;
  }


  isMQTTTopicSmartConfig(topic)
  {
    var cs = topic.split("/");
    if (cs.length >= 4)
    {
      if (cs[0] === "vp"
          && cs[1] === "iot"
          && cs[2] === "S"
          && cs[3] === "device")
      {
        return true;
      }
    }
    return false;
  }
  //
  //
  // getMQTTTopicSerial(topic, prefix)
  // {
  //   var c1 = topic.split(prefix);
  //   if (c1.length > 1)
  //   {
  //     var c1_1 = c1[1];
  //     var c2 = c1_1.split('/');
  //     if (c2.length > 1)
  //     {
  //       return c2[1];
  //     }
  //   }
  // }










  /* FUNCTIONS */

  constructor()
  {
    super();
    this.fulfillClient();
    instance = this;
  }


  fulfillClient()
  {
    if (this.client)
    {
      this.client.disconnect();
      this.client = null;
    }

    this.clientId = "clientId-" + parseInt(Math.random() * 1000000);

    this.client = new Client({ uri: `${this.getBaseMQTTUrl()}`, clientId: this.clientId, storage: myStorage });

    this.client.on('connectionLost', (responseObject) => {
      if (responseObject.errorCode !== 0) {
        console.log(responseObject.errorMessage);
      } else {
      }
      this.client.disconnect();
    });

    this.client.on('messageReceived', (message) => {
      // alert(message.destinationName + ": " + message.payloadString);

      /* DO ANY DEBUG HERE FOR MQTT-MESSAGE RESPONSE-RELATED */

      if (this.isMQTTTopicGateway(message.destinationName))
      {
        var json = message.payloadString && JSON.parse(message.payloadString);
        Gateway_ack_callback && Gateway_ack_callback(json);
      }
      else if (this.isMQTTTopicSmartConfig(message.destinationName))
      {
        var json = message.payloadString && JSON.parse(message.payloadString);
        Smartconfig_ack_callback && Smartconfig_ack_callback(json);
      }
    });

  }


  static getInstance()
  {
    if (instance == null) {
      instance = new APISourcerMqtt();
    }
    return instance;
  }


  connect(reconnect, successConnectCallback, isForce)
  {
    if (isForce || !this.client.isConnected())
    {
      if (isForce)
      {
        debugger;
        this.fulfillClient();
      }
      this.client.connect()
          .then((response) => {
            // Once a connection has been made, make a subscription and send a message.
            // console.log('onConnect');
            // // alert('connected');
            // // alert(JSON.stringify(response));
            if (successConnectCallback)
            {
              successConnectCallback();
            }

            // alert("CONNECTED");

            return null;
          })
          .catch((responseObject) => {
            if (responseObject.errorCode !== 0) {
              console.log('onConnectionLost:' + responseObject);
              //alert(responseObject.errorMessage);
            }

            if (reconnect)
            {
              setTimeout(() => {
                this.connect(reconnect,successConnectCallback);
              }, 5000);
            }
          })
    }
    else
    {
      if (successConnectCallback)
      {
        successConnectCallback();
      }
    }
  }



  unsubscribeGateway(type, serial)
  {
    this.client.unsubscribe(this.getMQTTTopicAsSub(type,serial));
    this.client.unsubscribe(this.getMQTTTopicSubdeviceUpdateAsSub(type,serial));
  }


  subscribeGateway(type, serial)
  {
    this.client.subscribe(this.getMQTTTopicAsSub(type,serial));
    this.client.subscribe(this.getMQTTTopicSubdeviceUpdateAsSub(type,serial));
  }



  unsubscribeSmartConfig(type)
  {
    this.client.unsubscribe(this.getMQTTTopicSmartConfigAsSub(type));
  }


  subscribeSmartConfig(type)
  {
    this.client.subscribe(this.getMQTTTopicSmartConfigAsSub(type));
  }




  disconnect()
  {
    // alert("DISCON");
    if (this.client.isConnected())
    {
      this.client.disconnect();
    }
  }

  publish(topic, payload)
  {
    // alert("DP")
    try
    {
      const message = new Message(JSON.stringify(payload));
      message.destinationName = topic;
      message.qos = 1;
      message._qos = 1;
      this.client.send(message);
    }
    catch (e)
    {
      alert(e.message)
      this.connect(false, () => {
        try
        {
          const message = new Message(JSON.stringify(payload));
          message.destinationName = topic;
          message.qos = 1;
          message._qos = 1;
          this.client.send(message);
        }
        catch (e)
        {
          console.log(e);
        }
      }, true);
    }
  }

  publishDataAC(serial, ac)
  {
    id += 1;
    if (id > 999)
    {
      id = 1;
    }

    var payload = {
      id: id,
      p: ac.p || 0,
      m: ac.m || 0,
      f: ac.f || 1,
      t: ac.t || 25,
      sv: ac.sv || 0,
      sh: ac.sh || 0,
      ct: ac.ct || 1212,
      ot: ac.ot || 1313,
      otio: ac.otio || 0,
    }

    var dev = GLOBAL.STORE.get_device(serial);
    if (dev)
    {
      payload.tp = dev.host_model || 1;
      dev.date = moment(new Date()).subtract(60*21, "seconds").add(GLOBAL.FW_SIGNAL_ACK_TIMEOUT || 30, "seconds").toDate();
    }

    GLOBAL.OUTPUT_MQTT_RESPONSE && GLOBAL.OUTPUT_MQTT_RESPONSE({serial, from: "app", type: "data", json: JSON.stringify(payload)});
    return this.publish(this.getMQTTAppDataTopic(serial), payload);
  }

  publishCommandAC(serial, action, userInfo)
  {
    var payload = Object.assign({}, {
      access_token: GLOBAL.GET_ACCESS_TOKEN(serial),
      action: action, // deactivate / activate
    }, userInfo || {});

    return this.publish(this.getMQTTAppControlTopic(serial), payload);
  }
}

