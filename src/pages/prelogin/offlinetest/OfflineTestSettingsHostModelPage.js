import React, { Component } from 'react';
import { ScrollView, Image } from "react-native";
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3,
} from "native-base";
import BasePage from "../../base/BasePage";

export default class OfflineTestSettingsHostModelPage extends BasePage {

  constructor(props)
  {
    super(props);
    this.state = {
      isComplete: false,
      models: [
        {title: "Daikin VAC Air conditioner series"},
        {title: "Daikin 420"},
      ]
    }

    this.state.selected_model = this.props.navigation.state.params.item.host_model;
  }

  componentDidMount(): void {
    setTimeout(() => {
      this.setState({
        isComplete: true,
      })
    }, 5000)
  }

  renderInner() {
    return (
        <Container>

          <Header>
            <Left>
              <Button transparent onPress={() => {
                this.props.navigation.pop();
              }}>
                <Icon name='arrow-back' style={{color: '#039BE5'}} color={`#039BE5`} />
              </Button>
            </Left>
            <Body>
            <Title>{`Configure`.i18n()}</Title>
            </Body>
            <Right>
            </Right>
          </Header>

          <ScrollView>

            <Content padder>

              <H1 style={{color: '#586C78', marginTop: 60, marginBottom: 30, textAlign: `center`}}>{`IR remote model`.i18n()}</H1>

              <Text style={{color: '#586C78', padding: 5, textAlign: `left`}}>{`Please select a host model from our officially supported list of models:`.i18n()}</Text>

              <List>
                {
                  this.state.models.map((it, i) => {
                    return (
                        <ListItem
                            button
                            onPress={() => {
                              this.setState({
                                selected_model: it.title,
                              })
                            }}
                            selected={this.state.selected_model === it.title}>
                          <Left>
                            <Text>{it.title}</Text>
                          </Left>
                          <Right>
                            <Icon name="checkmark" style={{
                              opacity: this.state.selected_model === it.title ? 1 : 0
                            }} />
                          </Right>
                        </ListItem>
                        )
                  })
                }
              </List>

              <View style={{height: 50}}/>

              <Button
                  onPress={() => {
                    this.props.navigation.navigate("OfflineTestFindDevicePage");
                  }}
                  style={{backgroundColor: '#4fadf9', justifyContent: `center`}}>
                <H3 style={{color: '#fff'}}>{`Ready, proceed to next step`.i18n()}</H3>
              </Button>

            </Content>

          </ScrollView>

        </Container>
    );
  }
}
