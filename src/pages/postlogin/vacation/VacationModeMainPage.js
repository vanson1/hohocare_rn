import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { Container, Header, Left, Switch, Right, Body, Title, Accordion, Button, Icon, Content, List, ListItem, Text, H1, H2, H3 } from 'native-base';

import BasePage from "../../base/BasePage";
import APISourcerMqtt from '../../../apis/APISourcerMqtt';
import moment from 'moment';
import BaseNavigationBar from '../../base/BaseNavigationBar';

const dataArray = [
      {isSelected: true, title: "BedRoomTH(WWW)", temp_room: "28˚c", temp_ac: "25˚c", hum_room: "65%",
        ssid: "D2Bros-XXXXXXXX", serial: "D2Bros-XXXXXXXX", host_model: "Daikin VAC Air conditioner series", alias: "BedRoomTH(WWW)", status: "Online"},
      {isSelected: false, title: "KidsRoomTH(WWW)", temp_room: "28˚c", temp_ac: "25˚c", hum_room: "65%",
        ssid: "D2Bros-XXXXXXXX", serial: "D2Bros-XXXXXXXX", host_model: "Daikin VAC Air conditioner series", alias: "BedRoomTH(WWW)", status: "Online"},
      {isSelected: false, title: "LivingRoomTH(WWW)", temp_room: "28˚c", temp_ac: "25˚c", hum_room: "65%",
        ssid: "D2Bros-XXXXXXXX", serial: "D2Bros-XXXXXXXX", host_model: "Daikin VAC Air conditioner series", alias: "BedRoomTH(WWW)", status: "Online"},
];

export default class HomeDashboardPage extends BasePage {

  constructor(props)
  {
    super(props);
    this.refresh = this.refresh.bind(this);
    this.state = {
      vacation_mode: GLOBAL.STORE.vacation_mode,
      items: GLOBAL.STORE.devices || [],
    }
  }

  componentDidMount(): void {
    this.refresh();
    GLOBAL.LISTEN_LAST_UPDATE_UPDATES(this.refresh);
  }

  componentWillAppear() {
    super.componentWillAppear();
    this.refresh();
    GLOBAL.LISTEN_LAST_UPDATE_UPDATES(this.refresh);
  }


  componentWillDisappear() {
    super.componentWillDisappear();
    GLOBAL.UNLISTEN_LAST_UPDATE_UPDATES(this.refresh);
  }

  componentWillUnmount(): void {

  }

  refresh()
  {
    this.state.items = GLOBAL.STORE.devices;
    this.forceUpdate();
  }

  isDeviceOffline(device)
  {
    return GLOBAL.IDENTIFY_DEVICE_IS_OFFLINE(device);
  }

  renderInner() {
    return (
        <Container>

          <BaseNavigationBar {...this.props}
                             isMenu
                             title={`Vacation Mode`.i18n()}
          />

          <Content>

            <View style={{borderBottomColor: '#eee', borderBottomWidth: 1, flexDirection: `row`, padding: 15}}>
              <H3 style={{flex: 1}}>{`Enable vacation mode`.i18n()}</H3>
              <Switch value={this.state.vacation_mode.is_enabled} onValueChange={(v) => {
                this.setState({vacation_mode: {is_enabled: v}}, () => {

                  GLOBAL.STORE.vacation_mode.is_enabled = this.state.vacation_mode.is_enabled;
                  this.forceUpdate();

                  this.state.items.map((it, i) => {
                    var serial = it.serial;
                    APISourcerMqtt.getInstance().publishCommandAC(serial, 'settings', {
                      led_disable: (it.info || {}).led_disable,
                      vacation_mode: it.vacation_mode && GLOBAL.STORE.vacation_mode.is_enabled,
                    });
                    GLOBAL.STORE.apply_vacation_mode();
                  });

                })
              }}/>
            </View>

            <Text note>
            </Text>

            <View padder style={{backgroundColor: 'rgba(216, 216, 216, 1)', flexDirection: `row`, padding: 10,
              opacity: this.state.vacation_mode.is_enabled ? 1.0 : 0.25}}>
              <Title>{`Apply to the following units:`.i18n()}</Title>
            </View>

            <List>
              {
                this.state.items.map((it, i) => {
                  if (this.isDeviceOffline(it))
                  {
                    return null;
                  }
                  return (
                      <ListItem
                          style={{opacity: this.state.vacation_mode.is_enabled ? 1.0 : 0.25}}
                          selected={it.vacation_mode}>
                        <Left>
                          <View>
                            <Image source={GLOBAL.IMAGE.icon_model_ac} style={{width: 64, height: 56, resizeMode: `contain`}}/>
                          </View>
                          <Text>{it.alias || it.serial} {this.isDeviceOffline(it) ? `(Offline)` : ``}</Text>
                        </Left>
                        <Right>
                          <Switch value={it.vacation_mode} onValueChange={(v) => {
                            it.vacation_mode = v;
                            this.forceUpdate(() => {
                              var serial = it.serial;
                              // alert(GLOBAL.STORE.vacation_mode);
                              APISourcerMqtt.getInstance().publishCommandAC(serial, 'settings', {
                                led_disable: (it.info || {}).led_disable,
                                vacation_mode: it.vacation_mode && GLOBAL.STORE.vacation_mode.is_enabled,
                              });
                              GLOBAL.STORE.apply_vacation_mode();
                            })
                          }}/>
                        </Right>
                      </ListItem>
                  )
                })
              }
            </List>

          </Content>

        </Container>
    );
  }
}
