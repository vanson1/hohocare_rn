import React, { Component } from 'react';
import { Container, Header, View, Left, Right, Body, Title, Accordion, Button, Icon, Content, List, ListItem, Text } from 'native-base';

export default class AccordionHeader extends Component {
  render() {

    var {
      item,
      expanded,
    } = this.props;

    return (
        <View style={{backgroundColor: '#f1f3f2', marginLeft: 0, padding: 10, flexDirection: `row`, alignItems: `center` }}>
          <Icon name={expanded ? "arrow-up" : "arrow-down"} style={{paddingHorizontal: 10}}/>
          <Text>{item.title}</Text>
        </View>
    );
  }
}
