import React, { Component } from 'react';
import { Image, ImageBackground, Dimensions, SafeAreaView } from 'react-native';
import { View, Container, Header, Left, Right, Body, Title, Accordion, Button, Icon, Content, List, ListItem, Text, H1, H2, H3 } from 'native-base';
import BasePage from "../../../base/BasePage";
import APISourcerMqtt from "../../../../apis/APISourcerMqtt";
import { withNavigationFocus } from "react-navigation";
import moment from 'moment';

const {width, height} = Dimensions.get("window");


class BottomSheetSwingDirection extends React.Component {
  constructor(props)
  {
    super(props);
    this.state = {

    }
  }

  componentDidMount(): void {

  }

  render()
  {
    var desc = "";

    var d1 = GLOBAL.IMAGE.d_ac_ic_swing_off_n;
    var d2 = GLOBAL.IMAGE.d_ac_ic_swing_horizontal_n;
    var d3 = GLOBAL.IMAGE.d_ac_ic_swing_vertical_n;
    var d4 = GLOBAL.IMAGE.d_ac_ic_swing_3D_n;

    if (this.props.item.sh == 0
        && this.props.item.sv == 0)
    {
      desc = "Off";
      d1 = GLOBAL.IMAGE.d_ac_ic_swing_off_o;
    }

    if (this.props.item.sh == 1
        && this.props.item.sv == 0)
    {
      desc = "Horizontal";
      d2 = GLOBAL.IMAGE.d_ac_ic_swing_horizontal_o;
    }

    if (this.props.item.sh == 0
        && this.props.item.sv == 1)
    {
      desc = "Vertical";
      d3 = GLOBAL.IMAGE.d_ac_ic_swing_vertical_o;
    }

    if (this.props.item.sh == 1
        && this.props.item.sv == 1)
    {
      desc = "3D mode";
      d4 = GLOBAL.IMAGE.d_ac_ic_swing_3D_o;
    }

    return (
        <Container>
          <View style={{flexDirection: `row`, alignItems: `center`, justifyContent: `space-around`}}>
            <Text style={{fontWeight: `bold`, padding: 10, fontSize: 16, color: '#666'}}>
              {`Air flow direction`.i18n()}
            </Text>
          </View>
          <View style={{flexDirection: `row`, alignItems: `center`, justifyContent: `space-around`}}>
            <Text style={{fontWeight: `bold`, padding: 10, fontSize: 14, color: '#0ca4e3'}}>
              {desc && desc.i18n()}
            </Text>
          </View>
          <View style={{marginTop: 15, flexDirection: `row`, alignItems: `center`, justifyContent: `space-around`}}>
            <Button transparent
                    onPress={() => {
                      this.props.onValueChangeItem({sh: 0, sv: 0})
                    }}
                    style={{padding: 30}}>
              <Image source={d1} style={{width: 56, height: 56}}/>
            </Button>
            <Button transparent
                    onPress={() => {
                      this.props.onValueChangeItem({sh: 1, sv: 0})
                    }}
                    style={{padding: 30}}>
              <Image source={d2} style={{width: 56, height: 56}}/>
            </Button>
            <Button transparent
                    onPress={() => {
                      this.props.onValueChangeItem({sh: 0, sv: 1})
                    }}
                    style={{padding: 30}}>
              <Image source={d3} style={{width: 56, height: 56}}/>
            </Button>
            <Button transparent
                    onPress={() => {
                      this.props.onValueChangeItem({sh: 1, sv: 1})
                    }}
                    style={{padding: 30}}>
              <Image source={d4} style={{width: 56, height: 56}}/>
            </Button>
          </View>
        </Container>
    )
  }
}

export default BottomSheetSwingDirection;

