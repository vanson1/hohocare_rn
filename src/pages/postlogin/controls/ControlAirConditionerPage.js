import React, { Component } from 'react';
import { Image, ImageBackground, Dimensions, SafeAreaView, Platform } from 'react-native';
import { View, Container, Header, Left, Right, Body, Title, Accordion, Button, Icon, Content, List, ListItem, Text, H1, H2, H3 } from 'native-base';
import BasePage from "../../base/BasePage";
import CircularSlider from './components/CircularSlider';
import APISourcerMqtt from "../../../apis/APISourcerMqtt";
import { withNavigationFocus } from "react-navigation";
import moment from 'moment';
import RBSheet from "react-native-raw-bottom-sheet";
import BottomSheetFanPower from './components/BottomSheetFanPower'
import BottomSheetSwingDirection from './components/BottomSheetSwingDirection';
import APISourcerExternal from '../../../apis/APISourcerExternal';
import BaseNavigationBar from '../../base/BaseNavigationBar';

const {width, height} = Dimensions.get("window");


class ControlAirConditionerPage extends BasePage {
  constructor(props)
  {
    super(props);
    this.state = {
      startAngle: 0,
      angleLength: 0,
    }

    this.refresh = this.refresh.bind(this);
  }

  getDeviceItem()
  {
    return this.local_config_item;
  }

  componentDidMount(): void {
    setTimeout(() => {
      this.setState({
        is_ready: true,
      })
    }, 100);
    this.local_config_item = Object.assign({}, GLOBAL.CONFIG_ITEM);
    this.local_config_item = Object.assign(this.local_config_item, {
      payload: Object.assign({}, GLOBAL.CONFIG_ITEM.payload || {}),
    });
    this.refresh();
    GLOBAL.LISTEN_DEVICE_UPDATES(GLOBAL.CONFIG_ITEM.serial, this.refresh);
    this.loopForceUpdate = setInterval(() => {
      this.forceUpdate();
    }, 60000);
  }

  componentWillUnmount(): void {
    clearInterval(this.loopForceUpdate);
  }


  componentWillAppear() {
    super.componentWillAppear();
    this.setState({
      is_ready: true,
    }, () => {
      this.doRefresh(() => {})
    })
    this.local_config_item = Object.assign({}, GLOBAL.CONFIG_ITEM);
    this.local_config_item = Object.assign(this.local_config_item, {
      payload: Object.assign({}, GLOBAL.CONFIG_ITEM.payload || {}),
    });
    this.refresh();
    GLOBAL.LISTEN_DEVICE_UPDATES(GLOBAL.CONFIG_ITEM.serial, this.refresh);
  }

  componentWillDisappear() {
    super.componentWillDisappear();

    if (this.cooldown)
    {
      clearTimeout(this.cooldown);
      this.cooldown = null;
      if (this.pending)
      {
        this.pending = false;
        this.onSendCommandPending();
      }

      if (this.pending_silent)
      {
        this.pending_silent = false;
      }
    }

    GLOBAL.CONFIG_ITEM.payload = Object.assign({}, this.local_config_item.payload);
    try
    {
      GLOBAL.HOME_ITEM && GLOBAL.HOME_ITEM.refresh && GLOBAL.HOME_ITEM.refresh(true);
      GLOBAL.EMIT_DEVICE_UPDATES(GLOBAL.CONFIG_ITEM.serial)
    }
    catch (e)
    {
    }

    this.setState({
      is_ready: false,
    })
    GLOBAL.UNLISTEN_DEVICE_UPDATES(GLOBAL.CONFIG_ITEM.serial, this.refresh);
    if (this.cooldown2) { clearTimeout(this.cooldown2); this.cooldown2 = null; }
    if (this.cooldown3) { clearTimeout(this.cooldown3); this.cooldown3 = null; }
  }



  refresh(callback)
  {
    if (!this.pending && !this.pending_silent)
    {
      if (this.cooldown)
      {
        if (this.cooldown2)
        {
          clearTimeout(this.cooldown2);
        }

        this.cooldown2 = setTimeout(() => {
          this.cooldown2 = null;
          this.refresh(callback)
        }, 10000);
      }
      else
      {
        this.local_config_item = Object.assign(this.local_config_item || {}, {
          payload: Object.assign({}, (GLOBAL.CONFIG_ITEM && GLOBAL.CONFIG_ITEM.payload) || {}),
        });
        return this.doRefresh(callback)
      }
    }
  }



  meRefresh(callback)
  {
    return this.doRefresh(callback)
  }



  doRefresh(callback)
  {
    if (this.circularSlider
    && this.circularSlider.getAngleLengthByParsedValue)
    {
      var t = this.getDeviceItem().payload.t;
      var m = this.getDeviceItem().payload.m;
      if (!t || isNaN(t))
      {
        t = this.getTempDefaultByMode(m);
        this.getDeviceItem().payload.t = t;
      }
      this.state.angleLength = this.circularSlider.getAngleLengthByParsedValue(t, this.getTempMinByMode(m), this.getTempMaxByMode(m));
      if (!this.state.angleLength || isNaN(this.state.angleLength))
      {
        this.state.angleLength = 0;
      }
    }
    return this.forceUpdate(callback);
  }


  onSendCommand()
  {
    // console.log("DEBUGME", this.getDeviceItem(), GLOBAL.CONFIG_ITEM)
    this.meRefresh(() => {

      var cooldownTime = 750;
      if (this.cooldown)
      {
        this.pending = true;
        clearTimeout(this.cooldown);
        this.cooldown = null;
      }
      else
      {
        if (!this.pending_started)
        {
          this.pending_started = true;
        }
        else
        {
          this.pending_silent = true;
        }
        this.pending = true;
        cooldownTime = 350;
        // this.publishCommand();
      }

      this.cooldown = setTimeout(() => {
        this.cooldown = null;
        if (this.pending)
        {
          this.pending = false;
          this.onSendCommandPending();
          this.cooldown = setTimeout(() => {
            this.cooldown = null;
          }, 750);
        }

        if (this.pending_silent)
        {
          this.pending_silent = false;
        }

      }, cooldownTime);
    });
  }


  onSendCommandPending()
  {
    this.publishCommand();
  }



  publishCommand()
  {
    APISourcerMqtt.getInstance().publishDataAC(this.getDeviceItem().serial, this.getDeviceItem().payload);

    if (this.cooldown3)
    {
      clearTimeout(this.cooldown3);
    }

    this.cooldown3 = setTimeout(() => {
      this.cooldown3 = null;
      this.refresh(() => {})
    }, 6000);

    setTimeout(() => {
      if (this.isDeviceOffline())
      {
        this.forceUpdate()
      }
    }, 1000 * 10);
  }

  onTempIncrease()
  {
    var payload = this.getDeviceItem().payload;
    if (!payload.t) {
      payload.t = this.getTempDefaultByMode(payload.m);
    }
    payload.t += 1;
    if (payload.t > this.getTempMaxByMode(payload.m)) {
      payload.t = this.getTempMaxByMode(payload.m);
    }
    this.onSendCommand();
  }


  onTempDecrease()
  {
    var payload = this.getDeviceItem().payload;
    if (!payload.t) {
      payload.t = this.getTempDefaultByMode(payload.m);
    }
    payload.t -= 1;
    if (payload.t < this.getTempMinByMode(payload.m)) {
      payload.t = this.getTempMinByMode(payload.m);
    }
    this.onSendCommand();
  }


  onTempChangeTo(temp)
  {
    var payload = this.getDeviceItem().payload;
    payload.t = temp;
    if (payload.t < this.getTempMinByMode(payload.m)) {
      payload.t = this.getTempMinByMode(payload.m);
    }
    if (payload.t > this.getTempMaxByMode(payload.m)) {
      payload.t = this.getTempMaxByMode(payload.m);
    }
    this.onSendCommand();
  }


  onTempChange()
  {
    this.onSendCommand();
  }

  onPowerOn()
  {
    var payload = this.getDeviceItem().payload;
    payload.p = 1;
    this.onSendCommand();
  }

  onPowerOff()
  {
    var payload = this.getDeviceItem().payload;
    payload.p = 0;
    this.onSendCommand();
  }

  onModeAuto()
  {
    var payload = this.getDeviceItem().payload;
    if (payload.m != 0)
    {
      payload.t = this.getTempDefaultByMode(0);
    }
    payload.m = 0;
    this.onSendCommand();
  }

  onModeCool()
  {
    var payload = this.getDeviceItem().payload;
    if (payload.m != 3)
    {
      payload.t = this.getTempDefaultByMode(3);
    }
    payload.m = 3;
    this.onSendCommand();
  }

  onModeHeat()
  {
    var payload = this.getDeviceItem().payload;
    if (payload.m != 4)
    {
      payload.t = this.getTempDefaultByMode(4);
    }
    payload.m = 4;
    this.onSendCommand();
  }

  onModeFan()
  {
    var payload = this.getDeviceItem().payload;
    if (payload.m != 6)
    {
      payload.t = this.getTempDefaultByMode(6);
    }
    payload.m = 6;
    this.onSendCommand();
  }

  onModeDehum()
  {
    var payload = this.getDeviceItem().payload;
    if (payload.m != 2)
    {
      payload.t = this.getTempDefaultByMode(2);
    }
    payload.m = 2;
    this.onSendCommand();
  }

  onFanPowerToggle()
  {
    var payload = this.getDeviceItem().payload;
    payload.f += 1;
    if (payload.f > 5) {
      payload.f = 1;
    } else if (payload.f < 1) {
      payload.f = 1;
    }
    this.onSendCommand();
  }

  onSwipeToggle()
  {
    var payload = this.getDeviceItem().payload;
    if (payload.sh == 1) {
      payload.sh = 0;
    } else {
      payload.sh = 1;
    }
    this.onSendCommand();
  }




  getTempMinByMode(mode)
  {
    var t = 18;
    if (mode == 0)
    {

    }
    if (mode == 2)
    {

    }
    if (mode == 3)
    {
      t = 18;
    }
    if (mode == 4)
    {
      t = 10;
    }
    if (mode == 6)
    {

    }
    return t;
  }
  getTempMaxByMode(mode)
  {
    var t = 30;
    if (mode == 0)
    {

    }
    if (mode == 2)
    {

    }
    if (mode == 3)
    {
      t = 32;
    }
    if (mode == 4)
    {
      t = 30;
    }
    if (mode == 6)
    {

    }
    return t;
  }
  getTempDefaultByMode(mode)
  {
    var t = 25;
    if (mode == 0)
    {

    }
    if (mode == 2)
    {

    }
    if (mode == 3)
    {
      t = 22;
    }
    if (mode == 4)
    {
      t = 25;
    }
    if (mode == 6)
    {

    }
    return t;
  }








  __renderHeader()
  {
    var device = this.getDeviceItem();

    return (

        <BaseNavigationBar {...this.props}
            icon_left={
              <Button transparent onPress={() => {
                this.props.navigation.navigate("HomeDashboardPage");
              }}>
                <Icon style={{color: '#039BE5'}} name='arrow-back' />
              </Button>
            }
            icon_right={
              <Right>
                <Button transparent onPress={() => {
                  this.props.navigation.navigate("ConfigureMainPage");
                }}>
                  <Icon style={{color: '#039BE5'}} name='settings' />
                </Button>
              </Right>
            }
            title={device.alias}
        />
    )
  }

  isDeviceOffline()
  {
    var device = GLOBAL.CONFIG_ITEM;
    var isOffline = GLOBAL.IDENTIFY_DEVICE_IS_OFFLINE(device);
    return isOffline;
  }


  isDeviceVacationMode()
  {
    var device = GLOBAL.CONFIG_ITEM;
    var isVacationMode = device.vacation_mode && GLOBAL.STORE.vacation_mode.is_enabled;
    return isVacationMode;
  }


  renderInner() {
    if (!this.state.is_ready)
    {
      return null;
    }

    var device = this.getDeviceItem();
    var isOffline = this.isDeviceOffline();

    return (
        <Container>

          {this.__renderHeader()}

          <View style={{flex: 1}}>
            <View style={{flex: 1, opacity: (isOffline || this.isDeviceVacationMode()) ? 0.3 : 1}}>

              <Text note style={{padding: 5, textAlign: `center`}}>— {`Last Updated at`.i18n()} {GLOBAL.LAST_UPDATED_TIME} —</Text>



              <View style={{height: 15}}/>

              {this.__renderTop(device)}

              <View style={{height: 15}}/>

              {this.__renderCenterTopModeButtons(device)}

              {this.__renderCenterOvalControl(device)}


            </View>


            {
              isOffline ? (
                  <View style={{backgroundColor: '#ff2222', position: `absolute`, top: 0, left: 0, right: 0}}>
                    <Text style={{padding: 10, textAlign: `center`, color: '#fff'}}>{`Device offline`.i18n()}</Text>
                  </View>
              ) : (
                  this.isDeviceVacationMode() ? (
                      <View style={{alignItems: `center`,
                        position: `absolute`, top: 0, left: 0, right: 0,
                        justifyContent: `center`,
                        backgroundColor: '#57595d', padding: 5, flexDirection: `row`}}>
                        <Image source={GLOBAL.IMAGE.ic_vacation_mode_o} style={{width: 22, height: 22}}/>
                        <Text style={{color: `#fff`, paddingHorizontal: 5, textAlign: `center`}}>{`Vacation mode is active`.i18n()}</Text>
                      </View>
                  ) : null
              )
            }
          </View>

          {this.__renderBottomBarButtons(device)}

        </Container>
    );
  }



  __renderTop(device)
  {
    var isOffline = this.isDeviceOffline() || this.isDeviceVacationMode();

    return (
        <View padder style={{flexDirection: `row`, alignItems: `flex-start`, justifyContent: `space-between`, marginHorizontal: 25}}>
          <Button transparent disabled={isOffline} onPress={() => {
            this.onPowerOff()
          }}>
            <Image source={device.payload.p == 0 ? GLOBAL.IMAGE.d_ac_ic_power_off_o : GLOBAL.IMAGE.d_ac_ic_power_off_n} style={{width: 42, height: 42, padding: 5, resizeMode: `contain`}} />
          </Button>

          <View style={{paddingHorizontal: 20, flex: 1, flexDirection: `row`, alignItems: `flex-start`, justifyContent: `space-around`}}>

            <View>
              <View style={{flexDirection: `row`, alignItems: `center`, justifyContent: `space-between`}}>
                <Image source={GLOBAL.IMAGE.d_ac_ic_env_temp} style={{width: 26, height: 26, padding: 5, resizeMode: `contain`}} />
                <Text style={{fontSize: 14}}>{((APISourcerExternal.weather_temp !== 0 && !APISourcerExternal.weather_temp)) ? '--' : GLOBAL.GET_DEGREE(APISourcerExternal.weather_temp)}</Text>
              </View>
              <View style={{flexDirection: `row`, alignItems: `center`, justifyContent: `space-between`}}>
                <Image source={GLOBAL.IMAGE.d_ac_ic_room_temp} style={{width: 26, height: 26, padding: 5, resizeMode: `contain`}} />
                <Text style={{fontSize: 14}}>{((device.payload.dht_t !== 0 && !device.payload.dht_t) || device.payload.dht_t == -1) ? '--' : GLOBAL.GET_DEGREE(device.payload.dht_t)}</Text>
              </View>
            </View>
            <View style={{width: 10}}/>
            <View>
              <View style={{flexDirection: `row`, alignItems: `center`, justifyContent: `space-between`}}>
                <Image source={GLOBAL.IMAGE.d_ac_ic_mode_dehum_n} style={{width: 26, height: 26, padding: 5, resizeMode: `contain`}} />
                <Text style={{fontSize: 14}}>{((APISourcerExternal.weather_humidity !== 0 && !APISourcerExternal.weather_humidity)) ? '--' : APISourcerExternal.weather_humidity}%</Text>
              </View>
              <View style={{flexDirection: `row`, alignItems: `center`, justifyContent: `space-between`}}>
                <Image source={GLOBAL.IMAGE.d_ac_ic_room_hum} style={{width: 26, height: 26, padding: 5, resizeMode: `contain`}} />
                <Text style={{fontSize: 14}}>{((device.payload.dht_h !== 0 && !device.payload.dht_h) || device.payload.dht_h == -1) ?  '--' : device.payload.dht_h}%</Text>
              </View>
            </View>
          </View>

          <Button transparent disabled={isOffline} onPress={() => {
            this.onPowerOn()
          }}>
            <Image source={device.payload.p == 1 ? GLOBAL.IMAGE.d_ac_ic_power_on_o : GLOBAL.IMAGE.d_ac_ic_power_on_n} style={{width: 42, height: 42, padding: 5, resizeMode: `contain`}} />
          </Button>
        </View>
    )
  }


  __renderCenterTopModeButtons(device)
  {
    var isOffline = this.isDeviceOffline() || this.isDeviceVacationMode();

    return (

        <View style={{flexDirection: `row`, alignItems: `flex-start`, justifyContent: `space-between`, marginHorizontal: 50}}>
          <View style={{height: 60, flex: 1, flexDirection: `row`, alignItems: `center`, justifyContent: `center`}}>
            <Button disabled={isOffline} transparent onPress={() => {
              this.onModeAuto()
            }} style={{width: 44, height: 44, padding: 5, marginTop: 35, marginHorizontal: 7}}>
              <Image source={device.payload.m === 0 ? GLOBAL.IMAGE.d_ac_ic_mode_auto_o : GLOBAL.IMAGE.d_ac_ic_mode_auto_n} style={{width: 30, height: 30}} />
            </Button>
            <Button disabled={isOffline} transparent onPress={() => {
              this.onModeCool()
            }} style={{width: 44, height: 44, padding: 5, marginTop: 10, marginHorizontal: 7}}>
              <Image source={device.payload.m === 3 ? GLOBAL.IMAGE.d_ac_ic_mode_cool_o : GLOBAL.IMAGE.d_ac_ic_mode_cool_n} style={{width: 30, height: 30}} />
            </Button>
            <Button disabled={isOffline} transparent onPress={() => {
              this.onModeHeat()
            }} style={{width: 44, height: 44, padding: 5, marginTop: 0, marginHorizontal: 7}}>
              <Image source={device.payload.m === 4 ? GLOBAL.IMAGE.d_ac_ic_mode_heat_o : GLOBAL.IMAGE.d_ac_ic_mode_heat_n} style={{width: 30, height: 30}} />
            </Button>
            <Button disabled={isOffline} transparent onPress={() => {
              this.onModeFan()
            }} style={{width: 44, height: 44, padding: 5, marginTop: 10, marginHorizontal: 7}}>
              <Image source={device.payload.m === 6 ? GLOBAL.IMAGE.d_ac_ic_mode_fan_o : GLOBAL.IMAGE.d_ac_ic_mode_fan_n} style={{width: 30, height: 30}} />
            </Button>
            <Button disabled={isOffline} transparent onPress={() => {
              this.onModeDehum()
            }} style={{width: 44, height: 44, padding: 5, marginTop: 35, marginHorizontal: 7}}>
              <Image source={device.payload.m === 2 ? GLOBAL.IMAGE.d_ac_ic_mode_dehum_o : GLOBAL.IMAGE.d_ac_ic_mode_dehum_n} style={{width: 30, height: 30}} />
            </Button>
          </View>
        </View>
    )
  }


  __renderCenterOvalControl(device)
  {
    var isOffline = this.isDeviceOffline();

    var mode = device.payload.m;
    var bg = GLOBAL.IMAGE.d_ac_ic_circle_oval_upper_off;
    var is_show_digits = false;
    var title = " -- ";

    if (!isOffline)
    {
      if (mode == 0)
      {
        bg = GLOBAL.IMAGE.d_ac_ic_circle_oval_full_auto;
        is_show_digits = true;
        title = "Auto";
      }
      else if (mode == 2)
      {
        bg = GLOBAL.IMAGE.d_ac_ic_circle_oval_upper_dehum;
        is_show_digits = false;
        title = "Dry";
      }
      else if (mode == 3)
      {
        bg = GLOBAL.IMAGE.d_ac_ic_circle_oval_full_cool;
        is_show_digits = true;
        title = "Cool";
      }
      else if (mode == 4)
      {
        bg = GLOBAL.IMAGE.d_ac_ic_circle_oval_full_heat;
        is_show_digits = true;
        title = "Heat";
      }
      else if (mode == 6)
      {
        bg = GLOBAL.IMAGE.d_ac_ic_circle_oval_upper_fan;
        is_show_digits = false;
        title = "Fan";
      }
    }

    return (
        <View style={{flexDirection: `row`, alignItems: `flex-start`, justifyContent: `space-between`, marginHorizontal: 50}}>
          <View style={{height: 50}}>
          </View>

          <View
              pointerEvents={(this.isDeviceVacationMode() || isOffline) ? 'none' : 'auto'}
              style={{}}>
            <ImageBackground source={bg} style={{width: width - 100, height: width - 100, resizeMode: `contain`, paddingTop: 3}} >

              <CircularSlider
                  ref={(v) => {this.circularSlider = v;}}
                  startAngle={this.state.startAngle}
                  angleLength={this.state.angleLength}
                  onUpdate={({ startAngle, al, angleLength }) => {
                    var val = this.circularSlider.getParsedValue(al, this.getTempMinByMode(mode), this.getTempMaxByMode(mode));
                    this.onTempChangeTo(val);
                    this.setState({ startAngle, angleLength });
                  }}
                  segments={5}
                  strokeWidth={40}
                  iconRadius={ (width - 124) / 6 }
                  radius={ (width - 98) / 2}
                  gradientColorFrom="#ff9800"
                  gradientColorTo="#ffcf00"
                  showClockFace={is_show_digits}
                  clockFaceColor="#9d9d9d"
                  bgCircleColor="#171717"
                  payload={device.payload}
              />

              {
                is_show_digits ? (
                    <View style={{bottom: 0, position: `absolute`, width: width - 100, height: null,
                      flexDirection: 'row', justifyContent: `center`}}>

                      <Button transparent onPress={() => {
                        this.onTempDecrease()
                      }} style={{position: `relative`,
                        backgroundColor: 'transparent',
                        alignItems: `center`, justifyContent: `center`,
                        width: (width - 100) / 2.9, height: (width - 100) / 3.5}}>
                        <Image source={GLOBAL.IMAGE.d_ac_ic_circle_btn_minus}/>
                      </Button>

                      <Button transparent onPress={() => {
                        this.onTempIncrease();
                      }} style={{position: `relative`,
                        backgroundColor: 'transparent',
                        alignItems: `center`, justifyContent: `center`,
                        width: (width - 100) / 2.9, height: (width - 100) / 3.5}}>
                        <Image source={GLOBAL.IMAGE.d_ac_ic_circle_btn_plus}/>
                      </Button>

                    </View>
                ) : null
              }


              <View style={{backgroundColor: 'transparent',
                position: `absolute`, left: (width - 100) / 3.5, right: (width - 100) / 3.5, top: (width - 100) / 2 - (Platform.OS === 'ios' ? 60 : 60), bottom: (width - 100) / 2 + 50, height: 50 * 2, width: null,
                flexDirection: 'row', alignItems: `center`, justifyContent: `center`}}>
                <View style={{alignItems: `center`}}>
                  {
                    (!is_show_digits)
                        ?  null : (
                            <Text style={{fontSize: 32}}><Text style={{fontSize: 52}}>{isOffline ? '--' : device.payload.t}</Text>˚c</Text>
                        )
                  }
                  {
                    (!is_show_digits)
                        ?  (
                            <Text style={{fontSize: 24, marginBottom: Platform.OS === 'ios' ? 20 : 0, top: Platform.OS === 'ios' ? 6 : 12}}>{title && title.i18n()}</Text>
                        ) : (
                            <Text style={{fontSize: 16}}>{title && title.i18n()}</Text>
                        )
                  }
                </View>
              </View>

            </ImageBackground>
          </View>
          <View style={{height: 50}}>
          </View>
        </View>
    )
  }



  __renderBottomBarButtons(device)
  {
    var isOffline = this.isDeviceOffline() || this.isDeviceVacationMode();

    var fp = GLOBAL.IMAGE.d_ac_ic_fan_power_1;
    var fp_icon = GLOBAL.IMAGE.d_ac_ic_fan_power_small_ic;
    if (device.payload.f == 1)
    {
      fp = GLOBAL.IMAGE.d_ac_ic_fan_power_1;
    }
    else if (device.payload.f == 2)
    {
      fp = GLOBAL.IMAGE.d_ac_ic_fan_power_2;
    }
    else if (device.payload.f == 3)
    {
      fp = GLOBAL.IMAGE.d_ac_ic_fan_power_3;
    }
    else if (device.payload.f == 4)
    {
      fp = GLOBAL.IMAGE.d_ac_ic_fan_power_4;
    }
    else if (device.payload.f == 5)
    {
      fp = GLOBAL.IMAGE.d_ac_ic_fan_power_5;
    }
    else if (device.payload.f == 10)
    {
      fp = GLOBAL.IMAGE.d_ac_ic_fan_power_5;
      fp_icon = GLOBAL.IMAGE.d_ac_ic_mode_auto_o;
    }
    else if (device.payload.f == 11)
    {
      fp = GLOBAL.IMAGE.d_ac_ic_fan_power_5;
      fp_icon = GLOBAL.IMAGE.d_ac_ic_mode_eco_o;
    }

    var sw = GLOBAL.IMAGE.d_ac_ic_swipe_off;
    if (device.payload.sh == 1
    || device.payload.sv == 1)
    {
      sw = GLOBAL.IMAGE.d_ac_ic_swipe_on;
    }

    return (
        <SafeAreaView>
          <View style={{opacity: isOffline ? 0.2 : 1, flexDirection: `row`, alignItems: `flex-start`, justifyContent: `space-between`}}>
            <View style={{flex: 1, flexDirection: `row`, height: 64, alignItems: `center`, justifyContent: `space-around`, borderTopColor: '#eee', borderTopWidth: 1}}>

              <Button
                  style={{height: 54, marginTop: 5, alignItems: `center`, justifyContent: `center`}}
                  disabled={isOffline} transparent onPress={() => {
                this.props.navigation.navigate("SchedulesMainPage");
              }}>
                <View style={{alignItems: `center`, justifyContent: `center`}}>
                  <View style={{flexDirection: 'row', alignItems: `center`, justifyContent: `center`}}>
                    <Image style={{margin: 3, width: 16, height: 16, resizeMode: `contain`}}
                           source={GLOBAL.IMAGE.d_ac_ic_time}/>
                    <Text style={{fontSize: 14}}>10:00 Mo</Text>
                  </View>
                  <View style={{flexDirection: 'row', alignItems: `center`, justifyContent: `center`}}>
                    <Image style={{margin: 3, width: 16, height: 16, resizeMode: `contain`}}
                           source={GLOBAL.IMAGE.d_ac_ic_auto}/>
                    <Text style={{fontSize: 14}}>25˚c</Text>
                  </View>
                </View>
              </Button>


              <Button disabled={isOffline} transparent onPress={() => {
                // this.onFanPowerToggle()
                this.RBSheetFanPower.open();
              }}>
                <Image style={{width: 64, height: 40, marginTop: 15, resizeMode: `contain`}}
                       source={fp}/>
                <Image style={{position: `absolute`, width: 24, top: 5, height: 24, resizeMode: `contain`}}
                       source={fp_icon}/>
              </Button>

              <Button disabled={isOffline} transparent onPress={() => {
                this.RBSheetSwingMode.open();
              }}>
                <Image style={{width: 64, height: 40, marginTop: 5, resizeMode: `contain`}}
                       source={sw}/>
              </Button>

            </View>
          </View>



          <RBSheet
              ref={ref => {
                this.RBSheetFanPower = ref;
              }}
              height={200}
              duration={250}
              customStyles={{
                container: {
                  justifyContent: "center",
                  alignItems: "center"
                }
              }}
          >
            <BottomSheetFanPower
                item={this.getDeviceItem().payload}
                onValueChangeItem={(ch) => {
                  Object.assign(this.getDeviceItem().payload, ch);
                  this.forceUpdate();
                  this.onSendCommand();
                }}
            />
          </RBSheet>


          <RBSheet
              ref={ref => {
                this.RBSheetSwingMode = ref;
              }}
              height={250}
              duration={250}
              customStyles={{
                container: {
                  justifyContent: "center",
                  alignItems: "center"
                }
              }}
          >
            <BottomSheetSwingDirection
                item={this.getDeviceItem().payload}
                onValueChangeItem={(ch) => {
                  Object.assign(this.getDeviceItem().payload, ch);
                  this.forceUpdate();
                  this.onSendCommand();
                }}
            />
          </RBSheet>

        </SafeAreaView>
    )
  }
}

export {
  ControlAirConditionerPage,
}
export default withNavigationFocus(ControlAirConditionerPage);

