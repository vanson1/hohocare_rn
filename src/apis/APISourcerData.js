import APISourcer from './APISourcer';

export default class APISourcerData extends APISourcer
{
  status()
  {
    return fetch(`${this.getBaseAccessPointUrl()}/status`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {
      return json;
    }).catch((error) => {
      throw error;
    });
  }


  sendjson(ac)
  {
    var payload = {
      p: ac.p || 0,
      m: ac.m || 0,
      f: ac.f || 1,
      t: ac.t || 25,
      sv: ac.sv || 0,
      sh: ac.sh || 0,
      ct: ac.ct || 1212,
      ot: ac.ot || 1313,
      otio: ac.otio || 0,
    }

    return fetch(`${this.getBaseAccessPointUrl()}/json`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify(payload),
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {
      return json;
    }).catch((error) => {
      throw error;
    });
  }


  sendraw(serial, data)
  {
    data = data || [9858,9694,9850,9698,4636,2438,412,334,416,868,438,866,412,286,468,866,440,232,494,236,466,288,462,236,468,890,440,234,492,236,492,234,492,236,492,866,414,288,436,892,438,238,490,242,486,868,438,864,412,292,460,868,412,290,464,866,440,230,492,244,484,234,494,230,474,286,462,238,490,234,496,234,492,234,496,234,492,236,488,868,438,238,490,232,492,236,492,236,492,236,466,290,438,292,436,892,438,238,488,292,438,230,496,866,412,338,418,236,466,288,466,290,440,862,414,284,470,228,472,290,464,234,492,868,438,866,440,866,440,232,496,866,438,866,438,20162,4662,4662];
    var payload = {
      access_token: GLOBAL.GET_ACCESS_TOKEN(serial),
      data: data,
      count: data.length,
    }

    return fetch(`${this.getBaseAccessPointUrl()}/api/sendrawdata`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify(payload),
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {
      return json;
    }).catch((error) => {
      throw error;
    });
  }
}
