import Prompt from '../../pages/postlogin/activation/ActivationFindDevicePage';

global.i18n.zh_CN = {
  "en_US" : 'English',
  "zh_CN" : '简体中文',
  "zh_HK" : '繁體中文',

  "Email" : "电邮地址",
  "Password" : "密码",
  "Login" : "登入",
  "Register" : "新用户登记",
  "Forgot Password?" : "忘记密码?",
  "Offline Test" : "离线测试",
  "Offline Test Mode" : "离线测试模式",
  "Register new account" : "新用户登记",
  "In order to use our D2Bros device service, please register your account below. Enter your email address and password below:" : "如你希望使用 D2Bros 装置服务，请填写以下表格登记。在下面输入你的登入电邮地址和密码：",
  "min length: 8 characters" : "最少 8 个字元",
  "Confirm password" : "确认密码",
  "I hereby confirm to accept the " : "我已确认并同意此",
  "Terms and Conditions" : "使用条款",
  "Confirm and submit" : "确认并送出",
  "Password must match confirm password field input." : "密码栏位输入要与确认密码栏位输入相同",
  "Verification" : "电邮验证",
  "Verify your email account" : "验证您的电邮地址",
  "Please check your email inbox (including your spam inbox) for an email sent by D2Bros. Retrieve and type the verification code below:" : "请检查您的电子邮件收件箱（包括垃圾邮件收件箱）以查找 D2Bros 发送的电子邮件。检索并在以下输入验证码：",
  "Verification code" : "验证码",
  "Registration complete!" : "新用户登记成功!",
  "Login now!" : "立即进入系统",
  "or" : "或",
  "Go back to title screen" : "返回主屏幕",
  "Reset password" : "重设密码",
  "This wizard will guide you through a few steps in order to reset your account password.\n\nIf you somehow forgot your account login password, we can help you reset your login password by sending an activation email to your email address. Please enter your account email address below:" : "为了重置您的登入密码，我们将会发送一封包含验证码的电邮到您的帐户。请在下面输入您的帐户电子邮件地址：",
  "Account verification" : "户口验证",
  "Please check your email inbox (including your spam inbox) for an email sent by D2Bros. Retrieve and type the verification code and create your new password below:" : "请检查您的电子邮件收件箱（包括垃圾邮件收件箱）以查找D2Bros发送的电子邮件。 检索并在以下输入验证码及创建新密码：",
  "Reset password complete" : "重设密码完成",
  "Home" : "主屏幕",
  "Last Updated at" : "最后更新时间",
  "Schedules" : "每周定时器",
  "Vacation mode" : "假期模式",
  "Add device" : "新增装置",
  "General settings" : "一般设定",
  "User Manual" : "用户手册",
  "FAQ" : "常见问题",
  "Terms of use" : "使用条款",
  "Logout" : "登出",

  "General Settings" : "一般设定",
  "Change language" : "Language / 語言 / 语言",
  "Language" : "语言",
  "Add D2Bros" : "新增 D2Bros 装置",
  "1. Make sure your D2Bros device is powered on and its LED is flashing blue light (i.e. blinking on and off at 1 second interval)\n\n2. If it does not perform the LED light flashing, wait for up to 1 minute.\n\n3. If it still does not perform the LED light flashing, press and hold the “Reset” button for 6 seconds." : "1.确保您的 D2Bros 装置已打开电源，并且其LED闪烁蓝灯（每隔1秒闪烁一次）\n\n2.如果LED指示灯没有闪烁，请等待 1.5 分钟。\n\n3.如果LED指示灯仍然没有闪烁，请长按“Reset”按钮 6秒。",
  "Ready, proceed to next step" : "准备好了，下一步",
  "Vacation Mode" : "假期模式",
  "Enable vacation mode" : "开启假期模式",
  "Apply to the following units:" : "套用于以下的装置:",
  "Terms of Use" : "使用条款",
  "Configure Wi-Fi connection" : "设定您的 Wi-Fi 连线",
  "In order to activate your D2Bros device, enter your home router Wi-Fi credentials below:" : "为了激活您的 D2Bros 装置，请在下面输入您的家庭路由器Wi-Fi凭据：",
  "Done! Proceed to next step" : "完成! 下一步",
  "D2Bros" : "D2Bros",
  "Reminder: please make sure your target Wi-Fi router is a 2.4G channel." : "小提示：请确保您的目标Wi-Fi路由器是2.4G频道。",
  "Not yet" : "不确定",
  "Confirm" : "确定",
  "Find your D2Bros" : "寻找你的 D2Bros 装置",
  "Please check your target D2Bros device you wish to activate by going to \"Settings > Wifi\" page\n\nAfter configuration complete, wait for up to 3 seconds and press the button below." : "请转到智能手机的“设定> Wi-Fi”，然后选择以下无线网络。\n\n配置完成后，等待3秒钟，然后按下面的按钮。",

  "Su" : "日",
  "Mo" : "一",
  "Tu" : "二",
  "We" : "三",
  "Th" : "四",
  "Fr" : "五",
  "Sa" : "六",

  "All units" : "所有装置",
  "Fan power" : "风速设置",
  "Air flow direction" : "风向摆动设置",
  "Vertical" : "垂直自动摆动",
  "3D mode" : "3D模式 (垂直及左右自动摆动)",
  "Horizontal" : "左右自动摆动",
  "Off" : "关闭",
  "Heat" : "供暖模式",
  "Fan" : "送风模式",
  "Dry" : "除湿模式",
  "Cool" : "制冷模式",
  "Auto" : "自动",
  "Configure" : "设定装置",
  "SSID" : "SSID",
  "Serial " : "序号",
  "Alias" : "别名",
  "Status" : "状态",
  "Online" : "在线",
  "Firmware " : "固件版本",
  "LED Settings" : "LED 指示灯设定",
  "Change alias" : "修改别名",
  "Check for firmware updates" : "检查固件更新",
  "Deactivate D2Bros" : "解除及重置装置",
  "Disable LED light" : "关闭 LED 指示灯",
  "OTA Update" : "OTA 固件升级",
  "Step" : "步骤",
  "Make sure D2Bros is online." : "请确保 D2Bros 处于在线状态。",
  "Yes, please proceed" : "好，下一步",
  "This will remove any linkage, settings and all schedules from your account. This action is irreversible. Are you sure to proceed?" : "此操作不可更改，将从您的帐户中删除此设备的所有设置和每周定时器。\n" +
      "您确定要继续吗？",
  "New Task" : "新增操作",
  "Apply" : "储存",
  "Schedule" : "启动时间",
  "Power" : "开机/关机",
  "Mode" : "运作模式设置",
  "Temperature" : "温度设置",


  "verification code incorrect" : "验证码不正确",
  "Email User already exists [500]" : "此电邮已被其他用户登记",
  "Submit forgot password request" : "要求重设密码",
  "Before you start" : "设置前准备",

  "Are you sure you wish to remove this device from your app?" : "您确定要从应用中删除此装置吗？",
  "Could not connect to device or device/app is offline. Device may be unable to reset properly. Do you still wish to remove this device from your app?" : "无法连接到装置或装置/应用正处于离线状态。装置可能无法正确重置。您仍然希望从您的应用中删除此装置吗？",
  "Activating your D2Bros" : "正在设定您的 D2Bros 装置",
  "Phone connected to D2Bros device" : "智能手机连接到 D2Bros 装置",
  "D2Bros applying home router Wi-Fi settings" : "D2Bros 装置已连接到家庭 Wi-Fi",
  "D2Bros setup complete" : "设定完成",
  "Completed, go back to home" : "完成了，回到主屏幕",

  "Edit task" : "编辑操作",
  "Remove task" : "删除操作",
  "Are you sure to remove this task?" : "您确定要删除此操作吗？",



  "Add your first D2Bros device by tapping here" : "按此新增您的第一个 D2Bros 装置",
  "Checking for firmware updates ... " : "检查固件更新 ... ",
  "OTA firmware ready. Go to \"settings > Wi-Fi settings\" and connect to your D2Bros device" : "OTA firmware ready. Go to \"settings > Wi-Fi settings\" and connect to your D2Bros device",
  "Then click \"proceed\" to undergo OTA update." : "Then click \"proceed\" to undergo OTA update.",
  "Proceed" : "开始",

  "OTA version" : "OTA 固件版本",
  "Your device version is already up-to-date." : "此装置的 OTA 固件版本已经是最新的。",
  "OTA firmware ready. Click \"proceed\" to undergo OTA update now." : "OTA 固件已准备好，按 \"开始\" 执行OTA固件更新。",


  "Vacation mode is active" : "假期模式已开启",
  "Device offline" : "装置已离线",
  "Offline" : "离线",

  "Please select a host model from our officially supported list of models:" : "请从以下系统支持红外遥控装置清单中选择：",
  "Could not connect to device or device/app is offline. Device may be unable to reset properly. " : "未能连接此装置或装置离线。",
  "Updating firmware... \n\nThis may take a few minutes. Please do not turn off device." : "正在更新固件... \n\n此步骤可能需要数分钟，请不要关掉装置。",
  "Congratulations" : "恭喜",
  "OTA Update Completed." : "OTA 固件升级完成。",

  "Quit" : "离开",
  "Exit" : "离开",
  "Retry" : "重试",

  "IR remote model" : "红外遥控器型号",
  "Daikin generic infrared remote": "大金通用遥控",
  "Daikin infrared remote (420)": "大金遥控 420 型",
  "Daikin infrared remote (R32)": "大金遥控 R32 型",

  "Change temperature scale unit": "設定溫度量度單位",
  "Temperature scale unit": "溫度量度單位",
  "Celcius": "攝氏",
  "Farenheit": "華氏",


  "Re-configure router connection" : "重新设定路由器连接",
  "Your home router Wi-Fi SSID (2.4G only)" : "您的路由器 Wi-Fi 无线网络 SSID (只支持 2.4G)",
  "Your home router Wi-Fi Password" : "您的路由器 Wi-Fi 无线网络密码",
  "Default" : "预设",
  "No" : "否",
  "Yes" : "是",

  "Cancel" : "取消",
  "Enter D2Bros device passcode" : "输入您的 D2Bros 装置密码",
  "D2Bros device passcode" : "您的 D2Bros 装置密码",
  "Version" : "版本号",

  "error" : "错误",
  "wait" : "等待",

  "Connection invalid. Please check your Wi-Fi settings. If your D2Bros device has entered AP mode, you may go through \"Add new D2Bros Device\" to re-activate the device.": "连线失败，你可能输入了错误的 Wi-Fi SSID 值或密码，请按\"离开\"。\n\n如果您的 D2Bros 装置无法连接，装置会自动进入 AP 模式，这时候请再尝试：\"新增 D2Bros 装置\"步骤，重新激活装置。",
  "Request timed out" : "要求逾时，请转到智能手机的“设定> Wi-Fi”，然后确定已转到 D2Bros 装置之无线网络。",

  "Please make sure below SSID and password combination is correct" : "请确认以下的 SSID 和密码组合输入正确",
  "An incorrect Wi-Fi settings may render your D2Bros unable to establish connection." : "若输入错误会导致您的 D2Bros 装置无法连接。",
}
