import APISourcerPreference from './APISourcerPreference'
import APISourcer from './APISourcer';

export default class APISourcerPrelogin extends APISourcer
{
  static D2Bros_Firmware_Latest = null;
  static D2Bros_Firmware_Compare_Device = (device) => {
    if (device
        && device.info
        && device.info.fw_version)
    {
      if (APISourcerPrelogin.D2Bros_Firmware_Latest
          && APISourcerPrelogin.D2Bros_Firmware_Latest.version)
      {
        return APISourcerPrelogin.D2Bros_Firmware_Compare_Device_Fw_version(device.info.fw_version, APISourcerPrelogin.D2Bros_Firmware_Latest.version);
      }
    }
    return false;
  }

  static D2Bros_Firmware_Compare_Device_Fw_version = (fw_version_current, fw_version_target) => {
    if (fw_version_current)
    {
      if (fw_version_target)
      {
        if (fw_version_current !== fw_version_target)
        {
          var fwv1 = fw_version_current.split('.');
          var fwv2 = fw_version_target.split('.');

          if (fwv1.length === fwv2.length)
          {
            for (var k = 0; k < fwv1.length; k++)
            {
              if (Number(fwv1[k]) < Number(fwv2[k]))
              {
                return true;
              }
              if (Number(fwv1[k]) > Number(fwv2[k]))
              {
                return false;
              }
              if (Number(fwv1[k]) == Number(fwv2[k]))
              {

              }
            }
          }
        }
      }
    }
    return false;
  }

  static D2Bros_Firmware_Update_Method = (device) => {
    if (device
        && device.info
        && device.info.fw_version)
    {
      if (APISourcerPrelogin.D2Bros_Firmware_Compare_Device_Fw_version(device.info.fw_version, "1.1.8"))
      {
        // older than 1.1.8, use method 1
        return 1;
      }
    }
    // otherwise, use method 2
    return 2;
  }

  static D2Bros_Firmware_Latest_Get_Url = () => {
    return APISourcerPrelogin.D2Bros_Firmware_Latest.url;
  }





  me()
  {
    return fetch(`${this.getBaseAPIUrl()}/HOHOCARE_User_Auth_Me`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'x-access-token': GLOBAL.STORE.session.accessToken,
      },
    }).then((response) => {
      return response.json();
    }).then((json) => {
      if (json.success)
      {
        return json;
      }
      else
      {
        throw Error(json.message);
      }
    }).catch((error) => {
      throw error;
    });
  }




  login(phone_number, password, phone_country_code = '852')
  {
    return fetch(`${this.getBaseAPIUrl()}/HOHOCARE_User_Auth_Login_Phone` , {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        phone_number: phone_number,
        phone_country_code: phone_country_code,
        password: password,
      }),
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {
      if (json.success)
      {
        GLOBAL.STORE.save_session(json.data);
        return json;
      }
      else
      {
        throw Error(json.message);
      }
    }).catch((error) => {
      throw error;
    });
  }


  register(username, password)
  {
    return fetch(`${this.getBaseAPIUrl()}/HOHOCARE_User_Auth_Register_v2` , {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    }).then((json) => {
      if (json.success)
      {
        return json;
      }
      else
      {
        throw Error(json.message);
      }
    }).catch((error) => {
      throw error;
    });
  }


  logout()
  {
    return new APISourcerPreference().save()
        .then((res) => {
          return new Promise((resolve, reject) => {
            return fetch(`${this.getBaseAPIUrl()}/HOHOCARE_User_Auth_Logout` , {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
              },
            }).then((response) => {
              return response.json();
            }).then((json) => {
              GLOBAL.STORE.unload_session();
              resolve(json);
            }).catch((error) => {
              GLOBAL.STORE.unload_session();
              resolve(null);
            });
          })
        })
        .then((res) => {
          return res;
        })
        .catch((e) => {
          throw e;
        })
  }



  register_request_verify(username)
  {
    return fetch(`${this.getBaseAPIUrl()}/HOHOCARE_User_Auth_Register_Request_Validate` , {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        username: username,
      }),
    }).then((response) => {
      return response.json();
    }).then((json) => {
      if (json.success)
      {
        return json;
      }
      else
      {
        throw json;
      }
    }).catch((error) => {
      throw error;
    });
  }



  register_verify(username, code)
  {
    return fetch(`${this.getBaseAPIUrl()}/HOHOCARE_User_Auth_Register_Validate` , {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        username: username,
        code: code,
      }),
    }).then((response) => {
      return response.json();
    }).then((json) => {
      if (json.success)
      {
        return json;
      }
      else
      {
        throw json;
      }
    }).catch((error) => {
      throw error;
    });
  }





  resetPassword_request_verify(username)
  {
    return fetch(`${this.getBaseAPIUrl()}/HOHOCARE_User_Auth_ResetPassword_Request_Validate` , {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        username: username,
      }),
    }).then((response) => {
      return response.json();
    }).then((json) => {
      if (json.success)
      {
        return json;
      }
      else
      {
        throw json;
      }
    }).catch((error) => {
      throw error;
    });
  }



  resetPassword_verify(username, code, password)
  {
    return fetch(`${this.getBaseAPIUrl()}/HOHOCARE_User_Auth_ResetPassword_Validate` , {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        username: username,
        code: code,
        password: password,
      }),
    }).then((response) => {
      return response.json();
    }).then((json) => {
      if (json.success)
      {
        return json;
      }
      else
      {
        throw json;
      }
    }).catch((error) => {
      throw error;
    });
  }








  HOHOCARE_Firmware_Get_Latest()
  {
    return fetch(`${this.getBaseAPIUrl()}/HOHOCARE_Firmware_Get_Latest` , {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    }).then((response) => {
      return response.json();
    }).then((json) => {
      if (json.success)
      {
        APISourcerPrelogin.D2Bros_Firmware_Latest = json.data;
        return json;
      }
      else
      {
        throw json;
      }
    }).catch((error) => {
      throw error;
    });
  }
}
