/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';

import RootMain from './src/pages/roots/RootMain'

const App = () => {
  return (
      <RootMain/>
  )
};

export default App;
