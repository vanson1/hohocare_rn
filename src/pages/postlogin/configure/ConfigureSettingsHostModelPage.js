import React, { Component } from 'react';
import { ScrollView, Image } from "react-native";
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3,
} from "native-base";
import BasePage from "../../base/BasePage";
import BaseNavigationBar from '../../base/BaseNavigationBar';

export default class ConfigureSettingsHostModelPage extends BasePage {

  constructor(props)
  {
    super(props);
    this.state = {
      isComplete: false,
      models: [
        {name: 1, title: "Daikin generic infrared remote".i18n()},
        {name: 2, title: "Daikin infrared remote (420)".i18n()},
        {name: 3, title: "Daikin infrared remote (R32)".i18n()},
      ]
    }

    this.state.selected_model = this.props.navigation.state.params.item.host_model || 1;
  }

  componentDidMount(): void {
    setTimeout(() => {
      this.setState({
        isComplete: true,
      })
    }, 5000)
  }

  renderInner() {
    return (
        <Container>

          <BaseNavigationBar {...this.props}
              isBack
              title={`Configure`.i18n()}
          />

          <Content padder scrollEnabled={false} style={{flex: 1}}>

            <H1 style={{color: '#586C78', marginTop: 60, marginBottom: 30, textAlign: `center`}}>{`IR remote model`.i18n()}</H1>

            <Text style={{color: '#586C78', padding: 5, textAlign: `left`}}>{`Please select a host model from our officially supported list of models:`.i18n()}</Text>

            <List>
              {
                this.state.models.map((it, i) => {
                  return (
                      <ListItem
                          button
                          onPress={() => {
                            this.setState({
                              selected_model: it.name,
                            })
                          }}
                          selected={this.state.selected_model === it.name}>
                        <Left>
                          <Text>{it.title}</Text>
                        </Left>
                        <Right>
                          <Icon name="checkmark" style={{
                            opacity: this.state.selected_model === it.name ? 1 : 0
                          }} />
                        </Right>
                      </ListItem>
                  )
                })
              }
            </List>

            <View style={{height: 50}}/>

            <Button
                onPress={() => {
                  this.props.navigation.state.params.item.host_model = this.state.selected_model;
                  this.props.navigation.state.params.parent.forceUpdate();
                  this.props.navigation.pop();
                }}
                style={{backgroundColor: '#4fadf9', justifyContent: `center`}}>
              <H3 style={{color: '#fff'}}>{`Confirm`.i18n()}</H3>
            </Button>

          </Content>

        </Container>
    );
  }
}
