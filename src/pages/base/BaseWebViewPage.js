import React, { Component } from 'react';
import { ScrollView, Image } from "react-native";
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3,
} from "native-base";
import BasePage from "./BasePage";
import BaseNavigationBar from './BaseNavigationBar';
import { WebView } from 'react-native-webview';

export default class BaseWebViewPage extends BasePage {

  getTitle()
  {
    return '';
  }

  getUrl()
  {
    return '';
  }

  renderInner() {

    return (
        <Container>

          <BaseNavigationBar {...this.props}
                             isMenu
                             title={this.getTitle()}
          />

          <WebView source={{ uri: this.getUrl() }} style={{flex: 1}}/>

        </Container>
    );
  }
}
