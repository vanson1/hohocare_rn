import React, { Component } from 'react';
import { ScrollView, Image } from "react-native";
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3, CheckBox,
} from 'native-base';
import BasePage from "../../../base/BasePage";
import BaseNavigationBar from '../../../base/BaseNavigationBar';

export default class GeneralSettingsPage extends BasePage {

  renderInner() {

    return (
        <Container>

          <BaseNavigationBar {...this.props}
              isMenu
              title={`General Settings`.i18n()}
          />

          <ScrollView>

            <Content padder>

              <ListItem onPress={() => {
                this.props.navigation.navigate("GeneralSettingsLanguagePage");
              }}>
                <Body>
                <Text>{`Change language`.i18n()}</Text>
                </Body>
              </ListItem>

              <ListItem onPress={() => {
                this.props.navigation.navigate("GeneralSettingsDegreeDisplayPage");
              }}>
                <Body>
                <Text>{`Change temperature scale unit`.i18n()}</Text>
                </Body>
              </ListItem>


            </Content>

          </ScrollView>

        </Container>
    );
  }
}
