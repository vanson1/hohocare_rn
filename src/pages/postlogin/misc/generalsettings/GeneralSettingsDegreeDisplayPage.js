import React, {PureComponent} from 'react';
import {Image, TouchableOpacity, ScrollView} from 'react-native';
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3, CheckBox,
} from 'native-base';
import BasePage from "../../../base/BasePage";
import BaseNavigationBar from '../../../base/BaseNavigationBar';


export default class GeneralSettingsDegreeDisplayPage extends BasePage {
  refresh()
  {
    this.changeLang = true;
    this.forceUpdate();
  }

  render() {
    return (
        <Container>

          <BaseNavigationBar {...this.props}
              icon_left={
                <Button transparent onPress={() => {
                  if (this.changeLang)
                  {
                    this.props.navigation.pop();

                    GLOBAL.NAV_GOTO_BY_SESSION();
                  }
                  else
                  {
                    this.props.navigation.pop();
                  }
                }}>
                  <Icon name='arrow-back' style={{color: '#039BE5'}} color={`#039BE5`} />
                </Button>
              }
              title={`Temperature scale unit`.i18n()}
          />


          <ScrollView>

            <Content padder>

              <ListItem  onPress={() => {
                global.APP_SETDEGREEDISPLAY('c');
                this.refresh();
              }} >
                <CheckBox checked={global.APP_GETDEGREEDISPLAY() === 'c'}  onPress={() => {
                  global.APP_SETDEGREEDISPLAY('c');
                  this.refresh();
                }} />
                <Body>
                <Text>{`Celcius`.i18n()}</Text>
                </Body>
              </ListItem>

              <ListItem  onPress={() => {
                global.APP_SETDEGREEDISPLAY('f');
                this.refresh();
              }} >
                <CheckBox checked={global.APP_GETDEGREEDISPLAY() === 'f'}  onPress={() => {
                  global.APP_SETDEGREEDISPLAY('f');
                  this.refresh();
                }} />
                <Body>
                <Text>{`Farenheit`.i18n()}</Text>
                </Body>
              </ListItem>


            </Content>

          </ScrollView>

        </Container>
    );
  }
}

