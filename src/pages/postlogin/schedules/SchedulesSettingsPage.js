import React, { Component } from 'react';
import {ScrollView, Image, Dimensions, Alert} from 'react-native';
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3,
} from "native-base";
import BasePage from "../../base/BasePage";
import moment from 'moment';
import Picker from 'react-native-picker';
import APISourcerMqtt from '../../../apis/APISourcerMqtt';
import BaseNavigationBar from '../../base/BaseNavigationBar';

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;


export default class SchedulesSettingsPage extends BasePage {

  constructor(props)
  {
    super(props);
    this.state = Object.assign(this.state || {}, {
      item: (this.props.navigation.state.params.item && Object.assign({}, this.props.navigation.state.params.item)) || GLOBAL.STORE.get_schedule_template_new(
          this.props.navigation.state.params.serial,
          this.props.navigation.state.params.weekday,
          this.props.navigation.state.params.start_time,
      )
    });

    this.state.is_new = !this.props.navigation.state.params.item;
    // {
    //   serial: serial,
    //       weekday: weekday,
    //     start_time: start_time,
    //     disabled: false,
    //     action: {power: true, temp: 25, mode: 'cool'},
    // }
  }




  showPicker(it)
  {
    var seconds = it.start_time || 0;
    let data = [
      [],
      [],
      ['am', 'pm'],
      ['Su',
        'Mo',
        'Tu',
        'We',
        'Th',
        'Fr',
        'Sa',],
    ];

    for(var i=1;i<13;i++){
      data[0].push(i);
    }
    for(var i=0;i<60;i++){
      data[1].push(i);
    }


    var hours = (Math.floor(seconds / 60 / 60));
    var minutes = (Math.floor(seconds / 60 % 60));
    var ampm = 'am';
    if (hours >= 12)
    {
      ampm = 'pm';
      hours -= 12;
    }
    if (hours <= 0) {
      hours += 12;
    }
    var weekday = it.weekday;


    Picker.init({
      pickerData: data,
      selectedValue: [hours, minutes, ampm, weekday],
      onPickerConfirm: data => {
        var hours = data[0];
        var mins = data[1];
        var ampm = data[2];
        var weekday = data[3];

        if (ampm === 'am') {
          if (hours >= 12) {
            hours -= 12;
          }
        } else if (ampm === 'pm') {
          hours += 12;
          if (hours >= 24)
          {
            hours -= 12;
          }
        }

        it.start_time = 0;
        it.start_time += mins * 60;
        it.start_time += hours * 60 * 60;
        it.weekday = weekday;

        this.forceUpdate(() => {
        })
      },
      onPickerCancel: data => {
        console.log(data);
      },
      onPickerSelect: data => {
        console.log(data);
      }
    });
    Picker.show();
  }






  getCurrentItem()
  {
    return this.state.item;
  }


  onConfirmItem()
  {
    this.props.navigation.state.params.onConfirmItem(this.state.item);
    this.props.navigation.pop();
  }


  onRemoveItem()
  {
    this.props.navigation.state.params.onRemoveItem(this.state.item);
    this.props.navigation.pop();
  }


  getTempMinByMode(mode)
  {
    var t = 18;
    if (mode == 1)
    {

    }
    if (mode == 2)
    {

    }
    if (mode == 3)
    {
      t = 18;
    }
    if (mode == 4)
    {
      t = 10;
    }
    if (mode == 6)
    {

    }
    return t;
  }
  getTempMaxByMode(mode)
  {
    var t = 30;
    if (mode == 1)
    {

    }
    if (mode == 2)
    {

    }
    if (mode == 3)
    {
      t = 32;
    }
    if (mode == 4)
    {
      t = 30;
    }
    if (mode == 6)
    {

    }
    return t;
  }
  getTempDefaultByMode(mode)
  {
    var t = 25;
    if (mode == 1)
    {

    }
    if (mode == 2)
    {

    }
    if (mode == 3)
    {
      t = 24;
    }
    if (mode == 4)
    {
      t = 26;
    }
    if (mode == 6)
    {

    }
    return t;
  }



  getMode(mode)
  {
    if (mode === "auto")
    {
      return 1;
    }
    if (mode === "cool")
    {
      return 3;
    }
    if (mode === "heat")
    {
      return 4;
    }
    if (mode === "fan")
    {
      return 6;
    }
    if (mode === "dehum")
    {
      return 2;
    }
  }



  setTemp(increment)
  {
    var {
      action,
    } = this.state.item;
    action.temp += increment;



    if (action.temp < this.getTempMinByMode(this.getMode(action.mode)))
    {
      action.temp = this.getTempDefaultByMode(this.getMode(action.mode));
    }
    if (action.temp > this.getTempMaxByMode(this.getMode(action.mode)))
    {
      action.temp = this.getTempDefaultByMode(this.getMode(action.mode));
    }


  }




  renderInner() {

    // var item = this.props.navigation.getParam("item");
    var item = this.state.item;

    var {
      serial,
      weekday,
      start_time,
      disabled,
      action,
    } = this.state.item;

    var {
      is_new
    } = this.state;

    var title = is_new ? 'New Task'.i18n() : 'Edit Task'.i18n();

    //     action: {power: true, temp: 25, mode: 'cool'},



    var desc = "";

    var d1 = GLOBAL.IMAGE.d_ac_ic_swing_off_n;
    var d2 = GLOBAL.IMAGE.d_ac_ic_swing_horizontal_n;
    var d3 = GLOBAL.IMAGE.d_ac_ic_swing_vertical_n;
    var d4 = GLOBAL.IMAGE.d_ac_ic_swing_3D_n;

    if (action.swing === 'Off')
    {
      desc = "Off";
      d1 = GLOBAL.IMAGE.d_ac_ic_swing_off_o;
    }

    if (action.swing === 'Horizontal')
    {
      desc = "Horizontal";
      d2 = GLOBAL.IMAGE.d_ac_ic_swing_horizontal_o;
    }

    if (action.swing === 'Vertical')
    {
      desc = "Vertical";
      d3 = GLOBAL.IMAGE.d_ac_ic_swing_vertical_o;
    }

    if (action.swing === '3D mode')
    {
      desc = "3D mode";
      d4 = GLOBAL.IMAGE.d_ac_ic_swing_3D_o;
    }


    var seconds = item.start_time || 0;
    var hhmm = ((Math.floor(seconds / 60 / 60) + "").padStart(2, "0")) + ":" + ((Math.floor(seconds / 60 % 60) + "").padStart(2, "0"));


    return (
        <Container>

          <BaseNavigationBar {...this.props}
              isBack
              icon_right={
                <Right>
                  <Button transparent onPress={() => {
                    this.onConfirmItem()
                  }}>
                    <Text>{`Apply`.i18n()}</Text>
                  </Button>
                </Right>
              }
              title={title}
          />

          <ScrollView>

            <Content padder>

              <View style={{height: 30}}/>




              <View style={{flexDirection: `row`, alignItems: `center`, justifyContent: `center`}}>
                <Text>{`Schedule`.i18n()}</Text>
                <View style={{flexDirection: `row`, alignItems: `center`, justifyContent: `center`, paddingLeft: 30}}>


                  <Button transparent
                          onPress={() => {
                            this.showPicker(item);
                          }}
                          style={{flexDirection: `row`}}>
                    <View style={{paddingLeft: 10, flexDirection: `row`}}>
                      <H2>{hhmm}</H2>
                      <View style={{width: 10}}/>
                      <H2>{item.weekday}</H2>

                    </View>

                  </Button>
                </View>
              </View>


              <View style={{height: 30}}/><View style={{height: 1, backgroundColor: '#efefef'}}/><View style={{height: 30}}/>



              <View style={{flexDirection: `row`, alignItems: `center`, justifyContent: `center`}}>
                <Text>{`Power`.i18n()}</Text>
                <View style={{flexDirection: `row`, alignItems: `center`, justifyContent: `center`, paddingLeft: 30}}>

                  <Button transparent onPress={() => {
                    action.power = false;
                    this.forceUpdate();
                  }} style={{paddingHorizontal: 4, width: 44, height: 44}}>
                    <Image source={action.power == 0 ? GLOBAL.IMAGE.d_ac_ic_power_off_o :  GLOBAL.IMAGE.d_ac_ic_power_off_n} style={{resizeMode: `contain`, padding: 5, width: 34, height: 34}}/>
                  </Button>

                  <Button transparent onPress={() => {
                    action.power = true;
                    this.forceUpdate();
                  }} style={{paddingHorizontal: 4, width: 44, height: 44}}>
                    <Image source={action.power == 1 ? GLOBAL.IMAGE.d_ac_ic_power_on_o :  GLOBAL.IMAGE.d_ac_ic_power_on_n} style={{resizeMode: `contain`, padding: 5, width: 34, height: 34}}/>
                  </Button>

                </View>
              </View>


              <View style={{height: 30}}/><View style={{height: 1, backgroundColor: '#efefef'}}/><View style={{height: 30}}/>


              <View style={{alignItems: `center`, justifyContent: `center`}}>
                <Text>{`Mode`.i18n()}</Text>
                <View style={{flexDirection: `row`, alignItems: `center`, justifyContent: `center`}}>


                  <View style={{flexDirection: `row`, alignItems: `flex-start`, justifyContent: `space-between`, marginHorizontal: 50}}>
                    <View style={{height: 64, flex: 1, flexDirection: `row`, alignItems: `center`, justifyContent: `center`}}>
                      <Button transparent onPress={() => {
                        action.mode = "auto";
                        this.setTemp(0);
                        this.forceUpdate();
                      }} style={{width: 44, height: 44, padding: 5, marginHorizontal: 7}}>
                        <Image source={action.mode === "auto" ? GLOBAL.IMAGE.d_ac_ic_mode_auto_o : GLOBAL.IMAGE.d_ac_ic_mode_auto_n} style={{width: 30, height: 30}} />
                      </Button>
                      <Button transparent onPress={() => {
                        action.mode = "cool";
                        this.setTemp(0);
                        this.forceUpdate();
                      }} style={{width: 44, height: 44, padding: 5, marginHorizontal: 7}}>
                        <Image source={action.mode === "cool" ? GLOBAL.IMAGE.d_ac_ic_mode_cool_o : GLOBAL.IMAGE.d_ac_ic_mode_cool_n} style={{width: 30, height: 30}} />
                      </Button>
                      <Button transparent onPress={() => {
                        action.mode = "heat";
                        this.setTemp(0);
                        this.forceUpdate();
                      }} style={{width: 44, height: 44, padding: 5, marginHorizontal: 7}}>
                        <Image source={action.mode === "heat" ? GLOBAL.IMAGE.d_ac_ic_mode_heat_o : GLOBAL.IMAGE.d_ac_ic_mode_heat_n} style={{width: 30, height: 30}} />
                      </Button>
                      <Button transparent onPress={() => {
                        action.mode = "fan";
                        this.setTemp(0);
                        this.forceUpdate();
                      }} style={{width: 44, height: 44, padding: 5, marginHorizontal: 7}}>
                        <Image source={action.mode === "fan" ? GLOBAL.IMAGE.d_ac_ic_mode_fan_o : GLOBAL.IMAGE.d_ac_ic_mode_fan_n} style={{width: 30, height: 30}} />
                      </Button>
                      <Button transparent onPress={() => {
                        action.mode = "dehum";
                        this.setTemp(0);
                        this.forceUpdate();
                      }} style={{width: 44, height: 44, padding: 5, marginHorizontal: 7}}>
                        <Image source={action.mode === "dehum" ? GLOBAL.IMAGE.d_ac_ic_mode_dehum_o : GLOBAL.IMAGE.d_ac_ic_mode_dehum_n} style={{width: 30, height: 30}} />
                      </Button>
                    </View>
                  </View>


                </View>
              </View>


              <View style={{height: 30}}/><View style={{height: 1, backgroundColor: '#efefef'}}/><View style={{height: 30}}/>



              <View style={{alignItems: `center`, justifyContent: `center`}}>
                <Text>{`Fan power`.i18n()}</Text>

                <View style={{flexDirection: `row`, width: width}}>
                  <View style={{marginTop: 15, flex: 1, flexDirection: `row`, alignItems: `center`, justifyContent: `space-around`}}>
                    <Button transparent
                            onPress={() => {
                              action.fan = "1";
                              this.forceUpdate();
                            }}
                            style={{flex: 1, alignItems: `center`, justifyContent: `center`}}>
                      <Text style={{color: action.fan === "1" ? '#0ca4e3' : '#666', fontSize: 28, fontWeight: 'bold'}}>{`1`}</Text>
                    </Button>
                    <Button transparent
                            onPress={() => {
                              action.fan = "2";
                              this.forceUpdate();
                            }}
                            style={{flex: 1, alignItems: `center`, justifyContent: `center`}}>
                      <Text style={{color: action.fan === "2" ? '#0ca4e3' : '#666', fontSize: 28, fontWeight: 'bold'}}>{`2`}</Text>
                    </Button>
                    <Button transparent
                            onPress={() => {
                              action.fan = "3";
                              this.forceUpdate();
                            }}
                            style={{flex: 1, alignItems: `center`, justifyContent: `center`}}>
                      <Text style={{color: action.fan === "3" ? '#0ca4e3' : '#666', fontSize: 28, fontWeight: 'bold'}}>{`3`}</Text>
                    </Button>
                    <Button transparent
                            onPress={() => {
                              action.fan = "4";
                              this.forceUpdate();
                            }}
                            style={{flex: 1, alignItems: `center`, justifyContent: `center`}}>
                      <Text style={{color: action.fan === "4" ? '#0ca4e3' : '#666', fontSize: 28, fontWeight: 'bold'}}>{`4`}</Text>
                    </Button>
                    <Button transparent
                            onPress={() => {
                              action.fan = "5";
                              this.forceUpdate();
                            }}
                            style={{flex: 1, alignItems: `center`, justifyContent: `center`}}>
                      <Text style={{color: action.fan === "5" ? '#0ca4e3' : '#666', fontSize: 28, fontWeight: 'bold'}}>{`5`}</Text>
                    </Button>
                    <Button transparent
                            onPress={() => {
                              action.fan = "auto";
                              this.forceUpdate();
                            }}
                            style={{flex: 1, alignItems: `center`, justifyContent: `center`}}>
                      <Image style={{height: 28, resizeMode: `contain`}}
                             source={action.fan === "auto" ? GLOBAL.IMAGE.d_ac_ic_mode_auto_o : GLOBAL.IMAGE.d_ac_ic_mode_auto_n}/>
                    </Button>
                    <Button transparent
                            onPress={() => {
                              action.fan = "quiet";
                              this.forceUpdate();
                            }}
                            style={{flex: 1, alignItems: `center`, justifyContent: `center`}}>
                      <Image style={{height: 28, resizeMode: `contain`}}
                             source={action.fan === "quiet" ? GLOBAL.IMAGE.d_ac_ic_mode_eco_o : GLOBAL.IMAGE.d_ac_ic_mode_eco_n}/>
                    </Button>
                  </View>
                </View>

              </View>


              <View style={{height: 30}}/><View style={{height: 1, backgroundColor: '#efefef'}}/><View style={{height: 30}}/>




              <View style={{alignItems: `center`, justifyContent: `center`}}>
                <Text>{`Air flow direction`.i18n()}</Text>
                <View style={{flexDirection: `row`, alignItems: `center`, justifyContent: `center`}}>


                  <View style={{marginTop: 15, flexDirection: `row`, alignItems: `center`, justifyContent: `space-around`}}>
                    <Button transparent
                            onPress={() => {
                              action.swing = "Off";
                              this.forceUpdate();
                            }}
                            style={{padding: 30}}>
                      <Image source={d1} style={{width: 56, height: 56}}/>
                    </Button>
                    <Button transparent
                            onPress={() => {
                              action.swing = "Horizontal";
                              this.forceUpdate();
                            }}
                            style={{padding: 30}}>
                      <Image source={d2} style={{width: 56, height: 56}}/>
                    </Button>
                    <Button transparent
                            onPress={() => {
                              action.swing = "Vertical";
                              this.forceUpdate();
                            }}
                            style={{padding: 30}}>
                      <Image source={d3} style={{width: 56, height: 56}}/>
                    </Button>
                    <Button transparent
                            onPress={() => {
                              action.swing = "3D mode";
                              this.forceUpdate();
                            }}
                            style={{padding: 30}}>
                      <Image source={d4} style={{width: 56, height: 56}}/>
                    </Button>
                  </View>


                </View>
              </View>



              <View style={{height: 30}}/><View style={{height: 1, backgroundColor: '#efefef'}}/><View style={{height: 30}}/>



              {
                (this.getMode(action.mode) === 6 || this.getMode(action.mode) === 2) ? null : [
                  <View style={{alignItems: `center`, justifyContent: `center`, flexDirection: `row`}}>
                    <Text>{`Temperature`.i18n()}</Text>
                    <View style={{flexDirection: `row`, alignItems: `center`, justifyContent: `center`, paddingLeft: 30}}>

                      <Button transparent onPress={() => {
                        this.setTemp(-1);
                        this.forceUpdate();
                      }} style={{borderRadius: 10, width: 44, height: 44, backgroundColor: '#999', alignItems: `center`, justifyContent: `center`}}>
                        <Image source={GLOBAL.IMAGE.d_ac_ic_circle_btn_minus} style={{resizeMode: `contain`, padding: 5, width: 24, height: 24}}/>
                      </Button>

                      <View style={{height: 44, width: 84, alignItems: `center`, justifyContent: `center`}}>
                        <Text style={{color: '#333', fontSize: 18, fontWeight: `bold`}}>
                          {action.temp}
                        </Text>
                      </View>

                      <Button transparent onPress={() => {
                        this.setTemp(+1);
                        this.forceUpdate();
                      }} style={{borderRadius: 10, width: 44, height: 44, backgroundColor: '#999', alignItems: `center`, justifyContent: `center`}}>
                        <Image source={GLOBAL.IMAGE.d_ac_ic_circle_btn_plus} style={{resizeMode: `contain`, padding: 5, width: 24, height: 24}}/>
                      </Button>

                    </View>
                  </View>,

                  <View style={{height: 30}}/>,<View style={{height: 1, backgroundColor: '#efefef'}}/>,<View style={{height: 30}}/>
                ]
              }



              {
                this.state.is_new ? null : [(

                    <Button danger
                            onPress={() => {
                              Alert.alert("D2Bros", "Are you sure to remove this task?", [
                                {text: 'No', onPress: () => {}},
                                {text: 'Yes', onPress: () => {
                                    this.onRemoveItem();
                                  }},
                              ])
                            }}
                            style={{justifyContent: `center`}}>
                      <H3 style={{color: '#fff'}}>Remove task</H3>
                    </Button>
                ),
                    <View style={{height: 30}}/>,<View style={{height: 1, backgroundColor: '#efefef'}}/>,<View style={{height: 30}}/>
                ]
              }



            </Content>

          </ScrollView>

        </Container>
    );
  }
}
