import {AsyncStorage} from 'react-native';

function init()
{
  global.___lang = null;
  global.___lang_default = 'en_US';
  global.___langs = [
    'en_US',
    'zh_HK',
    'zh_CN',
  ]

  global.APP_SETLANGUAGE = ((lang) => {
    global.___lang = lang;
    return AsyncStorage.setItem('__LANGUAGE', lang)
      .then(() => {
        return null;
      })
  })

  global.APP_GETLANGUAGE = (is_strict) => {
    if (global.___lang == null)
    {
      return AsyncStorage.getItem('__LANGUAGE')
        .then((value) => {
          if (!value && !is_strict) {
            value = global.___lang_default;
          }
          global.___lang = value;
          return value;
        })
    }
    return global.___lang;
  }

  global.i18n = Object.assign(global.i18n || {}, {
    render: function(str, lang) {
      if (!lang) {
        lang = global.APP_GETLANGUAGE();
      }
      if (!global.i18n[lang]) {
        global.i18n.pending.push(str);
        if (lang === 'en_US')
        {
          return "" + str;
        }
        return "***" + str;
      }
      if (!global.i18n[lang][str]) {
        global.i18n.pending.push(str);
        if (lang === 'en_US')
        {
          return "" + str;
        }
        return "***" + str;
      }
      return global.i18n[lang][str];
    }
  })

  global.i18n.pending = [];

  require('./i18n/en_US');
  require('./i18n/zh_CN');
  require('./i18n/zh_HK');

  return global.APP_GETLANGUAGE()
    .then(() => {
      return null;
    })
}


String.prototype.i18n = function(lang) {
  var str = global.i18n.render(this, lang);
  return str;
}

if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) {
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
        ;
    });
  };
}

String.prototype.dateTimeFormat = function() {
  if (!this) {
    return "";
  }
  var str = this;
  var date = str.split("T")[0]
  var time = str.split("T")[1].split(".000Z")[0];
  return date + " " + time;
}


module.exports = {
  init: init,
}
