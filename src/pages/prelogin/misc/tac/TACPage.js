import React, { Component } from 'react';
import { ScrollView, Image } from "react-native";
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3,
} from "native-base";
import BasePage from "../../../base/BasePage";
import { WebView } from 'react-native-webview';
import BaseNavigationBar from '../../../base/BaseNavigationBar';

export default class TACPage extends BasePage {

  renderInner() {

    return (
        <Container>

          <BaseNavigationBar {...this.props}
                             isBack
                             title={`Terms of use`.i18n()}
          />

          <WebView source={{uri: GLOBAL.url_tc}} style={{flex: 1}}/>

        </Container>
    );
  }
}
