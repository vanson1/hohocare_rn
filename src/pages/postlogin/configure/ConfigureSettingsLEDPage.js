import React, { Component } from 'react';
import { StackActions, NavigationActions } from 'react-navigation';
import { Alert, ScrollView, Image } from "react-native";
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3, Form, Item, Label, Input, Switch,
} from 'native-base';
import BasePage from "../../base/BasePage";
import WifiManager from "react-native-wifi-reborn";
import APISourcerMqtt from "../../../apis/APISourcerMqtt";
import BaseNavigationBar from '../../base/BaseNavigationBar';

export default class ConfigureSettingsLEDPage extends BasePage {

  constructor(props)
  {
    super(props);
    var item = GLOBAL.CONFIG_ITEM;
    this.state = {
      led_disabled: (item.info && !!item.info.is_led_disable),
    }
  }

  componentDidMount(): void {
    this.didChange = false;
    var serial = this.props.navigation.state.params.item.serial;
    GLOBAL.LISTEN_DEVICE_ACK(serial, this.onAck.bind(this))
  }

  componentWillUnmount(): void {
    this.didChange = false;
    var serial = this.props.navigation.state.params.item.serial;
    GLOBAL.UNLISTEN_DEVICE_ACK(serial, this.onAck.bind(this))
  }

  onAck()
  {
    if (this.didChange)
    {

    }
  }

  renderInner() {

    return (
        <Container>

          <BaseNavigationBar {...this.props}
              isBack
              title={`LED Settings`.i18n()}
          />

          <Content padder>

            <ListItem style={{paddingRight: 20, justifyContent: `space-between`}}>

              <Text style={{color: '#586C78', padding: 5, textAlign: `left`}}>{`Disable LED light`.i18n()}</Text>

              <Switch
                  onValueChange={(bool) => {
                    this.setState({
                      led_disabled: bool,
                    }, () => {

                      var serial = this.props.navigation.state.params.item.serial;
                      APISourcerMqtt.getInstance().publishCommandAC(serial, 'settings', {
                        led_disable: this.state.led_disabled,
                        vacation_mode: GLOBAL.CONFIG_ITEM.vacation_mode && GLOBAL.STORE.vacation_mode.is_enabled,
                      });

                      var item = GLOBAL.CONFIG_ITEM;
                      item.info = item.info || {};
                      item.info.led_disabled = bool;
                    })
                  }}
                  value={this.state.led_disabled} />

            </ListItem>


          </Content>

        </Container>
    );
  }
}
