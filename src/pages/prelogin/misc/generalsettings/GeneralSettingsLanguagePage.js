import React, {PureComponent} from 'react';
import {Image, TouchableOpacity, ScrollView} from 'react-native';
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  View,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  H1,
  H2,
  H3, CheckBox,
} from 'native-base';
import BasePage from "../../../base/BasePage";
import BaseNavigationBar from '../../../base/BaseNavigationBar';
import APISourcerPreference from '../../../../apis/APISourcerPreference';


export default class GeneralSettingsLanguagePage extends BasePage {
  constructor(props)
  {
    super(props);
    this.state = Object.assign(this.state || {}, {
      is_ready: false,
    })
  }

  componentDidMount(): void {
    if (global.APP_GETLANGUAGE(true)) {
      if (!this.props.navigation || !this.props.navigation.state.params || !this.props.navigation.state.params.isBack)
      {
        this.props.navigation.replace("WelcomePage");
      }
      else
      {
        this.setState({
          is_ready: true,
        })
      }
    } else {
      this.setState({
        is_ready: true,
      })
    }
  }

  refresh()
  {
    this.forceUpdate();
    this.showLoading();
    new APISourcerPreference().HOHOCARE_User_Get_Access_Info()
        .then(() => {
          this.refreshNext()
        })
        .catch((e) => {
          this.refreshNext()
        })
  }

  refreshNext()
  {
    if (!this.props.navigation || !this.props.navigation.state.params || !this.props.navigation.state.params.isBack)
    {
      this.props.navigation.replace("WelcomePage");
    }
    else
    {
      this.props.navigation.pop();
      this.props.navigation.state.params.callback && this.props.navigation.state.params.callback();
    }
  }

  renderInner() {
    if (!this.state.is_ready) {
      return null;
    }
    return (
        <Container>

          <BaseNavigationBar {...this.props}
              isBack={this.props.navigation && this.props.navigation.state.params && this.props.navigation.state.params.isBack}
              title={`Language`.i18n()}
          />

          <ScrollView>

            <Content padder>

              <ListItem  onPress={() => {
                global.APP_SETLANGUAGE('en_US');
                this.refresh();
              }} >
                <CheckBox checked={global.APP_GETLANGUAGE() === 'en_US'}  onPress={() => {
                  global.APP_SETLANGUAGE('en_US');
                  this.refresh();
                }} />
                <Body>
                <Text>{`en_US`.i18n()}</Text>
                </Body>
              </ListItem>

              <ListItem  onPress={() => {
                global.APP_SETLANGUAGE('zh_HK');
                this.refresh();
              }} >
                <CheckBox checked={global.APP_GETLANGUAGE() === 'zh_HK'}  onPress={() => {
                  global.APP_SETLANGUAGE('zh_HK');
                  this.refresh();
                }} />
                <Body>
                <Text>{`zh_HK`.i18n()}</Text>
                </Body>
              </ListItem>

              <ListItem  onPress={() => {
                global.APP_SETLANGUAGE('zh_CN');
                this.refresh();
              }} >
                <CheckBox checked={global.APP_GETLANGUAGE() === 'zh_CN'}  onPress={() => {
                  global.APP_SETLANGUAGE('zh_CN');
                  this.refresh();
                }} />
                <Body>
                <Text>{`zh_CN`.i18n()}</Text>
                </Body>
              </ListItem>


            </Content>

          </ScrollView>

        </Container>
    );
  }
}

