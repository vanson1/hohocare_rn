import React, { Component } from 'react';
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Accordion,
  Button,
  Icon,
  Content,
  List,
  ListItem,
  Text,
  Form, Item, Label, Input,
} from 'native-base';
import {Image, TouchableOpacity, View, FlatList, Alert, PermissionsAndroid} from 'react-native';
import BasePage from "../../base/BasePage";
import { withNavigationFocus } from "react-navigation";
import APISourcerMqtt from '../../../apis/APISourcerMqtt';
import BaseNavigationBar from '../../base/BaseNavigationBar';
import Smartconfig from 'react-native-smartconfig-esp';
import WifiManager from "react-native-wifi-reborn";


class GatewaySmartWifiPageWizard extends BasePage {
  constructor(props)
  {
    super(props);
    this.state = {
      username: "",
      password: "",
    }
  }

  componentDidMount(): void {
    this.set_ssid()
  }

  componentWillUnmount(): void {

    this.sc_stop()
  }




  set_ssid()
  {

    var connect = () => {

      WifiManager.getCurrentWifiSSID().then(
          ssid => {
            this.setState({
              username: ssid,
            })

          },
          () => {
            console.log("Cannot get current SSID!");
            alert("Cannot get current SSID!")
          }
      );
    }

    if (Platform.OS === "ios")
    {
      connect();
    }
    else
    {
      PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Location permission is required for WiFi connections',
            message:
                'This app needs location permission as this is required  ' +
                'to scan for wifi networks.',
            buttonNegative: 'DENY',
            buttonPositive: 'ALLOW',
          },
      )
          .then((granted) => {
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {

              connect();

            } else {

            }

          })
          .catch((e) => {

          })
    }

  }




  sc_begin()
  {
    Smartconfig.start({
      type: 'esptouch', //or airkiss, now doesn't not effect
      ssid: this.state.username,
      bssid: '', //"" if not need to filter (don't use null)
      password: this.state.password,
      taskCount: 1
    }).then(function(results){
      //Array of device success do smartconfig
      console.log(results);
      alert(JSON.stringify(results))
      /*[
        {
          'bssid': 'device-bssi1', //device bssid
          'ipv4': '192.168.1.11' //local ip address
        },
        {
          'bssid': 'device-bssi2', //device bssid
          'ipv4': '192.168.1.12' //local ip address
        },
        ...
      ]*/
    }).catch(function(error) {
      alert(JSON.stringify(error))
    });

  }

  sc_stop()
  {
    Smartconfig.stop();
  }



  renderInner() {
    return (
        <Container>

          <BaseNavigationBar {...this.props}
                             isBack
                             title={`Smart Wifi`.i18n()}
          />

          <Content>


            <Form>
              <Item>
                <Label>{`WiFi SSID`.i18n()}</Label>
                <Input value={this.state.username} autoCapitalize={false} autoCompleteType={'off'} onChangeText={(val) => {this.setState({username: val})}} />
              </Item>
              <Item>
                <Label>{`Password`.i18n()}</Label>
                <Input value={this.state.password} autoCapitalize={false} autoCompleteType={'off'} secureTextEntry onChangeText={(val) => {this.setState({password: val})}} />
              </Item>
            </Form>

            <Button transparent
                    onPress={() => {
                      this.sc_begin()
                    }}
                    style={{justifyContent: `center`}}>
              <Text style={{}}>{`Smart Config Test begin`.i18n()}</Text>
            </Button>

            <Button transparent
                    onPress={() => {
                      this.sc_stop()
                    }}
                    style={{justifyContent: `center`}}>
              <Text style={{}}>{`Smart Config Test stop`.i18n()}</Text>
            </Button>


          </Content>

        </Container>
    );
  }
}


export default withNavigationFocus(GatewaySmartWifiPageWizard)
